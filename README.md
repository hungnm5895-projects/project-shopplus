<h1 align="center"> ShopPlus </h1> <br>
<p align="center">
  <a href="https://gitpoint.co/">
    <img alt="ShopPlus" title="ShopPlus" src="https://tse1.mm.bing.net/th/id/OIP.cLFOLwIR8eC_nGBMeJ5fmgHaB7?rs=1&pid=ImgDetMain" width="%">
  </a>
</p>

<p align="center">
  Website tai nghe trực tuyến ShopPlus.
</p>

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Feedback](#feedback)
- [Contributors](#contributors)
- [Build Process](#build-process)
- [Backers](#backers-)
- [Sponsors](#sponsors-)
- [Acknowledgments](#acknowledgments)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

- Sản phẩm website thương mại điện tử cung cấp hệ thống cho phép người dùng là khách hàng có nhu cầu tìm kiếm, mua sắm các sản phẩm tai nghe, phụ kiện và cung cấp hệ thống cho phép nhân viên quản trị, báo cáo phục vụ hoạt động sản xuất kinh doanh.

<p><b>Công nghệ phát triển:<b></p> 
+ Front End
<ul>
<li>HTML, CSS</li>
<li>Bootstrap 4, Bootstrap5, fontawesome 4, 5</li>
<li>JavaScrip</li>
<li>Jquery</li>
<li>DataTable</li>
<li>Admin Lte 3.2.0</li>
<li>Pagingnation, Ajax </li>
<li>DayJs, ChartJs</li>
<li>Google Map</li>
</ul>
+ Back End
<ul>
<li>Java 17</li>
<li>Spring Boot</li>
<li>Hibernate</li>
<li>MyPHPAdmin</li>
<li>Jwt</li>
<li>Apache POI</li>

</ul>

**Hệ thống triển khai qua hình thức Website thương mại điện tử.**


## Features

Hệ thống cung cấp các tinh năng:
<p><b>Khách hàng<b></p>
<ul>
<li>Truy cập trang chủ, kiểm tra các sản phẩm nổi bật</li>
<li>Đăng nhập, đăng ký, quên mật khẩu, quản lý thông tin người dùng</li>
<li>Xem các sản phẩm HOT, xu hướng, bán chạy</li>
<li>Xem các chương trình khuyến mại HOT</li>
<li>Xem blog, đăng ký email theo dõi</li>
<li>Xem chi tiết sản phẩm, đánh giá, viết bình luận cho sản phẩm</li>
<li>Lọc, tìm kiếm các sản phẩm theo tên, từ khóa, tầm giá, hãng, quốc gia</li>
<li>Danh sách cửa hàng trực tiếp theo tỉnh thành, bản đồ vị trí chi tiết của cửa hàng</li>
<li>Giỏ hàng, thanh toán</li>
<li>Thông báo, nhắc nhở</li>
<li>Thêm mới sản phẩm</li>
<li>Quản lý đơn hàng</li>
</ul>


<p><b>Quản trị viên</b></p>
<ul>
<li>Dashboard quản lý chung</li>
<li>Quản trị người dùng</li>
<li>Quản trị sản phẩm</li>
<li>Quản lý đơn hàng, lịch sử mua hàng</li>
<li>Quản trị khách hàng</li>
<li>Gửi email, gửi thông báo cho khách hàng</li>
<li>Quản trị danh sách cửa hàng</li>
<li>Quản trị thương hiệu, nhẫn hàng, quốc gia, quận, huyện</li>
</ul>

<p><b>Đang phát triển</b></p>
<ul>
<li>Xuất báo cáo, thống kê</li>


</ul>

## Feedback
Mọi phản hồi vui lòng xin liên hệ chúng tôi theo email: hungnm5895@gmail.com . Mọi đóng góp, phản hồi sẽ được ghi nhận và đánh giá.

## Contributors

This project follows the [all-contributors](https://github.com/kentcdodds/all-contributors) specification and is brought to you by these [awesome contributors](./CONTRIBUTORS.md).

## Build Process


## Backers [![Backers on Open Collective](https://opencollective.com/git-point/backers/badge.svg)](#backers)

Cám ơn tất cả các bạn tuyển dụng đã quan tâm đến dự án của tôi.

<a href="https://opencollective.com/git-point#backers" target="_blank"><img src="https://opencollective.com/git-point/backers.svg?width=890"></a>

## Sponsors [![Sponsors on Open Collective](https://opencollective.com/git-point/sponsors/badge.svg)](#sponsors)

N/a

<a href="https://opencollective.com/git-point/sponsor/0/website" target="_blank"><img src="https://opencollective.com/git-point/sponsor/0/avatar.svg"></a>

## Acknowledgments

N/a

