/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gIsLogin = getCookie("loginSession");

var gLoginSesstion;
var gtoken;
var gRole;
var gId;
var gCusId;


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {

  onPageLoadingAuth();

  //Khi ấn đăng xuất
  $("#btn-logout").click(function () {
    localStorage.clear(); // Xóa giỏ hàng + xóa voucher dùng;
    setCookie("loginSession", "", 1); // Xóa login session
    window.location.href = "index.html";
    loadAllNoti(0)
  });

  $("#noti-div").on("click", ".ti-eye", function () {
    var vNotiId = $(this).data("mydata");
    SeeNoti(vNotiId);

  })
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoadingAuth() {
  $("#btn-user-info").addClass("d-none"); //Ẩn nút tài khoản của tôi
  $("#btn-admin").addClass("d-none"); //Ẩn nút đăng nhập
  $("#btn-login-btn").addClass("d-none"); //Hiện nút cho phép xem DS BDS
  $("#btn-logout").addClass("d-none"); //Hiện nút cho phép xem DS BDS

  //nếu chưa đăng nhập, có thông tin session trong cookie
  if (gIsLogin == "") { //Nếu chưa đăng nhập
    $("#btn-login-btn").removeClass("d-none"); //Hiện nút đăng nhập


  } else { //nếu đã đăng nhập
    $("#btn-logout").removeClass("d-none");
    $("#btn-user-info").removeClass("d-none");

    gLoginSesstion = JSON.parse(gIsLogin);
    gtoken = gLoginSesstion.token;
    gRole = gLoginSesstion.authorities;
    gId = gLoginSesstion.id;
    gCusId = gLoginSesstion.customerId;

    //Hiển thị các menu trên icon avatar theo từng case
    switch (gRole[0]) {
      case "ROLE_ADMIN":
        $("#btn-admin").removeClass("d-none"); //Hiện nút quản trị
        break;
    }

    //Load danh sách noti
    loadAllNoti(gCusId);
  }

}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

//Hàm get Cookie
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

//Hàm setCookie
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//hàm gọi api lấy dữ liệu noti của khách hàng
function loadAllNoti(gCusId) {
  $.ajax({
    url: gBASE_URL + "/notifications/customer/" + gCusId,
    method: "GET",
    async: false,
    success: function (pObjRes) {
      (pObjRes);
      showNotiInfo(pObjRes);
    },
    error: function (pXhrObj) {
      $("#noti-div").html("Không có thông báo nào chưa đọc");
      $("#noti-sum").html("0");
    },
  });
}

//Hàm hiển thị dữ liệu noti
function showNotiInfo(paramData) {
  $("#noti-sum").html(paramData.length);
  var vNotiDiv = $("#noti-div").html("");
  if (paramData.length > 0) {
    paramData.forEach(element => {
      var vLi = $("<li>").html(`<h4 class="noti-link" data-mydata="${element.id}"><a href="${element.notiLink}">  ${element.notiContent}</a></h4>`)
        .appendTo(vNotiDiv);
      $("<i>").addClass("ti-eye").data("mydata", element.id).appendTo(vLi);
    });
  } else {
    $("#noti-div").html("Không có thông báo nào chưa đọc");
    $("#noti-sum").html("0");
  }

}

//hàm see Noti
function SeeNoti(paramId) {
  $.ajax({
    url: gBASE_URL + "/notifications/see/" + paramId,
    method: "PUT",
    async: false,
    success: function (pObjRes) {
      ("xem ok");
      loadAllNoti(gCusId);
    },
    error: function (pXhrObj) {
      toastr.error("Có lỗi xảy ra, vui lòng thử lại!");
    },
  });
}