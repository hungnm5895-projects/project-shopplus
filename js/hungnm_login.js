/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gUrl = "http://localhost:8080/";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  $("#inp-username").val("");
  $("#inp-password").val("");
  //nếu chưa đăng nhập, có thông tin session trong cookie
  $("#btn-login").click(function () {
    loginErrorClear();
    onBtnLoginClick();
  });
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onBtnLoginClick() {
  var vLoginData = {
    username: $("#inp-username").val().trim(),
    password: $("#inp-password").val().trim(),
  };

  var vCheck = validateLoginData(vLoginData);

  if (vCheck) {
    $.ajax({
      url: gUrl + "login",
      type: "POST",
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(vLoginData),
      success: function (responseObject) {
        setCookie("loginSession", JSON.stringify(responseObject), 1);
        window.location.href = "index.html";
      },
      error: function (xhr) {
        $("#error-login").html(xhr.responseText).removeClass("d-none");
        //toastr.error(xhr.responseText);
      },
    });
  }
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Kiểm tra dữ liệu login
function validateLoginData(vLoginData) {
  var bCheck = true;
  if (vLoginData.username == "") {
    $("#error-username").html("Bạn cần nhập vào tài khoản").removeClass("d-none");
    bCheck = false;
  }
  if (vLoginData.password.length < 6) {
    $("#error-password").html("Bạn cần nhập mật khẩu tổi thiểu 6 ký tự").removeClass("d-none");
    
    bCheck = false;
  }
  return bCheck;
}

//Hàm get Cookie
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

//Hàm setCookie
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//Hàm ẩn các thông báo lỗi
function loginErrorClear() {
  $("#error-username").addClass("d-none");
  $("#error-password").addClass("d-none");
  $("#error-login").addClass("d-none");
}
