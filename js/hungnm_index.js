"use strict"
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

//Gọi hàm khi tải trang
onLandingPageLoading();



/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Xử lý hàm khi tải trang
function onLandingPageLoading() {
  //Tải về sản phầm mục xu hướng phần wirelesss
  loadAllWirelessTrend();

  //Tải về sản phầm mục xu hướng phần earphone
  loadAllEarPhoneTrend();

  //Tải về sản phầm mục xu hướng phần có dây
  loadAllWiredTrend();

  //Tải về các sản phẩm mục Hot
  loadAllProductHot();


}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

//Gọi dữ liệu trả về 8 sản phẩm mục wireless
function loadAllWirelessTrend() {
  $.ajax({
    url: gBASE_URL + "/products/index/" + gWIRELESS_TYPE + "/" + 8,
    method: "GET",
    success: function (pObjRes) {
      //(pObjRes);
      loadDataToWireLessTab(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error('Có lỗi xảy ra, vui lòng thử lại sau!')
    }
  });
}

//Gọi dữ liệu trả về 8 sản phẩm mục earphone
function loadAllEarPhoneTrend() {
  $.ajax({
    url: gBASE_URL + "/products/index/" + gEARPHONE_TYPE + "/" + 8,
    method: "GET",
    success: function (pObjRes) {
      //(pObjRes);
      loadDataToEarPhoneTab(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error('Có lỗi xảy ra, vui lòng thử lại sau!')
    }
  });
}

//Gọi dữ liệu trả về 8 sản phẩm mục wired
function loadAllWiredTrend() {
  $.ajax({
    url: gBASE_URL + "/products/index/" + gWIRED_TYPE + "/" + 8,
    method: "GET",
    success: function (pObjRes) {
      //(pObjRes);
      loadDataToWiredTab(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error('Có lỗi xảy ra, vui lòng thử lại sau!')
    }
  });
}

//Gọi dữ liệu trả về 8 sản phẩm mục Hot
function loadAllProductHot() {
  $.ajax({
    url: gBASE_URL + "/products/Hot/" + 4,
    method: "GET",
    success: function (pObjRes) {
      //(pObjRes);
      //loadDataToHotTab(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error('Có lỗi xảy ra, vui lòng thử lại sau!')
    }
  });
}

//Hiển thị 8 sản phẩm mục wireless
function loadDataToWireLessTab(paramProductLst) {
  var vDivWireLessTrending = $("#div-wireless-trend").html("");
  if (paramProductLst.length > 0) {
    paramProductLst.forEach(productElement => {
      $("<div>", { class: "col-xl-3 col-lg-4 col-md-4 col-12" }).html(RenderProductCard(productElement)).appendTo(vDivWireLessTrending);
    });
  } else {
    $("#div-wireless-trend").addClass("text-danger").html("Hiện không có sản phẩm đang được bán");
  }
  }
  
  //Hiển thị 8 sản phẩm mục wireless
  function loadDataToEarPhoneTab(paramProductLst) {
  var vDivWireLessTrending = $("#div-earphone-trend").html("");
  if (paramProductLst.length > 0) {
    paramProductLst.forEach(productElement => {
      $("<div>", { class: "col-xl-3 col-lg-4 col-md-4 col-12" }).html(RenderProductCard(productElement)).appendTo(vDivWireLessTrending);
    });
  } else {
    $("#div-earphone-trend").addClass("text-danger").html("Hiện không có sản phẩm đang được bán");
  }
  }
  
  //Hiển thị 8 sản phẩm mục wireless
  function loadDataToWiredTab(paramProductLst) {
  var vDivWireLessTrending = $("#div-wired-trend").html("");
  if (paramProductLst.length > 0) {
    paramProductLst.forEach(productElement => {
      $("<div>", { class: "col-xl-3 col-lg-4 col-md-4 col-12" }).html(RenderProductCard(productElement)).appendTo(vDivWireLessTrending);
    });
  } else {
    $("#div-wired-trend").addClass("text-danger").html("Hiện không có sản phẩm đang được bán");
  }
  }
