//RG1
var gSumCash = 0;
var gDiscountCash = 0;
var gCashback = 0;
var gCashsave = 0;
var gVoucherValue = -1;
//RG2
//hàm khi tải trang
onCartInfoLoading();

$("#cart-table-body").on("click", ".btn-plus", function () {
    var vCartItem = ($(this).closest('tr'));
    var vProductId = vCartItem.data("mydata2");
    addProduct(vProductId, 1);
    getCart();
    loadCartToTable(gCartLst);

})

$("#cart-table-body").on("click", ".btn-minus", function () {
    ("ấn");
    var vCartItem = ($(this).closest('tr'));
    var vProductId = vCartItem.data("mydata2");
    minusProduct(vProductId);
    getCart();
    loadCartToTable(gCartLst);

})

$("#cart-table-body").on("click", ".btn-remove", function () {
    
    var vCartItem = ($(this).closest('tr'));
    var vProductId = vCartItem.data("mydata2");
    //(vProductId);
    removeProduct(vProductId);
    getCart();
    loadCartToTable(gCartLst);

})

$("#btn-add-voucher").click(function () {
    onBtnUseVoucherClick();
});


//RG3
function onCartInfoLoading() {
    //getCart();
    $("#inp-qty").val(1);
    //(gCartLst);
    loadCartToTable(gCartLst);
    localStorage.removeItem("voucherValue");
    localStorage.removeItem("voucherCode");
}

//Hàm khi ấn nút áp dụng voucher
function onBtnUseVoucherClick() {
    var vVoucherCode = $("#inp-voucherCode").val().trim();

    if (vVoucherCode != "") {
        $.ajax({
            url: gBASE_URL + "/vouchers/" + vVoucherCode,
            method: "GET",
            async: false,
            success: function (pObjRes) {
                gVoucherValue = pObjRes.voucherValue;
                (gVoucherValue);
                localStorage.setItem("voucherCode", vVoucherCode);
                localStorage.setItem("voucherValue", gVoucherValue);
                loadCartToTable(gCartLst);
            },
            error: function (pXhrObj) {
              toastr.error("Voucher không tồn tại, đã hết hạn hoặc đã được sử dụng, vui lòng kiểm tra lại!");
            },
          });
    }
}
//RG4

function loadCartToTable(gCartLst) {
    gSumCash = 0
    var vDivTableCart = $("#cart-table-body").html("");
    if (gCartLst.length > 0) {
        for (let index = 0; index < gCartLst.length; index++) {
            $.ajax({
                url: gBASE_URL + "/products/" + gCartLst[index].id,
                method: "GET",
                async: "false",
                success: function (pObjRes) {
                    $("<tr>").data("mydata2",gCartLst[index].id).html(`<td class="image" data-title="No"><img src="${pObjRes.photo}"
                    alt="#"></td>
                     <td class="product-des" data-title="Description">
                        <p class="product-name"><a href="product-details.html?id=${pObjRes.id}">${pObjRes.productName}</a></p>
                    </td>
                    <td class="price" data-title="Price"><span>${formatCurrencyVND(pObjRes.byPrice)}</span></td>

                    <td class="qty" data-title="Qty"><!-- Input Order -->
                        <div class="input-group">
                            <div class="button minus btn-minus">
                                <button type="button" class="btn btn-primary btn-number " 
                                    data-type="minus" data-field="quant[3]">
                                    <i class="ti-minus"></i>
                                </button>
                            </div>
                            <input type="text" name="quant[3]" class="input-number" data-min="1"
                                data-max="100" value="${(gCartLst[index].qty)}" disabled>
                            <div class="button plus btn-plus">
                                <button type="button" class="btn btn-primary btn-number" data-type="plus"
                                    data-field="quant[3]">
                                    <i class="ti-plus"></i>
                                </button>
                            </div>
                        </div>
                        <!--/ End Input Order -->
                    </td>
                    <td class="total-amount" data-title="Total"><span class="sum-ele">${formatCurrencyVND(pObjRes.byPrice * gCartLst[index].qty)}</span></td>
                    <td class="action" data-title="Remove"><a ><i
                                class="ti-trash remove-icon btn-remove"></i></a></td>`)
                        .appendTo(vDivTableCart);
                        

                        var tSumCash = (pObjRes.byPrice * gCartLst[index].qty);
                        gSumCash = gSumCash + tSumCash;

                        //Nếu đã áp dụng voucher 
                        if (gVoucherValue != -1) {
                            gDiscountCash = gSumCash*gVoucherValue;
                        } else {                         //Nếu không áp dụng voucher 
                            gDiscountCash = 0;
                        }
                        
                    //Hiển thị vào bảng tạm tính
                    $("#cart-sum").html(formatCurrencyVND(gSumCash));
                    $("#cart-discount").html(formatCurrencyVND(gDiscountCash));
                    $("#cart-cashback").html(formatCurrencyVND(gCashback));
                    $("#cart-save").html(formatCurrencyVND(gCashsave));
                    $("#cart-final").html(formatCurrencyVND(gSumCash - gDiscountCash));
                    
                    //Lưu lại tổng tiền
                    localStorage.setItem("price", gSumCash);
                },
                error: function (pXhrObj) {
                    return 0
                }

            });
        }
        
        /*
        gCartLst.forEach(productEle => {
            
            $.ajax({
                url: gBASE_URL + "/products/" + productEle.id,
                method: "GET",
                async: "false",
                success: function (pObjRes) {
                    $("<tr>").data("mydata2",productEle.id).html(`<td class="image" data-title="No"><img src="${pObjRes.photo}"
                    alt="#"></td>
                     <td class="product-des" data-title="Description">
                        <p class="product-name"><a href="product-details.html?id=${pObjRes.id}">${pObjRes.productName}</a></p>
                    </td>
                    <td class="price" data-title="Price"><span>${formatCurrencyVND(pObjRes.byPrice)}</span></td>

                    <td class="qty" data-title="Qty"><!-- Input Order -->
                        <div class="input-group">
                            <div class="button minus btn-minus">
                                <button type="button" class="btn btn-primary btn-number " 
                                    data-type="minus" data-field="quant[3]">
                                    <i class="ti-minus"></i>
                                </button>
                            </div>
                            <input type="text" name="quant[3]" class="input-number" data-min="1"
                                data-max="100" value="${(productEle.qty)}" disabled>
                            <div class="button plus btn-plus">
                                <button type="button" class="btn btn-primary btn-number" data-type="plus"
                                    data-field="quant[3]">
                                    <i class="ti-plus"></i>
                                </button>
                            </div>
                        </div>
                        <!--/ End Input Order -->
                    </td>
                    <td class="total-amount" data-title="Total"><span class="sum-ele">${formatCurrencyVND(pObjRes.byPrice * productEle.qty)}</span></td>
                    <td class="action" data-title="Remove"><a ><i
                                class="ti-trash remove-icon btn-remove"></i></a></td>`)
                        .appendTo(vDivTableCart);
                        

                        var tSumCash = (pObjRes.byPrice * productEle.qty);
                        gSumCash = gSumCash + tSumCash;
                    //Hiển thị vào bảng tạm tính
                    $("#cart-sum").html(formatCurrencyVND(gSumCash));
                    $("#cart-discount").html(formatCurrencyVND(gDiscountCash));
                    $("#cart-cashback").html(formatCurrencyVND(gCashback));
                    $("#cart-save").html(formatCurrencyVND(gCashsave));
                    $("#cart-final").html(formatCurrencyVND(gSumCash - gDiscountCash));
                },
                error: function (pXhrObj) {
                    return 0
                }

            });

           

        });*/
    } else {
        $("#cart-table-body").addClass("text-danger").html("Chưa có sản phẩm trong giỏ hàng");
    }


}



