/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */


// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng



/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

loadUserInfo();


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm tải và hiển thị dữ liệu cá nhân
function loadUserInfo() {
  gLoginSesstion = JSON.parse(gIsLogin);
  gtoken = gLoginSesstion.token;
  gRole = gLoginSesstion.authorities;
  gId = gLoginSesstion.id;
  gCusId = gLoginSesstion.customerId;
  (gLoginSesstion);
  showUserInfo(gId);
  showCustomerInfo(gId);
  loadOrderById(gCusId)
}

function showUserInfo(userId) {
  $.ajax({
    url: gBASE_URL + "/users/detail/" + userId,
    method: "GET",
    async: false,
    success: function (pObjRes) {
      (pObjRes);
    },
    error: function (pXhrObj) {
      ("Not found");
    },
  });
}

function showCustomerInfo(userId) {
  $.ajax({
    url: gBASE_URL + "/customers/user/" + userId,
    method: "GET",
    async: false,
    success: function (pObjRes) {
      (pObjRes);
    },
    error: function (pXhrObj) {
      ("Not found");
    },
  });
}

function loadOrderById(gCusId) {
  $.ajax({
    url: gBASE_URL + "/orders/customer/" + gCusId,
    method: "GET",
    async: false,
    success: function (pObjRes) {
      (pObjRes);
    },
    error: function (pXhrObj) {
      ("Not found");
    },
  });
}