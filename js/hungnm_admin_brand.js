/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Dummy Data

var gBrandId = 0;
var gImgCampaignUrl = "";
var gCountriesLst = [];
//Các cột của bảng
const gBRAND_COLS = [
  "id",
  "id",
  "brandName",
  "brandCountryId"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gBRAND_COLS_ACTION_COL = 0;
const gBRAND_COLS_ID_COL = 1;
const gBRAND_COLS_NAME_COL = 2;
const gBRAND_COLS_COUNTRY_COL = 3;

var gBrandTable = $("#brand-table").DataTable({
  responsive: true,
  lengthChange: false,
  autoWidth: false,
  buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
  columns: [
    { data: gBRAND_COLS[gBRAND_COLS_ACTION_COL] },
    { data: gBRAND_COLS[gBRAND_COLS_ID_COL] },
    { data: gBRAND_COLS[gBRAND_COLS_NAME_COL] },
    { data: gBRAND_COLS[gBRAND_COLS_COUNTRY_COL] },
  ],
  columnDefs: [
    {
      target: gBRAND_COLS_ACTION_COL,
      render: renderAction
    },
    {
      target: gBRAND_COLS_COUNTRY_COL,
      render: renderCountries
    }
  ],
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

//OnpageLoading

//Gán sự kiện khi ấn nút thêm mới campain
$("#btn-create-brand").click(function () {
  clearData();
  $("#add-brand-modal").modal();
});


//Gán sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
$("#btn-modal-add-brand").click(function () {
  onBtnAddBrandModalClick();
});

//Gán sự kiện khi ấn vào biểu tượng kính lúp trên bảng
$("#brand-table").on("click", ".btn-edit", function () {
  onIconInfoInRowClick(this);
});

//Gán sự kiện khi ấn nút cập nhật thông tin trên modal info
$("#btn-modal-edit-brand").click(function () {
  onBtnUpdateBrandClick();
});

//Gán sự kiện khi ấn vào biểu tượng thùng rác
$("#brand-table").on("click", ".btn-delete", function () {
  onDeleteBrandClick(this);
});

$("#btn-modal-confirm-delete-brand").click(function () {
  onBtnDeleteBrandClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Tải toàn bộ quốc gia
loadAllCountry();

//Tải toàn bộ khách hàng về
loadAllBrand();


//Hàm xử lý sự kiện khi ấn vào icon kính lúp
function onIconInfoInRowClick(elementRow) {
  $("#info-brand-modal").modal();
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gBrandTable.row(vTableRow).data();
  gBrandId = vRowData.id;
  //(gCampaignId);
  showBrandInfoToForm(vRowData);
}
//Xử lý sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
function onBtnAddBrandModalClick() {
  var vObj = {
    brandName: $("#modal-add-brandName").val().trim(),
    brandCountryId: $("#modal-add-brandCountryId").val(),
    brandDescription: $("#modal-add-brandDescription").val().trim(),
  };

  if (validateBrand(vObj)) {
    $.ajax({
      url: gBASE_URL + "/brands",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Thêm mới thương hiệu thành công");
        loadAllBrand();
        $("#add-brand-modal").modal("hide");
      },
      error: function (ajaxContext) {
        var errorLog = JSON.parse(ajaxContext.responseText)
        toastr.error(errorLog.errors[0]);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút cập nhật thông tin
function onBtnUpdateBrandClick() {
  var vObj = {
    brandName: $("#modal-info-brandName").val().trim(),
    brandCountryId: $("#modal-info-brandCountryId").val(),
    brandDescription: $("#modal-info-brandDescription").val().trim(),
  };

  if (validateBrand(vObj)) {
    $.ajax({
      url: gBASE_URL + "/brands/" + gBrandId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Cập nhật thương hiệu thành công");
        loadAllBrand();
        $("#info-brand-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//hàm xử lý sự kiện khi ấn icon thùng rác
function onDeleteBrandClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gBrandTable.row(vTableRow).data();
  gBrandId = vRowData.id;
  $("#delete-brand-modal").modal();
}


//Hàm xử lý sự kiện khi ấn nút xóa khách hàng
function onBtnDeleteBrandClick() {
  $.ajax({
    url: gBASE_URL + "/brands/" + gBrandId,
    type: "DELETE",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Xóa thương hiệu thành công");
      loadAllBrand();
      $("#delete-brand-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
    },
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm gọi api lấy ra countries
function loadAllCountry() {
  $.ajax({
    url: gBASE_URL + "/countries",
    method: "GET",
    async: false,
    success: function (pObjRes) {
      gCountriesLst = pObjRes;
      loadDataToCountrySelect(gCountriesLst);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
    },
  });
}

function loadAllBrand() {
  $.ajax({
    url: gBASE_URL + "/brands",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showBrandToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      //showCustomerToTable(gCustomerList);
    },
  });
}

//Hàm hiển thị dữ liệu trả về lên table
function showBrandToTable(data) {
  gStt = 0;
  gBrandTable.clear();
  gBrandTable.rows.add(data);
  gBrandTable.draw();
  gBrandTable.buttons().container().appendTo("#brand-table");
}

//Hàm kiểm tra dữ liệu Khách hàng
function validateBrand(paramObj) {
  if (paramObj.brandName == "") {
    toastr.warning("Tên thương hiệu không được để trống");
    return false;
  }

  if (paramObj.brandDescription == "") {
    toastr.warning("Mô tả thương hiệu không được để trống");
    return false;
  }

  return true;
}

//Xóa dữ liệu modal thêm mới
function clearData() {
  $("#modal-add-brandName").val("");
  $("#modal-add-brandCountryId").val(1);
  $("#modal-add-brandDescription").val("");
}

//Hàm hiển thị dữ liệu khách hàng qua form
function showBrandInfoToForm(paramData) {
  $("#modal-info-brandName").val(paramData.brandName);
  $("#modal-info-brandCountryId").val(paramData.brandCountryId);
  $("#modal-info-brandDescription").val(paramData.brandDescription);
  
}

//Đổ dữ liệu vào select country
function loadDataToCountrySelect(paramCountriesData) {
  var vCountriesSelectElement = $("#modal-add-brandCountryId");
  for (i = 0; i < paramCountriesData.length; i++) {
    let bCountryOption = $("<option/>");
    bCountryOption.prop("value", paramCountriesData[i].id);
    bCountryOption.prop("text", paramCountriesData[i].countryName);
    vCountriesSelectElement.append(bCountryOption);
  };

   
  var vProvinceSelectElement2 = $("#modal-info-brandCountryId");
  for (i = 0; i < paramCountriesData.length; i++) {
    let bProvinceOption = $("<option/>");
    bProvinceOption.prop("value", paramCountriesData[i].id);
    bProvinceOption.prop("text", paramCountriesData[i].countryName);
    vProvinceSelectElement2.append(bProvinceOption);
  };
}