//RG1
var gDayLst = ["",];
var gDayCount = [0,];
var gDayMontLst = [];
var gDayCountMonthLst = [];


var areaChartData1 = {
    labels: gDayLst,
    datasets: [
      {
        label: "Đơn hàng",
        backgroundColor: "rgba(60,141,188,0.9)",
        borderColor: "rgba(60,141,188,0.8)",
        pointRadius: false,
        pointColor: "#3b8bba",
        pointStrokeColor: "rgba(60,141,188,1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(60,141,188,1)",
        data: gDayCount,
      },
    ],
  };
  
  var barChartCanvas1 = $("#barChart").get(0).getContext("2d");
  var barChartData1 = $.extend(true, {}, areaChartData1);
  //var temp0 = areaChartData.datasets[0]
  //barChartData.datasets[0] = temp0
  
  var barChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    datasetFill: false,
  };
  
  new Chart(barChartCanvas1, {
    type: "bar",
    data: barChartData1,
    options: barChartOptions,
  });
  
  //Bar char 2
  var areaChartData2 = {
    labels: gDayMontLst,
    datasets: [
      {
        label: "Đơn hàng",
        backgroundColor: "rgba(60,141,188,0.9)",
        borderColor: "rgba(60,141,188,0.8)",
        pointRadius: false,
        pointColor: "#3b8bba",
        pointStrokeColor: "rgba(60,141,188,1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(60,141,188,1)",
        data: gDayCountMonthLst,
      },
    ],
  };
  
  var barChartOptions2 = {
    responsive: true,
    maintainAspectRatio: false,
  };
  
  var barChartCanvas2 = $("#barChart2").get(0).getContext("2d");
  var barChartData2 = $.extend(true, {}, areaChartData2);
  //var temp0 = areaChartData.datasets[0]
  //barChartData.datasets[0] = temp0
  
  new Chart(barChartCanvas2, {
    type: "line",
    data: barChartData2,
    options: barChartOptions2,
  });

//RG2
//OnpageLoading
loadNewCustomerToday();

loadProductSaleStartToday();

loadNewOrderToday();

loadNonCompleteOrderToday();

loadProductOutStock();

loadGrossToday();

loadCampaignExpire();

loadTotalView();

loadOrderInWeek();

loadOrderInMonth();
//RG3

//RG4

//Hàm gọi APi lấy danh sách khách hàng mới đăng ký hôm nay
function loadNewCustomerToday() {
    $.ajax({
        url: gBASE_URL + "/customers/today",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            $("#text-newCustomer").html(pObjRes.length);
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

//Hàm gọi APi lấy danh sách sản phẩm bắt đầu bán hôm nay
function loadProductSaleStartToday() {
    $.ajax({
        url: gBASE_URL + "/products/today",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            $("#text-newProduct").html(pObjRes.length);
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

//Hàm gọi APi lấy danh sách đơn hàng bán hôm nay
function loadNewOrderToday() {
    $.ajax({
        url: gBASE_URL + "/orders/today",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            $("#text-newOrders").html(pObjRes.length);
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

//Hàm gọi APi lấy danh sách đơn hàng chưa hoàn thành
function loadNonCompleteOrderToday() {
    $.ajax({
        url: gBASE_URL + "/orders/nonComplete",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            $("#text-nonCom").html(pObjRes.length);
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

//Hàm gọi APi lấy danh sách đơn hàng sắp hết hàng (Số lượng trong kho < 50)
function loadProductOutStock() {
    $.ajax({
        url: gBASE_URL + "/products/outStock",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            $("#text-outStock").html(pObjRes.length);
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

//Hàm gọi APi lấy doanh thu ngày hôm nay
function loadGrossToday() {
    $.ajax({
        url: gBASE_URL + "/orders/today",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            var vGrossToday = 0;
            pObjRes.forEach(element => {
                vGrossToday += element.price;
            });

            $("#text-gross").html(formatCurrencyVND(vGrossToday));
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

//Hàm gọi APi lấy CTKM sắp hết  hạn, endate trong vòng 7 ngày
function loadCampaignExpire() {
    $.ajax({
        url: gBASE_URL + "/campaigns/expire",
        method: "GET",
        async: false,
        success: function (pObjRes) {

            $("#text-campaingExp").html(pObjRes.length);
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

//Hàm gọi APi lấy CTKM sắp hết  hạn, endate trong vòng 7 ngày
function loadTotalView() {
    $.ajax({
        url: gBASE_URL + "/products/totalView",
        method: "GET",
        async: false,
        success: function (pObjRes) {

            $("#text-totalView").html(formatNumber(pObjRes, '.', ',', 0));
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

//Hàm gọi APi lấy danh sách khách hàng mới đăng ký hôm nay
function loadOrderInWeek() {
    $.ajax({
        url: gBASE_URL + "/orders/report7day",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            drawChartInWeek(pObjRes);
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

function drawChartInWeek(data) {
    console.log(data);
    data.forEach(element => {
        gDayLst.push(element.ngay);
        gDayCount.push(element.dem);
    });

    var barChartCanvas1 = $("#barChart").get(0).getContext("2d");
    var barChartData1 = $.extend(true, {}, areaChartData1);
    //var temp0 = areaChartData.datasets[0]
    //barChartData.datasets[0] = temp0
    
    var barChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      datasetFill: false,
    };
    
    new Chart(barChartCanvas1, {
      type: "bar",
      data: barChartData1,
      options: barChartOptions,
    });
}

//Hàm gọi APi lấy danh sách khách hàng mới đăng ký hôm nay
function loadOrderInMonth() {
    $.ajax({
        url: gBASE_URL + "/orders/report30day",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            drawChartInMonth(pObjRes);
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

function drawChartInMonth(data) {
    data.forEach(element => {
        gDayMontLst.push(element.ngay);
        gDayCountMonthLst.push(element.dem);
    });

    var barChartOptions2 = {
        responsive: true,
        maintainAspectRatio: false,
      };
      
      var barChartCanvas2 = $("#barChart2").get(0).getContext("2d");
      var barChartData2 = $.extend(true, {}, areaChartData2);
      //var temp0 = areaChartData.datasets[0]
      //barChartData.datasets[0] = temp0
      
      new Chart(barChartCanvas2, {
        type: "line",
        data: barChartData2,
        options: barChartOptions2,
      });
}