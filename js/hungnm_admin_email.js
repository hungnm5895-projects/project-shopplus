/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

var gSumNote = $("#summernote").summernote();
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

$("#btn-sendEmail").click(function () {
  onBtnSendEmailClick();
});

$("#btn-Breset").click(function () {
  gSumNote.val("");
  $("#summernote").summernote(`destroy`);
  $("#summernote").summernote();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Hàm xử lý sự kiện khi ấn nút send email
function onBtnSendEmailClick() {
  var vObj = {
    emailContent: gSumNote.val(),
    emailSubject: $("#input-add-subject").val().trim(),
    emailList: $("#input-add-customerLst").val().split(";")
  };

  if (true) {
    $.ajax({
      url: gBASE_URL + "/email/send",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Gửi email thành công");
      },
      error: function (ajaxContext) {
        var errorLog = JSON.parse(ajaxContext.responseText)
        toastr.error(errorLog.errors[0]);
      },
    });
  }
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
