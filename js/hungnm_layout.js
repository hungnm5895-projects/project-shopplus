//Tải về các sản phẩm mục Sale
loadAllProductSale();

//Tải về các sản phẩm mục Best Saller
loadAllProductBestSaller();

//Tải về các sản phẩm mục Top View
loadAllProductTopView();

$("#btn-follow").click(function () {
  var vEmailObj = {
    email : $("#inp-email-fl").val().trim()
  }

  if (validateEmail(vEmailObj.email)) {
    $.ajax({
      url: gBASE_URL + "/followers",
      method: "POST",
      contentType: "application/json",
      data: JSON.stringify(vEmailObj),
      success: function (pObjRes) {
        toastr.success('Theo dõi thành công, các thông tin mới sẽ được gửi qua mail đã đăng ký!')
        $("#inp-email-fl").val("");
      },
      error: function (pXhrObj) {
        toastr.error('Có lỗi xảy ra, vui lòng thử lại sau!')
      }
    });
  } else {
    toastr.warning("Email sai định dạng, vui lòng kiểm tra lại");
  }

});

//Gọi dữ liệu trả về 8 sản phẩm mục Sale
function loadAllProductSale() {
$.ajax({
  url: gBASE_URL + "/products/Sale/",
  method: "GET",
  success: function (pObjRes) {
    (pObjRes);
    loadDataToSaleTab(pObjRes);
  },
  error: function (pXhrObj) {
    toastr.error('Có lỗi xảy ra, vui lòng thử lại sau!')
  }
});
}

//Gọi dữ liệu trả về 8 sản phẩm mục Best Saller
function loadAllProductBestSaller() {
$.ajax({
  url: gBASE_URL + "/products/BestSeller",
  method: "GET",
  success: function (pObjRes) {
    (pObjRes);
    loadDataToBestSellerTab(pObjRes);
  },
  error: function (pXhrObj) {
    toastr.error('Có lỗi xảy ra, vui lòng thử lại sau!')
  }
});
}

//Gọi dữ liệu trả về 8 sản phẩm mục Best Saller
function loadAllProductTopView() {
$.ajax({
  url: gBASE_URL + "/products/TopView",
  method: "GET",
  success: function (pObjRes) {
    (pObjRes);
    loadDataToTopViewrTab(pObjRes);
  },
  error: function (pXhrObj) {
    toastr.error('Có lỗi xảy ra, vui lòng thử lại sau!')
  }
});
}



//Hiển thị 8 sản phẩm mục Sale
function loadDataToSaleTab(paramProductLst) {
var vDivWireLessTrending = $("#lst-sale").html("");
if (paramProductLst.length > 0) {
  paramProductLst.forEach(productElement => {
    $("<div>", { class: "single-list" }).html(RenderProductMiniCard(productElement))
      .appendTo(vDivWireLessTrending);
  });
} else {
  $("#lst-sale").addClass("text-danger").html("Hiện không có sản phẩm đang được bán");
}
}

//Hiển thị 8 sản phẩm mục Best Sale
function loadDataToBestSellerTab(paramProductLst) {
var vDivWireLessTrending = $("#lst-best").html("");
if (paramProductLst.length > 0) {
  paramProductLst.forEach(productElement => {
    $("<div>", { class: "single-list" }).html(RenderProductMiniCard(productElement))
      .appendTo(vDivWireLessTrending);
  });
} else {
  $("#lst-best").addClass("text-danger").html("Hiện không có sản phẩm đang được bán");
}
}

//Hiển thị 8 sản phẩm mục Top View
function loadDataToTopViewrTab(paramProductLst) {
var vDivWireLessTrending = $("#lst-view").html("");
if (paramProductLst.length > 0) {
  paramProductLst.forEach(productElement => {
    $("<div>", { class: "single-list" }).html(RenderProductMiniCard(productElement))
      .appendTo(vDivWireLessTrending);
  });
} else {
  $("#lst-view").addClass("text-danger").html("Hiện không có sản phẩm đang được bán");
}
}
