/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Dummy Data
var gLoyaltyList = JSON.parse(
  `[{"id":1,"customerId":3,"rank":"bạch kim","ammountBuy":1500000},{"id":2,"customerId":4,"rank":"Thường","ammountBuy":0},{"id":3,"customerId":5,"rank":"Vip","ammountBuy":11500000},{"id":4,"customerId":6,"rank":"Vip","ammountBuy":111500000}]`);

var gStt = 0;
var gLoyaltyId = 0;

//Các cột của bảng
const gLOYALTY_COLS = [
  "id",
  "id",
  "customerId",
  "rank",
  "ammountBuy"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCOUNTRY_COLS_ACTION_COL = 0;
const gCOUNTRY_COLS_ID_COL = 1;
const gCOUNTRY_COLS_CUSTOMER_COL = 2;
const gCOUNTRY_COLS_RANK_COL = 3;
const gCOUNTRY_COLS_AMMOUNT_BUY_COL = 4;


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

var gLoyaltyTable = $("#loyalty-table")
  .DataTable({
    responsive: true,
    lengthChange: false,
    autoWidth: false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    columns: [
      { data: gLOYALTY_COLS[gCOUNTRY_COLS_ACTION_COL] },
      { data: gLOYALTY_COLS[gCOUNTRY_COLS_ID_COL] },
      { data: gLOYALTY_COLS[gCOUNTRY_COLS_CUSTOMER_COL] },
      { data: gLOYALTY_COLS[gCOUNTRY_COLS_RANK_COL] },
      { data: gLOYALTY_COLS[gCOUNTRY_COLS_AMMOUNT_BUY_COL] }

    ],
    columnDefs: [
      {
        target: gCOUNTRY_COLS_ACTION_COL,
        render: renderAction,
      },
      {
        target: gCOUNTRY_COLS_RANK_COL,
        render: renderRank,
      }
    ],
  })

//OnpageLoading

$("#btn-create-country").click(function () {
  clearData();
  $("#add-country-modal").modal();
});

//Gán sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
$("#btn-modal-add-country").click(function () {
  onBtnAddCountryModalClick();
});

//Gán sự kiện khi ấn vào dòng
//Gán sự kiện khi ấn vào dòng trên table
$("#country-table").on("click", ".btn-edit", function () {
  onCountryRowClick(this);
});

//Gán sự kiện khi ấn nút cập nhật thông tin trên modal info
$("#btn-modal-edit-country").click(function () {
  onBtnUpdateCountryClick();
});

$("#country-table").on("click", ".btn-delete", function () {
  onDeleteCountryClick(this);
});


$("#btn-modal-confirm-delete-country").click(function () {
  onBtnDeleteCountryConfirmClick();
});

$("#select-rank").on("change",function () {
  onSelectRankChange();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Lấy về danh sách thành phố
//Tải toàn bộ khách hàng về
loadAllLoyalty();

//hàm xử lý sự kiện khi thay đổi select rank
function onSelectRankChange() {
  var vRankSelect = $("#select-rank").val();
  if (vRankSelect < 0) {
    $("#btn-export").prop("href", gBASE_URL+"/export/customers/excel");
  } else {
    $("#btn-export").prop("href", gBASE_URL+"/export/customers/rank/"+ vRankSelect);
  }
}


//hàm xử lý sự kiện khi ấn icon thùng rác
function onDeleteCountryClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gLoyaltyTable.row(vTableRow).data();
  gLoyaltyId = vRowData.id;
  $("#delete-country-modal").modal();
}

//Hàm xử lý sự kiện khi ấn vào icon kính lúp
function onCountryRowClick(elementRow) {
  $("#info-country-modal").modal();
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gLoyaltyTable.row(vTableRow).data();
  gLoyaltyId = vRowData.id;
  //(gVoucherId);
  showCountryInfoToForm(vRowData);
}
//Xử lý sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
function onBtnAddCountryModalClick() {
  var vObj = {
    countryCode: $("#modal-add-countryCode").val().trim(),
    countryName: $("#modal-add-countryName").val().trim(),
  };
  (vObj);
  if (validateCountry(vObj)) {
    $.ajax({
      url: gBASE_URL + "/vouchers",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Thêm mới khách hàng thành công");
        loadAllLoyalty();
        $("#add-customer-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút cập nhật thông tin
function onBtnUpdateCountryClick() {
  var vObj = {
    voucherCode: $("#modal-info-countryCode").val().trim(),
    voucherName: $("#modal-info-countryName").val().trim(),
  };

  if (validateCountry(vObj)) {
    $.ajax({
      url: gBASE_URL + "/countries/" + gLoyaltyId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Cập nhật Quốc gia thành công");
        loadAllLoyalty();
        $("#info-country-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút xóa khách hàng
function onBtnDeleteCountryConfirmClick() {
  $.ajax({
    url: gBASE_URL + "/countries/" + gLoyaltyId,
    type: "DELETE",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Xóa quốc gia thành công");
      loadAllLoyalty();
      $("#delete-country-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
    },
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function loadAllLoyalty() {
  $.ajax({
    url: gBASE_URL + "/loyaltys",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showLoyaltyLstToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      showLoyaltyLstToTable(gLoyaltyList);
    },
  });
}

//Hàm hiển thị dữ liệu trả về lên table
function showLoyaltyLstToTable(data) {
  gStt = 0;
  gLoyaltyTable.clear();
  gLoyaltyTable.rows.add(data);
  gLoyaltyTable.draw();
}

//Hàm kiểm tra dữ liệu Khách hàng
function validateCountry(paramObj) {
  if (paramObj.countryCode == "") {
    toastr.warning("Mã Quốc Gia không được để trống");
    return false;
  }

  if (paramObj.countryName == "") {
    toastr.warning("Tên Quốc Gia không được để trống");
    return false;
  }

  return true;
}

//Xóa dữ liệu modal thêm mới
function clearData() {
  $("#modal-add-countryName").val("");
  $("#modal-add-countryCode").val("");
}

//Hàm hiển thị dữ liệu khách hàng qua form
function showCountryInfoToForm(paramData) {
  $("#modal-info-id").val(paramData.id);
  $("#modal-info-countryName").val(paramData.countryName);
  $("#modal-info-countryCode").val(paramData.countryCode);
}

function renderRank(data) {
  var type;
  switch (data) {
    case 0:
      type = "Thường";
      break;
    case 1:
      type = "Vip";
      break;
    case 2:
      type = "Bạc";
      break;
    case 3:
      type = "Vàng";
      break;
    case 4:
      type = "Bạch Kim";
      break;
  }

  return type;
}