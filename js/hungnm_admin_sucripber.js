/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Dummy Data
var SubList = JSON.parse(
  `[{"email":"email1@gmail.com","date":"01/01/2023","id":1},{"email":"email1@gmail.com","date":"01/01/2023","id":2},{"email":"email1@gmail.com","date":"01/01/2023","id":3},{"email":"email1@gmail.com","date":"01/01/2023","id":4}]`
);

var gStt = 0;
var gSubId = 0;

//Các cột của bảng
const gCOUNTRY_COLS = [
  "id",
  "id",
  "email",
  "createDate"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gSUB_COLS_ACTION_COL = 0;
const gSUB_COLS_ID_COL = 1;
const gSUB_COLS_MAIL_COL = 2;
const gSUB_COLS_DATE_COL = 3;


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

var gSubcriberTable = $("#subcriber-table")
  .DataTable({
    responsive: true,
    lengthChange: false,
    autoWidth: false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    columns: [
      { data: gCOUNTRY_COLS[gSUB_COLS_ACTION_COL] },
      { data: gCOUNTRY_COLS[gSUB_COLS_ID_COL] },
      { data: gCOUNTRY_COLS[gSUB_COLS_MAIL_COL] },
      { data: gCOUNTRY_COLS[gSUB_COLS_DATE_COL] }
    ],
    columnDefs: [
      {
        target: gSUB_COLS_ACTION_COL,
        render: renderAction,
      },
      {
        target: gSUB_COLS_DATE_COL,
        render: renderFullDateTime,
      }
    ],
  })

//OnpageLoading

$("#btn-create-subcriber").click(function () {
  clearData();
  $("#add-subcriber-modal").modal();
});

//Gán sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
$("#btn-modal-add-subcriber").click(function () {
  onBtnAddSubcriberModalClick();
});

//Gán sự kiện khi ấn vào dòng
//Gán sự kiện khi ấn vào dòng trên table
$("#subcriber-table").on("click", ".btn-edit", function () {
  onSubRowClick(this);
});

//Gán sự kiện khi ấn nút cập nhật thông tin trên modal info
$("#btn-modal-edit-subcriber").click(function () {
  onBtnUpdateSubClick();
});

$("#subcriber-table").on("click", ".btn-delete", function () {
  onDeleteSubcriberClick(this);
});


$("#btn-modal-confirm-delete-subcriber").click(function () {
  onBtnDeleteSubcriberConfirmClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Lấy về danh sách thành phố
//Tải toàn bộ khách hàng về
loadAllSubcriber();

//hàm xử lý sự kiện khi ấn icon thùng rác
function onDeleteSubcriberClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gSubcriberTable.row(vTableRow).data();
  gSubId = vRowData.id;
  $("#delete-subcriber-modal").modal();
}

//Hàm xử lý sự kiện khi ấn vào icon kính lúp
function onSubRowClick(elementRow) {
  $("#info-subcriber-modal").modal();
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gSubcriberTable.row(vTableRow).data();
  gSubId = vRowData.id;
  //(gVoucherId);
  showSubcriberInfoToForm(vRowData);
}
//Xử lý sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
function onBtnAddSubcriberModalClick() {
  var vObj = {
    email: $("#modal-add-email").val().trim(),
  };
  (vObj);
  if (validateSubcriber(vObj)) {
    $.ajax({
      url: gBASE_URL + "/followers",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Thêm mới Email theo dõi thành công");
        loadAllSubcriber();
        $("#add-subcriber-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút cập nhật thông tin
function onBtnUpdateSubClick() {
  var vObj = {
    email: $("#modal-info-email").val().trim(),
  };

  if (validateSubcriber(vObj)) {
    $.ajax({
      url: gBASE_URL + "/followers/" + gSubId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Cập nhật khách hàng đăng ký thành công");
        loadAllSubcriber();
        $("#info-subcriber-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút xóa khách hàng
function onBtnDeleteSubcriberConfirmClick() {
  $.ajax({
    url: gBASE_URL + "/followers/" + gSubId,
    type: "DELETE",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Xóa khách hàng theo dõi thành công");
      loadAllSubcriber();
      $("#delete-subcribers-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
    },
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function loadAllSubcriber() {
  $.ajax({
    url: gBASE_URL + "/followers",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showSubcriberLstToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      showSubcriberLstToTable(SubList);
    },
  });
}

//Hàm hiển thị dữ liệu trả về lên table
function showSubcriberLstToTable(data) {
  gStt = 0;
  gSubcriberTable.clear();
  gSubcriberTable.rows.add(data);
  gSubcriberTable.draw();
}

//Hàm kiểm tra dữ liệu Khách hàng
function validateSubcriber(paramObj) {
  if (paramObj.email == "") {
    toastr.warning("Địa chỉ email không được để trống");
    return false;
  }

  if (!validateEmail(paramObj.email) ) {
    toastr.warning("Địa chỉ email không hợp lệ");
    return false;
  }

    
  return true;
}

//Xóa dữ liệu modal thêm mới
function clearData() {
  $("#modal-add-email").val("");
}

//Hàm hiển thị dữ liệu khách hàng qua form
function showSubcriberInfoToForm(paramData) {
  $("#modal-info-id").val(paramData.id);
  $("#modal-info-email").val(paramData.email);
  $("#modal-info-date").val(renderDateTimeInput(paramData.createDate));
}
