
//Các loại biến cơ bản

//URL
var gBASE_URL = "http://localhost:8080";
var gURL_PHOTO = gBASE_URL + "/downloadFile/";

//List 
var gProvinceLst = [];

//Loại
var gWIRELESS_TYPE = 0;
var gEARPHONE_TYPE = 1;
var gWIRED_TYPE = 2;


//Hàm RenderSTT của các bảng
function renderStt() {
  gStt++;
  return gStt;
}

function getText(paramData) {
  if (paramData == "") {
    return "Đang cập nhật";
  } else return paramData;
}

//Hàm render nút action
function renderAction(params) {
  return `<i class="ti-search btn-edit mr-2 text-info" style="cursor: pointer"></i><i class="ti-trash text-danger btn-delete" style="cursor: pointer"></i>`;
}

//hàm render trạng thái sử dụng của voucher
function renderUseable(data) {
  if (data == 1) {
    return `<span class="text-success">Chưa sử dụng</span>`;
  } else {
    return `<span class="text-secondary">Đã sử dụng</span>`;
  }
}

//hàm render trạng thái sử dụng của voucher
function renderVoucherValue(data) {
  var vVal = "";
  if (data <= 1) {
    return "Giảm " + data * 100 + " %";
  } else {
    return "Giảm " + data + " VNĐ";
  }
}

//hàm render loại voucher
function renderVoucherType(data) {
  if (data == 1) {
    return "Giảm giá trực tiếp";
  } else {
    return "Giảm giá theo phần trăm";
  }
}

//hàm render thành phố
function renderProvince(data) {
  for (let index = 0; index < gProvinceLst.length; index++) {
    if (data == gProvinceLst[index].id) {
      return gProvinceLst[index].name;
    }
  }
}

//hàm render quốc gia
function renderCountries(data) {
  for (let index = 0; index < gCountriesLst.length; index++) {
    if (data == gCountriesLst[index].id) {
      return gCountriesLst[index].countryName;
    }
  }
}

//hàm render quốc gia
function renderBrand(data) {
  for (let index = 0; index < gBrandLst.length; index++) {
    if (data == gBrandLst[index].id) {
      return gBrandLst[index].brandName;
    }
  }
}

//hàm render quốc gia
function renderBrandCountry(data) {
  for (let index = 0; index < gBrandLst.length; index++) {
    if (data == gBrandLst[index].id) {
      return renderCountries(gBrandLst[index].brandCountryId);
    }
  }
}

//Hàm Trả render DateTime
function renderDateTime(data) {
  return dayjs(data).format('DD/MM/YYYY')
}

//Hàm Trả render DateTime
function renderDateTimeInput(data) {
  return dayjs(data).format('YYYY-MM-DD')
}

//Hàm Trả render Full DateTime
function renderFullDateTime(data) {
  return dayjs(data).format('YYYY-MM-DD hh:mm:ss');

}

/* Hàm kiểm tra email
    Input: email cần kiểm tra
    Output: trả về giấ trị true nếu mail hợp lệ, false nếu mail k hợp lệ
    */
function validateEmail(paramsEmail) {
  var vResuilt = false;
  var vRigexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (paramsEmail.match(vRigexEmail)) {
    vResuilt = true;
  }
  return vResuilt;
}

/* Hàm trả về link photo
Input: src của photo
Output: nếu src rỗng trả về src mặc định, nếu không trả về src truyền về
*/
function renderPhoto(srcData) {
  if (srcData == "" || srcData == null) {
    var bUrl = gURL_PHOTO + "banner6.jpg";
    return `<img src=${bUrl} width=100%>`
  } else return `<img src=${srcData} width=100% alt=${srcData}>`
}


/* Hàm trả về chuỗi là loại tai nghe 0: wireliess; 1: earphone; 2: wired	
Input: src của photo
Output: nếu src rỗng trả về src mặc định, nếu không trả về src truyền về
*/
function renderProductType(data) {
  var type;
  switch (data) {
    case 0:
      type = "Tai nghe không dây";
      break;
    case 1:
      type = "Tai nghe chụp đầu";
      break;
    case 2:
      type = "Tai nghe có dây";
      break;
  }

  return type;
}


//Render hot
function renderHotProduct(data) {
  if (data == 1) {
    return `<span class="text-danger">HOT</span>`
  } else {
    return "Thường"
  }
}

//Render hot
function renderTrendProduct(data) {
  if (data == 1) {
    return `<span class="text-success">Xu Hướng</span>`
  } else {
    return "Thường"
  }
}

//Rander sao
function renderStar(data) {
  switch (data) {
    case 0:
      return `Chưa có đánh giá`;
    case 1:
      return `<i class="ti-star"></i>`;
    case 2:
      return `<i class="ti-star"></i><i class="ti-star"></i>`;
    case 3:
      return `<i class="ti-star"></i><i class="ti-star"></i><i class="ti-star"></i>`;
    case 4:
      return `<i class="ti-star"></i><i class="ti-star"></i><i class="ti-star"></i><i class="ti-star"></i>`;
    case 5:
      return `<i class="ti-star"></i><i class="ti-star"></i><i class="ti-star"></i><i class="ti-star"></i><i class="ti-star"></i>`;
  }
}

//Render hot
function renderSeenNoti(data) {
  if (data == 0) {
    return `Chưa xem`
  } else {
    return "Đã xem"
  }
}


//RenderProductCard
function RenderProductCard(paramData) {


  return `<div class="single-product">
    <div class="product-img">
        <a href="product-details.html?id=${paramData.id}">
            <img class="default-img"
                src="${paramData.photo}"
                alt="#">
            <img class="hover-img"
                src="${paramData.photo}"
                alt="#">
                ${renderProductTag(paramData)}
        </a>
        <div class="button-head">
            <div class="product-action">
                <a data-toggle="modal" data-target="#exampleModal"
                    title="Quick View" href="#"><i
                        class=" ti-eye"></i><span>Quick Shop</span></a>

                <a title="Compare" href="#"><i
                        class="ti-bar-chart-alt"></i><span>Add to
                        Compare</span></a>
            </div>
            <div class="product-action-2">
                <a title="Add to cart" ><i class="ti-shopping-cart h3 btn-card" data-mydata="${paramData.id}"></i></a>
            </div>
        </div>
    </div>
    <div class="product-content">
        <h3><a href="product-details.html?id=${paramData.id}">${paramData.productName}</a></h3>
        <div class="product-price">
            <span class="old">${formatCurrencyVND(paramData.productScale)}</span>
            <span class="text-danger font-weight-bold">${formatCurrencyVND(paramData.byPrice)}</span>
        </div>
    </div>
</div>`;
}

//hàm chuyển đổi giá từ int sang định dạng VNĐ
function formatCurrencyVND(number) {
  const formatter = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND'
  });
  return formatter.format(number);
}

//hàm hiển thị tag cho Product card
function renderProductTag(productData) {
  if (productData.quatityInStock <= 0) {
    return `<span class="out-of-stock">Hết hàng</span>`;
  } else if (productData.isHot == 1) {
    return `<span class="out-of-stock">HOT</span>`;
  } else {
    return "";
  }
}

//RenderProductCard Hot
function RenderProductMiniCard(paramData) {
  return `<div class="row">
  <div class="col-lg-6 col-md-6 col-12">
      <div class="list-image overlay">
          <img src="${paramData.photo}" alt="#">
          <a class="buy"><i class="fa fa-shopping-bag btn-card" data="${paramData.id}"></i></a>
      </div>
  </div>
  <div class="col-lg-6 col-md-6 col-12">
      <div class="content" style="padding-top: 0px !important; ">
          <h4 class="title"><a href="product-details.html?id=${paramData.id}">${paramData.productName}</a></h4>
          <p class="price with-discount">${formatCurrencyVND(paramData.byPrice)}</p>
      </div>
  </div>
</div>`;
}

//Render stock
function renderStock(quatityInStock) {
  if (quatityInStock >=1) {
    return `<span class="text-success">Còn hàng</span>`
  } else {
    return `<span class="ole">Hết hàng</span>`
  }
}

function renderUserName(params) {
    return params
}

function renderEmo(params) {
  if (params == 1) {
    return `<i class="ti-face-smile mr-3 text-success"></i>`;
  } if (params == 2) {
    return `<i class="ti-face-sad mr-3 text-danger"></i>`;
  } else return `<i class="ti-help-alt mr-3 text-info"></i>`;
}

  //hàm định dạng số
function formatNumber(num, separator, decimal, precision) {
  // Tạo một đối tượng NumberFormat với ngôn ngữ cục bộ là 'vi-VN'
  let nf = new Intl.NumberFormat('vi-VN', {
    // Sử dụng dấu . làm separator
    thousandSeparator: separator,
    // Sử dụng dấu , làm decimal
    decimalSeparator: decimal,
    // Làm tròn số đến số chữ số thập phân cho trước
    maximumFractionDigits: precision
  });
  // Trả về chuỗi đã được định dạng
  return nf.format(num);
}


function renderOrderStatus(params) {
  if (params == 0) {
    return `Đặt hàng`;
  } 
  
  if (params == 1) {
    return `Chuẩn bị`;
  } 
  
  if (params == 2) {
    return `Đang vận chuyển`;
  } 

  if (params == 5) {
    return `Thành công`;
  } 

  if (params == 6) {
    return `Hủy`;
  } 
  
  return params
}

//Hàm render customer name
function renderCustomerName(customerId) {
  var vCuss = "";
  $.ajax({
    url: gBASE_URL + "/customers/" + customerId,
    type: "GET",
    async: false,
    success: function (Res) {
      vCuss = Res.name;
    },
    error: function (ajaxContext) {
      return 0;
    },
  });

  return vCuss;
}