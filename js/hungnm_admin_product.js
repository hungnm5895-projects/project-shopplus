/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Dummy Data

var gProductId = 0;
var gImgProductUrl = "";
var gBrandLst = [];
var gCountriesLst = [];

//Các cột của bảng
const gPRODUCT_COLS = [
  "id",
  "id",
  "productCode",
  "productName",
  "productType",
  "brandId",
  "quatityInStock",
  "byPrice",
  "isHot",
  "isTrend",
  "star",
  "view"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gPRODUCT_COLS_ACTION_COL = 0;
const gPRODUCT_COLS_ID_COL = 1;
const gPRODUCT_COLS_CODE_COL = 2;
const gPRODUCT_COLS_NAME_COL = 3;
const gPRODUCT_COLS_TYPE_COL = 4;
const gPRODUCT_COLS_BRAND_COL = 5;
const gPRODUCT_COLS_QTY_COL = 6;
const gPRODUCT_COLS_PRICE_COL = 7;
const gPRODUCT_COLS_HOT_COL = 8;
const gPRODUCT_COLS_TREND_COL = 9;
const gPRODUCT_COLS_STAR_COL = 10;
const gPRODUCT_COLS_VIEW_COL = 11;

var gProductTable = $("#product-table").DataTable({
  responsive: true,
  lengthChange: false,
  autoWidth: false,
  buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
  columns: [
    { data: gPRODUCT_COLS[gPRODUCT_COLS_ACTION_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_ID_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_CODE_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_NAME_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_TYPE_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_BRAND_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_QTY_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_PRICE_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_HOT_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_TREND_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_STAR_COL] },
    { data: gPRODUCT_COLS[gPRODUCT_COLS_VIEW_COL] }
  ],
  columnDefs: [
    {
      target: gPRODUCT_COLS_ACTION_COL,
      render: renderAction
    },
    {
      target: gPRODUCT_COLS_TYPE_COL,
      render: renderProductType
    }
    ,
    {
      target: gPRODUCT_COLS_HOT_COL,
      render: renderHotProduct
    }
    ,
    {
      target: gPRODUCT_COLS_TREND_COL,
      render: renderTrendProduct
    }
    ,
    {
      target: gPRODUCT_COLS_BRAND_COL,
      render: renderBrand
    }
    ,
    {
      target: gPRODUCT_COLS_STAR_COL,
      render: renderStar
    }
  ],
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

//OnpageLoading

//Gán sự kiện khi ấn nút thêm mới campain
$("#btn-create-product").click(function () {
  clearData();
  $("#add-product-modal").modal();
});

//Gán sự kiện khi ấn nút tải ảnh lên trên modal thêm mới campain
$("#btn-add-upload").click(function () {
  onBtnUploadPhotoAddClick();
});

$("#btn-info-upload").click(function () {
  onBtnUploadPhotoinfoClick();
});

//Gán sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
$("#btn-modal-add-product").click(function () {
  onBtnAddProductModalClick();
});

//Gán sự kiện khi ấn vào biểu tượng kính lúp trên bảng
$("#product-table").on("click", ".btn-edit", function () {
  onIconInfoInRowClick(this);
});

//Gán sự kiện khi ấn nút cập nhật thông tin trên modal info
$("#btn-modal-edit-product").click(function () {
  onBtnUpdateProductClick();
});

//Gán sự kiện khi ấn vào biểu tượng thùng rác
$("#product-table").on("click", ".btn-delete", function () {
  onDeleteProductClick(this);
});

$("#btn-modal-confirm-delete-product").click(function () {
  onBtnDeleteProductClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Tải toàn bộ thương hiệu
loadAllBrands();

//Tải toàn bộ quốc gia
loadAllCountries();

//Tải toàn bộ khách hàng về
loadAllProduct();


//Hàm xử lý sự kiện khi ấn vào icon kính lúp
function onIconInfoInRowClick(elementRow) {
  $("#info-product-modal").modal();
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gProductTable.row(vTableRow).data();
  gProductId = vRowData.id;
  //(gCampaignId);
  showProductInfoToForm(vRowData);
}
//Xử lý sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
function onBtnAddProductModalClick() {
  var vObj = {
    productCode: $("#input-add-productCode").val().trim(),
    productName: $("#input-add-productName").val().trim(),
    productType: $("#input-add-productType").val(),
    productDescription: $("#input-add-productDescription").val().trim(),
    productLine: 0,
    productScale: parseInt($("#input-add-productScale").val()),
    brandId: $("#input-add-brandId").val().trim(),
    quatityInStock: parseInt($("#input-add-quatityInStock").val().trim()),
    byPrice: $("#input-add-byPrice").val().trim(),
    discount: $("#input-add-discount").val().trim(),
    isHot: $("#input-add-isHot").val().trim(),
    isTrend: $("#input-add-isTrend").val().trim(),
    pinTime: $("#input-add-pinTime").val().trim(),
    boxTime: $("#input-add-boxTime").val().trim(),
    gateCharge: $("#input-add-gateCharge").val().trim(),
    audioTechnology: $("#input-add-audioTechnology").val().trim(),
    connectSameTime: $("#input-add-connectSameTime").val().trim(),
    madeIn: $("#input-add-madeIn").val().trim(),
    longX: $("#input-add-longX").val().trim(),
    widthY: $("#input-add-widthY").val().trim(),
    highZ: $("#input-add-highZ").val().trim(),
    weightW: $("#input-add-weightW").val().trim(),
    compatible: $("#input-add-compatible").val().join(";"),
    photo: gImgProductUrl
  };

  (JSON.stringify(vObj));


  if (validateProduct(vObj)) {
    $.ajax({
      url: gBASE_URL + "/products",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Thêm mới sản phẩm thành công");
        loadAllProduct();
        $("#add-product-modal").modal("hide");
      },
      error: function (ajaxContext) {
        var errorLog = JSON.parse(ajaxContext.responseText)
        toastr.error(errorLog.errors[0]);
        //console.log(ajaxContext);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút cập nhật thông tin
function onBtnUpdateProductClick() {
  var vObj = {
    productCode: $("#input-info-productCode").val().trim(),
    productName: $("#input-info-productName").val().trim(),
    productType: $("#input-info-productType").val(),
    productDescription: $("#input-info-productDescription").val().trim(),
    productLine: 0,
    productScale: parseInt($("#input-info-productScale").val()),
    brandId: $("#input-info-brandId").val().trim(),
    quatityInStock: parseInt($("#input-info-quatityInStock").val().trim()),
    byPrice: $("#input-info-byPrice").val().trim(),
    discount: $("#input-info-discount").val().trim(),
    isHot: $("#input-info-isHot").val().trim(),
    isTrend: $("#input-info-isTrend").val().trim(),
    pinTime: $("#input-info-pinTime").val().trim(),
    boxTime: $("#input-info-boxTime").val().trim(),
    gateCharge: $("#input-info-gateCharge").val().trim(),
    audioTechnology: $("#input-info-audioTechnology").val().trim(),
    connectSameTime: $("#input-info-connectSameTime").val().trim(),
    madeIn: $("#input-info-madeIn").val().trim(),
    longX: $("#input-info-longX").val().trim(),
    widthY: $("#input-info-widthY").val().trim(),
    highZ: $("#input-info-highZ").val().trim(),
    weightW: $("#input-info-weightW").val().trim(),
    compatible: $("#input-info-compatible").val().join(";"),
    photo: gImgProductUrl
  };

  if (validateProduct(vObj)) {
    $.ajax({
      url: gBASE_URL + "/products/" + gProductId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Cập nhật sản phẩm thành công");
        loadAllProduct();
        $("#info-product-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//hàm xử lý sự kiện khi ấn icon thùng rác
function onDeleteProductClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gProductTable.row(vTableRow).data();
  gProductId = vRowData.id;
  $("#delete-product-modal").modal();
}


//Hàm xử lý sự kiện khi ấn nút xóa khách hàng
function onBtnDeleteProductClick() {
  $.ajax({
    url: gBASE_URL + "/products/" + gProductId,
    type: "DELETE",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Xóa thương hiệu thành công");
      loadAllProduct();
      $("#delete-product-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
    },
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm gọi api lấy ra countries
function loadAllCountries() {
  $.ajax({
    url: gBASE_URL + "/countries",
    method: "GET",
    async: false,
    success: function (pObjRes) {
      gCountriesLst = pObjRes;
      loadDataToCountrySelect(gCountriesLst);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
    },
  });
}

//Hàm gọi api lấy ra Thương hiệu
function loadAllBrands() {
  $.ajax({
    url: gBASE_URL + "/brands",
    method: "GET",
    async: false,
    success: function (pObjRes) {
      gBrandLst = pObjRes;
      loadBrandToSelect(gBrandLst);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
    },
  });
}

function loadAllProduct() {
  $.ajax({
    url: gBASE_URL + "/products",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showProductLstToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      //showCustomerToTable(gCustomerList);
    },
  });
}

//Hàm hiển thị dữ liệu trả về lên table
function showProductLstToTable(data) {
  gStt = 0;
  gProductTable.clear();
  gProductTable.rows.add(data);
  gProductTable.draw();
  gProductTable.buttons().container().appendTo("#product-table");
}

//Hàm kiểm tra dữ liệu Khách hàng
function validateProduct(paramObj) {
  if (paramObj.productCode == "") {
    toastr.warning("Mã sản phẩm không được để trống");
    return false;
  }

  if (paramObj.productName == "") {
    toastr.warning("Tên sản phẩm không được để trống");
    return false;
  }

  if (paramObj.productDescription == "") {
    toastr.warning("Mô tả sản phẩm không được để trống");
    return false;
  }

  if (paramObj.productScale <= 0) {
    toastr.warning("Giá gốc sản phẩm phải >= 0");
    return false;
  }

  if (paramObj.quatityInStock <= 0) {
    toastr.warning("Số lượng trong kho phải >= 0 ");
    return false;
  }

  if (paramObj.byPrice <= 0) {
    toastr.warning("Giá bán phải > 0");
    return false;
  }

  if (paramObj.discount < 0 || paramObj.discount > 1) {
    toastr.warning("Chiết khấu không hợp lệ");
    return false;
  }

  if (paramObj.pinTime <= 0) {
    toastr.warning("Thời gian pin phải > 0");
    return false;
  }

  if (paramObj.boxTime <= 0) {
    toastr.warning("Thời gian hộp phải > 0");
    return false;
  }
  if (paramObj.weightW <= 0) {
    toastr.warning("Trọng lượng phải > 0");
    return false;
  }
  if (paramObj.longX <= 0) {
    toastr.warning("Chiều dài phải > 0");
    return false;
  }
  if (paramObj.widthY <= 0) {
    toastr.warning("Chiều rộng phải > 0");
    return false;
  }
  if (paramObj.highZ <= 0) {
    toastr.warning("Chiều cao phải > 0");
    return false;
  }

  return true;
}

//Xóa dữ liệu modal thêm mới
function clearData() {
  $("#input-add-productCode").val("");
  $("#input-add-productName").val("");
  $("#input-add-productType").val(0);
  $("#input-add-productDescription").val("");
  $("#input-add-productScale").val(0);
  $("#input-add-brandId").val(1);
  $("#input-add-quatityInStock").val(0);
  $("#input-add-byPrice").val(0);
  $("#input-add-discount").val(0);
  $("#input-add-isHot").val(0);
  $("#input-add-isTrend").val(0);
  $("#input-add-pinTime").val(0);
  $("#input-add-boxTime").val(0);
  $("#input-add-gateCharge").val("");
  $("#input-add-audioTechnology").val("");
  $("#input-add-connectSameTime").val(0);
  $("#input-add-madeIn").val(0);
  $("#input-add-longX").val(0);
  $("#input-add-widthY").val(0);
  $("#input-add-highZ").val(0);
  $("#input-add-weightW").val(0);
  $("#input-add-compatible").val([]);
  photo = "";
}

//Hàm hiển thị dữ liệu khách hàng qua form
function showProductInfoToForm(paramData) {
  $("#input-info-productCode").val(paramData.productCode);
  $("#input-info-productName").val(paramData.productName);
  $("#input-info-productType").val(paramData.productType);
  $("#input-info-productDescription").val(paramData.productDescription);
  $("#input-info-productScale").val(paramData.productScale);
  $("#input-info-brandId").val(paramData.brandId);
  $("#input-info-quatityInStock").val(paramData.quatityInStock);
  $("#input-info-byPrice").val(paramData.byPrice);
  $("#input-info-discount").val(paramData.discount);
  $("#input-info-isHot").val(paramData.isHot);
  $("#input-info-isTrend").val(paramData.isTrend);
  $("#input-info-pinTime").val(paramData.pinTime);
  $("#input-info-boxTime").val(paramData.boxTime);
  $("#input-info-gateCharge").val(paramData.gateCharge);
  $("#input-info-audioTechnology").val(paramData.audioTechnology);
  $("#input-info-connectSameTime").val(paramData.connectSameTime);
  $("#input-info-madeIn").val(paramData.madeIn);
  $("#input-info-longX").val(paramData.longX);
  $("#input-info-widthY").val(paramData.widthY);
  $("#input-info-highZ").val(paramData.highZ);
  $("#input-info-weightW").val(paramData.weightW);
  $("#input-info-compatible").val(paramData.compatible.split(";"));
  $("#div-info-photo").html(renderPhoto(paramData.photo));

  $("#input-info-view").html(paramData.view);
  $("#input-info-boughtSum").html(paramData.boughtSum);
  $("#input-info-star").html(renderStar(paramData.star));
  $("#input-info-saleDate").html(renderDateTime(paramData.saleDate));



}

//Đổ dữ liệu vào select country
function loadDataToCountrySelect(paramCountriesData) {
  var vCountriesSelectElement = $("#input-add-madeIn");
  for (i = 0; i < paramCountriesData.length; i++) {
    let bCountryOption = $("<option/>");
    bCountryOption.prop("value", paramCountriesData[i].id);
    bCountryOption.prop("text", paramCountriesData[i].countryName);
    vCountriesSelectElement.append(bCountryOption);
  };


  var vProvinceSelectElement2 = $("#input-info-madeIn");
  for (i = 0; i < paramCountriesData.length; i++) {
    let bProvinceOption = $("<option/>");
    bProvinceOption.prop("value", paramCountriesData[i].id);
    bProvinceOption.prop("text", paramCountriesData[i].countryName);
    vProvinceSelectElement2.append(bProvinceOption);
  };
}

function loadBrandToSelect(data) {
  var vBrandSelectElement = $("#input-add-brandId");
  for (i = 0; i < data.length; i++) {
    let bBrandOption = $("<option/>");
    bBrandOption.prop("value", data[i].id);
    bBrandOption.prop("text", data[i].brandName);
    vBrandSelectElement.append(bBrandOption);
  };

  var vBrandSelectElementInfo = $("#input-info-brandId");
  for (i = 0; i < data.length; i++) {
    let bBrandOption = $("<option/>");
    bBrandOption.prop("value", data[i].id);
    bBrandOption.prop("text", data[i].brandName);
    vBrandSelectElementInfo.append(bBrandOption);
  };
}

//Hàm xử lý sự kiện khi ấn nút tải ảnh lên trên modal thêm mới product
function onBtnUploadPhotoAddClick() {
  const fileInput = document.querySelector("#input-add-photo");
  const [file] = fileInput.files;
  var form = new FormData();
  form.append("file", file);
  if (fileInput.files.length > 0) {
    $.ajax({
      method: "POST",
      url: gBASE_URL + "/uploadFile",
      processData: false,
      mimeType: "multipart/form-data",
      contentType: false,
      data: form,
      success: function (pObjRes) {
        toastr.success("Tải ảnh thành công");
        pObjRes = JSON.parse(pObjRes);
        gImgProductUrl = pObjRes.fileDownloadUri;
        $("#div-add-photo").html(`<img src="${gImgProductUrl}" width="50%">`);
      },
      error: function (pXhrObj) {
        toastr.error(pXhrObj);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút tải ảnh lên trên modal sửa product
function onBtnUploadPhotoinfoClick() {
  const fileInput = document.querySelector("#input-info-photo");
  const [file] = fileInput.files;
  var form = new FormData();
  form.append("file", file);
  if (fileInput.files.length > 0) {
    $.ajax({
      method: "POST",
      url: gBASE_URL + "/uploadFile",
      processData: false,
      mimeType: "multipart/form-data",
      contentType: false,
      data: form,
      success: function (pObjRes) {
        toastr.success("Tải ảnh thành công");
        pObjRes = JSON.parse(pObjRes);
        gImgProductUrl = pObjRes.fileDownloadUri;
        $("#div-info-photo").html(`<img src="${gImgProductUrl}" width="50%">`);
      },
      error: function (pXhrObj) {
        toastr.error(pXhrObj);
      },
    });
  }
}