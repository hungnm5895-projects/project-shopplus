/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gProductId;
var gTotalRecords;
// Tổng số trang. Math ceil để lấy số bản ghi tối đa có được. Ví dụ: 10 / 3 = 3.33 => Cần 4 trang hiển thị
var gTotalPages;


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$("#btn-comment").click(function () {
    onBtnCommentClick();
});

//hàm xử lý sự kiện khi ân snuts thêm vào giỏ hàng tại trang product - detail
$("#btn-add-product1").click(function () {
    ("ấn");
    var vQty = parseInt($("#inp-qty").val());
    if (vQty >= 1) {
        addProduct(gProductId, vQty); 
        toastr.success("Thêm sản phẩm vào giỏ hàng thành công"); 
    } else {
        toastr.error("Số lượng không hợp lệ, vui lòng thử lại"); 
    }
    
});

//Gấn sự kiện khi ấn vào icon vote sao
$('.stars a').on('click', function () {
    var vStar = $(this).data("mydata1");
    $.ajax({
        url: gBASE_URL + "/products/vote/" + gProductId + "/" + vStar,
        method: "PUT",
        contentType: "application/json",
        success: function (pObjRes) {
            getProductById(gProductId);
            toastr.success("Đánh giá thành công");
        },
        error: function (pXhrObj) {
            toastr.error("Có lỗi xảy ra, vui lòng thử lại");
        }
    });
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//PageLoading
loadAllBrands();
//Tải toàn bộ quốc gia
loadAllCountries();

//Xử lý sự kiện khi ấn nút comment
function onBtnCommentClick() {
    var nComment = {
        comment: $("#input-comment").val().trim(),
        userId: 1,
        productId: gProductId
    }

    if (nComment.comment != "") {
        $.ajax({
            url: gBASE_URL + "/comments",
            method: "POST",
            data: JSON.stringify(nComment),
            contentType: "application/json",
            success: function (pObjRes) {
                loadCommentOfProduct(0);
                $("#input-comment").val("");
                toastr.success("Bình luận thành công");
            },
            error: function (pXhrObj) {
                return 0
            }
        });
    } else {
        toastr.warning("Bạn vui lòng nhập nội dung bình luận");
    }
}

getProductByURL();

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/


//hàm lấy id của product trên url
function getProductByURL() {
    var vUrlString = window.location.href; //đường đẫn gọi đến trang
    //tạo đối tượng url để truy vấn tham số 
    var vUrlObject = new URL(vUrlString);
    gProductId = vUrlObject.searchParams.get("id");
    //Gọi API lấy về Real Estate
    (gProductId);
    getProductById(gProductId);
    loadCommentOfProduct(0);
}

//Hàm lấy dữ liệu của product
function getProductById(paramId) {
    $.ajax({
        url: gBASE_URL + "/products/" + paramId,
        method: "GET",
        success: function (pObjRes) {
            //(pObjRes);
            showProductToInfo(pObjRes);
        },
        error: function (pXhrObj) {
            return 0
        }
    });
}

//Hàm hiển thị dữ liệu lên web
function showProductToInfo(paramData) {
    $("#info-productName").html(paramData.productName);
    $("#info-productCode").html(paramData.productCode);
    $("#info-photo").prop("src", paramData.photo)
    $("#info-star").html(renderStar(paramData.star));
    $("#info-byPrice").html(formatCurrencyVND(paramData.byPrice));
    $("#info-productScale").html(formatCurrencyVND(paramData.productScale));
    $("#info-quatityInStock").html(renderStock(paramData.quatityInStock));
    $("#info-saleDate").html(renderDateTime(paramData.saleDate));
    $("#info-brandCountry").html(renderBrandCountry(paramData.brandId));
    $("#info-madeIn").html(renderCountries(paramData.madeIn));
    $("#info-brandId").html(renderBrand(paramData.brandId));
    $("#info-pinTime").html(paramData.pinTime);
    $("#info-boxTime").html(paramData.boxTime);
    $("#info-gateCharge").html(paramData.gateCharge);
    $("#info-connectSameTime").html(paramData.connectSameTime);
    $("#info-connectTechnology").html(paramData.connectTechnology);
    $("#info-audioTechnology").html(paramData.audioTechnology);
    $("#info-longX").html(paramData.longX);
    $("#info-widthY").html(paramData.widthY);
    $("#info-highZ").html(paramData.highZ);
    $("#info-weightW").html(paramData.weightW);
    $("#info-re-brandId").html(renderBrand(paramData.brandId));
    $("#info-productDescription").html(paramData.productDescription);
}


//Hàm gọi api lấy ra countries
function loadAllCountries() {
    $.ajax({
        url: gBASE_URL + "/countries",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            gCountriesLst = pObjRes;
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

//Hàm gọi api lấy ra Thương hiệu
function loadAllBrands() {
    $.ajax({
        url: gBASE_URL + "/brands",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            gBrandLst = pObjRes;
        },
        error: function (pXhrObj) {
            toastr.error(pXhrObj);
        },
    });
}

//ham lấy comment của 1 product
function loadCommentOfProduct(vPagenum) {
    const vQueryParams = new URLSearchParams({
        "size": 5,    //mặc định 8 phần tử 1 trang
        "page": vPagenum        //Lấy page đầu tiên
    });

    $.ajax({
        url: gBASE_URL + "/comments/productDetail/" + gProductId + "?" + vQueryParams.toString(),
        method: "GET",
        success: function (Res) {
            (Res);
            gTotalRecords = Res.total;
            gTotalPages = Math.ceil(gTotalRecords / 5);
            var paramData = Res.data;
            showCommentToInfo(paramData);
            createpagination(vPagenum);
        },
        error: function (pXhrObj) {
            toastr.error("Có lỗi xảy ra, vui lòng thử lại");
        }
    });
}

//hàm hiển thị danh sách comment
function showCommentToInfo(commentLst) {
    var vDivComment = $("#comments-div").html("");
    if (commentLst.length > 0) {
        commentLst.forEach(CommentEle => {
            $("<div>", { class: "card mb-4" }).html(RenderCommentLst(CommentEle))
                .appendTo(vDivComment);
        });
    } else {
        $("#comments-div").addClass("text-danger").html("Hãy trở thành người đầu tiên nhận xét về sản phẩm này");
    }
}

function RenderCommentLst(CommentEle) {
    return `<div class="card-body">
    <div class="d-flex justify-content-between">
        <div class="d-flex flex-row align-items-center">
            <p class="small mb-0 ms-2 mr-2 font-weight-bold"><i
                    class="ti-user"></i> ${renderUserName(CommentEle.userId)}</p>
            <p class="small mb-0 ms-2"><i
                    class="ti-time"></i>${renderFullDateTime(CommentEle.createDate)}
            </p>
        </div>
    </div>
    <p>${(CommentEle.comment)} </p>
</div>`;
}


//Hàm tạo thanh phân trang
function createpagination(paramPage) {
    //(gTotalPages);
    var vDivPagination = $("#div-paging").html("");

    if (paramPage == 0) {
        vDivPagination.append(`<button class='page-item disabled previous btn'><a href='javascript:void(0)' > Trang trước </a></button>`);

    } else {
        vDivPagination.append("<button class='page-item btn' onclick='loadCommentOfProduct(" + (paramPage - 1) + ")'><a href='javascript:void(0)' > Trang trước </a></button>");

    }

    if (paramPage == gTotalPages - 1) { // do page 1 co chi so = 0 nen gTotalPages phai -1
        vDivPagination.append("<button class='page-item disabled previous btn'><a href='javascript:void(0)'>Trang kế</a></button>");

    } else {
        vDivPagination.append("<button class='page-item btn' onclick='loadCommentOfProduct(" + (paramPage + 1) + ")'><a href='javascript:void(0)'>Trang sau</a></button>");

    }
}