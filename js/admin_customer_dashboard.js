//Bar char 1
var gDataRankLst = [];




//RG2

//OnpageLoading

//RG3
loadBarCharData();
//RG4

function loadBarCharData() {
  $.ajax({
    url: gBASE_URL + "/loyaltys/ranks",
    method: "GET",
    success: function (pObjRes) {
      console.log(pObjRes);
      displayChar(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      //showNotiLstToTable(gNotiLst);
    },
  });
}

function displayChar(data) {
  gDataRankLst.push(data.rankNormal);
  gDataRankLst.push(data.rankVip);
  gDataRankLst.push(data.rankSilver);
  gDataRankLst.push(data.rankGold);
  gDataRankLst.push(data.rankPlatinum);

  var areaChartData1 = {
    labels: ["Thường", "Vip", "Bạc", "Vàng", "Bạch Kim"],
    datasets: [
      {
        label: "Phân hạng Khách hàng",
        backgroundColor: "rgba(60,141,188,0.9)",
        borderColor: "rgba(60,141,188,0.8)",
        pointRadius: false,
        pointColor: "#3b8bba",
        pointStrokeColor: "rgba(60,141,188,1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(60,141,188,1)",
        data: gDataRankLst,
      },
    ],
  };

  var barChartCanvas1 = $("#barChart").get(0).getContext("2d");
  var barChartData1 = $.extend(true, {}, areaChartData1);
  //var temp0 = areaChartData.datasets[0]
  //barChartData.datasets[0] = temp0

  var barChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    datasetFill: false,
  };

  new Chart(barChartCanvas1, {
    type: "bar",
    data: barChartData1,
    options: barChartOptions,
  });
}