/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Dummy Data
var gCustomerList = JSON.parse(
  `[{"name":"hung","address":"Ngọc Thụy","phone":"0964272535","createDate":"11/12/2023","email":"hungnm@gmail.com","userId":1,"id":1,"provinceId":2,"createdBy":0},{"name":"hung","address":"Ngọc Thụy","phone":"0964272535","email":"hungnm@gmail.com","userId":2,"id":2,"provinceId":2,"createdBy":0,"createDate":"11/12/2023"},{"name":"hung","address":"Ngọc Thụy","phone":"0964272535","email":"hungnm@gmail.com","userId":3,"id":3,"provinceId":2,"createdBy":0,"createDate":"11/12/2023"},{"name":"hung","address":"Ngọc Thụy","phone":"0964272535","email":"hungnm@gmail.com","userId":3,"id":4,"provinceId":2,"createdBy":0,"createDate":"11/12/2023"}]`
);

var gCampaignId = 0;
var gImgCampaignUrl = "";
//Các cột của bảng
const gCAMPAIGN_COLS = [
  "id",
  "id",
  "campaignCode",
  "campaignName",
  "startDate",
  "endDate"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCAMPAIGN_COLS_ACTION_COL = 0;
const gCAMPAIGN_COLS_ID_COL = 1;
const gCAMPAIGN_COLS_CODE_COL = 2;
const gCAMPAIGN_COLS_NAME_COL = 3;
const gCAMPAIGN_COLS_START_COL = 4;
const gCAMPAIGN_COLS_END_COL = 5;

var gCampaignTable = $("#campaign-table").DataTable({
  responsive: true,
  lengthChange: false,
  autoWidth: false,
  buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
  columns: [
    { data: gCAMPAIGN_COLS[gCAMPAIGN_COLS_ACTION_COL] },
    { data: gCAMPAIGN_COLS[gCAMPAIGN_COLS_ID_COL] },
    { data: gCAMPAIGN_COLS[gCAMPAIGN_COLS_CODE_COL] },
    { data: gCAMPAIGN_COLS[gCAMPAIGN_COLS_NAME_COL] },
    { data: gCAMPAIGN_COLS[gCAMPAIGN_COLS_START_COL] },
    { data: gCAMPAIGN_COLS[gCAMPAIGN_COLS_END_COL] },
  ],
  columnDefs: [
    {
      target: gCAMPAIGN_COLS_ACTION_COL,
      render: renderAction
    },
    {
      target: gCAMPAIGN_COLS_START_COL,
      render: renderDateTime
    },
    {
      target: gCAMPAIGN_COLS_END_COL,
      render: renderDateTime
    }

  ],
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

//OnpageLoading
//Đặt ngày trong modal tạo mới là ngày hiện tại
$("#modal-add-endDate").val(dayjs(new Date()).format('YYYY-MM-DD'));
$("#modal-add-endDate").prop("min", dayjs(new Date()).format('YYYY-MM-DD'))
$("#modal-add-startDate").val(dayjs(new Date()).format('YYYY-MM-DD'));
$("#modal-add-startDate").prop("min", dayjs(new Date()).format('YYYY-MM-DD'))
$("#modal-info-endDate").prop("min", dayjs(new Date()).format('YYYY-MM-DD'))

//Gán sự kiện khi ấn nút thêm mới campain
$("#btn-create-campaign").click(function () {
  clearData();
  $("#add-campaign-modal").modal();
});

//Gán sự kiện khi ấn nút tải ảnh lên trên modal thêm mới campain
$("#btn-add-upload").click(function () {
  onBtnUploadPhotoAddClick();
});

//Gán sự kiện khi ấn nút tải ảnh lên trên modal thêm mới campain
$("#btn-info-upload").click(function () {
  onBtnUploadPhotoInfoClick();
});

//Gán sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
$("#btn-modal-add-campaign").click(function () {
  onBtnAddCampaignModalClick();
});

//Gán sự kiện khi ấn vào biểu tượng kính lúp trên bảng
$("#campaign-table").on("click", ".btn-edit", function () {
  onIconInfoInRowClick(this);
});

//Gán sự kiện khi ấn nút cập nhật thông tin trên modal info
$("#btn-modal-edit-campaign").click(function () {
  onBtnUpdateCampaignClick();
});

//Gán sự kiện khi ấn vào biểu tượng thùng rác
$("#campaign-table").on("click", ".btn-delete", function () {
  onDeleteCampaignClick(this);
});

$("#btn-modal-confirm-delete-campaign").click(function () {
  onBtnDeleteCampaignClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */


//Tải toàn bộ khách hàng về
loadAllCampaign();

//Hàm xử lý sự kiện khi ấn nút tải ảnh lên trên modal thêm mới campaign
function onBtnUploadPhotoAddClick() {
  const fileInput = document.querySelector("#modal-add-photo");
  const [file] = fileInput.files;
  var form = new FormData();
  form.append("file", file);
  if (fileInput.files.length > 0) {
    $.ajax({
      method: "POST",
      url: gBASE_URL + "/uploadFile",
      processData: false,
      mimeType: "multipart/form-data",
      contentType: false,
      data: form,
      success: function (pObjRes) {
        toastr.success("Tải ảnh thành công");
        pObjRes = JSON.parse(pObjRes);
        gImgCampaignUrl = pObjRes.fileDownloadUri;
        $("#div-add-photo").html(`<img src="${gImgCampaignUrl}" width="100%">`);
      },
      error: function (pXhrObj) {
        toastr.error(pXhrObj);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút tải ảnh lên trên modal thêm mới campaign
function onBtnUploadPhotoInfoClick() {
  const fileInput = document.querySelector("#modal-info-photo");
  const [file] = fileInput.files;
  var form = new FormData();
  form.append("file", file);
  if (fileInput.files.length > 0) {
    $.ajax({
      method: "POST",
      url: gBASE_URL + "/uploadFile",
      processData: false,
      mimeType: "multipart/form-data",
      contentType: false,
      data: form,
      success: function (pObjRes) {
        toastr.success("Tải ảnh thành công");
        pObjRes = JSON.parse(pObjRes);
        gImgCampaignUrl = pObjRes.fileDownloadUri;
        $("#div-info-photo").html(`<img src="${gImgCampaignUrl}" width="100%">`);
      },
      error: function (pXhrObj) {
        toastr.error(pXhrObj);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn vào icon kính lúp
function onIconInfoInRowClick(elementRow) {
  $("#info-campaign-modal").modal();
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gCampaignTable.row(vTableRow).data();
  gCampaignId = vRowData.id;
  //(gCampaignId);
  showCampaignInfoToForm(vRowData);
}
//Xử lý sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
function onBtnAddCampaignModalClick() {
  var vObj = {
    campaignCode: $("#modal-add-campaignCode").val().trim(),
    campaignName: $("#modal-add-campaignName").val().trim(),
    campaignDescription: $("#modal-add-campaignDescription").val().trim(),
    note: $("#modal-add-note").val().trim(),
    startDate: $("#modal-add-startDate").val(),
    endDate: $("#modal-add-endDate").val(),
    photo: gImgCampaignUrl
  };

  if (validateCampaign(vObj)) {
    $.ajax({
      url: gBASE_URL + "/campaigns",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Thêm mới khuyến mại thành công");
        loadAllCampaign();
        $("#add-campaign-modal").modal("hide");
      },
      error: function (ajaxContext) {
        var errorLog = JSON.parse(ajaxContext.responseText)
        toastr.error(errorLog.errors[0]);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút cập nhật thông tin
function onBtnUpdateCampaignClick() {
  var vObj = {
    campaignCode: $("#modal-info-campaignCode").val().trim(),
    campaignName: $("#modal-info-campaignName").val().trim(),
    campaignDescription: $("#modal-info-campaignDescription").val().trim(),
    note: $("#modal-info-note").val().trim(),
    startDate: $("#modal-info-startDate").val(),
    endDate: $("#modal-info-endDate").val(),
    photo: gImgCampaignUrl
  };

  if (validateCampaign(vObj)) {
    $.ajax({
      url: gBASE_URL + "/campaigns/" + gCampaignId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Cập nhật Chiến dịch khuyến mại thành công");
        loadAllCampaign();
        $("#info-campaign-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//hàm xử lý sự kiện khi ấn icon thùng rác
function onDeleteCampaignClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gCampaignTable.row(vTableRow).data();
  gCampaignId = vRowData.id;
  $("#delete-campaign-modal").modal();
}


//Hàm xử lý sự kiện khi ấn nút xóa khách hàng
function onBtnDeleteCampaignClick() {
  $.ajax({
    url: gBASE_URL + "/campaigns/" + gCampaignId,
    type: "DELETE",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Xóa khách hàng khách hàng thành công");
      loadAllCampaign();
      $("#delete-campaign-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
    },
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function loadAllCampaign() {
  $.ajax({
    url: gBASE_URL + "/campaigns",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showCampaignToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      //showCustomerToTable(gCustomerList);
    },
  });
}

//Hàm hiển thị dữ liệu trả về lên table
function showCampaignToTable(data) {
  gStt = 0;
  gCampaignTable.clear();
  gCampaignTable.rows.add(data);
  gCampaignTable.draw();
  gCampaignTable.buttons().container().appendTo("#customer-table");
}

//Hàm kiểm tra dữ liệu Khách hàng
function validateCampaign(paramObj) {
  if (paramObj.campaignCode == "") {
    toastr.warning("Mã khuyến mại không được để trống");
    return false;
  }

  if (paramObj.campaignName == "") {
    toastr.warning("Tên khuyến mại không được để trống");
    return false;
  }

  if (paramObj.campaignDescription == "") {
    toastr.warning("Mô tả khuyến mại không được để trống");
    return false;
  }

  return true;
}

//Xóa dữ liệu modal thêm mới
function clearData() {
  $("#modal-add-name").val("");
  $("#modal-add-address").val("");
  $("#modal-add-phone").val("");
  $("#modal-add-email").val("");
  $("#modal-add-province").val(0);
  gImgCampaignUrl = "";
}

//Hàm hiển thị dữ liệu khách hàng qua form
function showCampaignInfoToForm(paramData) {
  $("#modal-info-campaignCode").val(paramData.campaignCode);
  $("#modal-info-campaignName").val(paramData.campaignName);
  $("#modal-info-campaignDescription").val(paramData.campaignDescription);
  $("#modal-info-note").val(paramData.note);
  $("#modal-info-startDate").val(renderDateTimeInput(paramData.startDate));
  $("#modal-info-endDate").val(renderDateTimeInput(paramData.endDate));
  $("#div-info-photo").html(renderPhoto(paramData.photo));
  gImgCampaignUrl = paramData.photo;
}

