/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Dummy Data
var gCountryList = JSON.parse(
  `[{"countryCode":"US","countryName":"Hoa Kỳ","id":1},{"countryCode":"VNI","countryName":"Việt Nam","id":2},{"countryCode":"CNN","countryName":"Trung Quốc","id":3},{"countryCode":"RUS","countryName":"Nga","id":4}]`
);

var gStt = 0;
var gCountryId = 0;

//Các cột của bảng
const gCOUNTRY_COLS = [
  "id",
  "id",
  "countryCode",
  "countryName"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCOUNTRY_COLS_ACTION_COL = 0;
const gCOUNTRY_COLS_ID_COL = 1;
const gCOUNTRY_COLS_CODE_COL = 2;
const gCOUNTRY_COLS_NAME_COL = 3;


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

var gCountryTable = $("#country-table")
  .DataTable({
    responsive: true,
    lengthChange: false,
    autoWidth: false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    columns: [
      { data: gCOUNTRY_COLS[gCOUNTRY_COLS_ACTION_COL] },
      { data: gCOUNTRY_COLS[gCOUNTRY_COLS_ID_COL] },
      { data: gCOUNTRY_COLS[gCOUNTRY_COLS_CODE_COL] },
      { data: gCOUNTRY_COLS[gCOUNTRY_COLS_NAME_COL] }
    ],
    columnDefs: [
      {
        target: gCOUNTRY_COLS_ACTION_COL,
        render: renderAction,
      }
    ],
  })

//OnpageLoading

$("#btn-create-country").click(function () {
  clearData();
  $("#add-country-modal").modal();
});

//Gán sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
$("#btn-modal-add-country").click(function () {
  onBtnAddCountryModalClick();
});

//Gán sự kiện khi ấn vào dòng
//Gán sự kiện khi ấn vào dòng trên table
$("#country-table").on("click", ".btn-edit", function () {
  onCountryRowClick(this);
});

//Gán sự kiện khi ấn nút cập nhật thông tin trên modal info
$("#btn-modal-edit-country").click(function () {
  onBtnUpdateCountryClick();
});

$("#country-table").on("click", ".btn-delete", function () {
  onDeleteCountryClick(this);
});


$("#btn-modal-confirm-delete-country").click(function () {
  onBtnDeleteCountryConfirmClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Lấy về danh sách thành phố
//Tải toàn bộ khách hàng về
loadAllCountry();

//hàm xử lý sự kiện khi ấn icon thùng rác
function onDeleteCountryClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gCountryTable.row(vTableRow).data();
  gCountryId = vRowData.id;
  $("#delete-country-modal").modal();
}

//Hàm xử lý sự kiện khi ấn vào icon kính lúp
function onCountryRowClick(elementRow) {
  $("#info-country-modal").modal();
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gCountryTable.row(vTableRow).data();
  gCountryId = vRowData.id;
  //(gVoucherId);
  showCountryInfoToForm(vRowData);
}
//Xử lý sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
function onBtnAddCountryModalClick() {
  var vObj = {
    countryCode: $("#modal-add-countryCode").val().trim(),
    countryName: $("#modal-add-countryName").val().trim(),
  };
  (vObj);
  if (validateCountry(vObj)) {
    $.ajax({
      url: gBASE_URL + "/countries",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Thêm mới khách hàng thành công");
        loadAllCountry();
        $("#add-country-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút cập nhật thông tin
function onBtnUpdateCountryClick() {
  var vObj = {
    voucherCode: $("#modal-info-countryCode").val().trim(),
    voucherName: $("#modal-info-countryName").val().trim(),
  };

  if (validateCountry(vObj)) {
    $.ajax({
      url: gBASE_URL + "/countries/" + gCountryId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Cập nhật Quốc gia thành công");
        loadAllCountry();
        $("#info-country-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút xóa khách hàng
function onBtnDeleteCountryConfirmClick() {
  $.ajax({
    url: gBASE_URL + "/countries/" + gCountryId,
    type: "DELETE",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Xóa quốc gia thành công");
      loadAllCountry();
      $("#delete-country-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
    },
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function loadAllCountry() {
  $.ajax({
    url: gBASE_URL + "/countries",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showCountriesLstToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      showCountriesLstToTable(gCountryList);
    },
  });
}

//Hàm hiển thị dữ liệu trả về lên table
function showCountriesLstToTable(data) {
  gStt = 0;
  gCountryTable.clear();
  gCountryTable.rows.add(data);
  gCountryTable.draw();
}

//Hàm kiểm tra dữ liệu Khách hàng
function validateCountry(paramObj) {
  if (paramObj.countryCode == "") {
    toastr.warning("Mã Quốc Gia không được để trống");
    return false;
  }

  if (paramObj.countryName == "") {
    toastr.warning("Tên Quốc Gia không được để trống");
    return false;
  }
  
  return true;
}

//Xóa dữ liệu modal thêm mới
function clearData() {
  $("#modal-add-countryName").val("");
  $("#modal-add-countryCode").val("");
}

//Hàm hiển thị dữ liệu khách hàng qua form
function showCountryInfoToForm(paramData) {
  $("#modal-info-id").val(paramData.id);
  $("#modal-info-countryName").val(paramData.countryName);
  $("#modal-info-countryCode").val(paramData.countryCode);
}
