/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Dummy Data
var gVoucherList = JSON.parse(
  `[{"voucherCode":"000002","VoucherName":"Voucher Mừng sinh nhật","type":1,"useable":2,"create_date":"01/01/2023","voucherValue":0.2,"id":1,"expireDate":"01/02/2023"},{"voucherCode":"000003","VoucherName":"Voucher Mừng sinh nhật","type":1,"useable":1,"create_date":"01/01/2023","voucherValue":0.2,"id":2,"expireDate":"01/02/2023"},{"voucherCode":"000004","VoucherName":"Voucher Mừng sinh nhật","type":1,"useable":1,"create_date":"01/01/2023","voucherValue":0.2,"id":3,"expireDate":"01/02/2023"},{"voucherCode":"000005","VoucherName":"Voucher Mừng sinh nhật","type":1,"useable":1,"create_date":"01/01/2023","voucherValue":0.2,"id":4,"expireDate":"01/02/2023"}]`
);

var gStt = 0;
var gVoucherId = 0;

//Các cột của bảng
const gVOUCHER_COLS = [
  "id",
  "id",
  "voucherCode",
  "voucherName",
  "type",
  "voucherValue",
  "useable",
  "createDate",
  "expireDate",
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCUSTOMER_COLS_ACTION_COL = 0;
const gCUSTOMER_COLS_ID_COL = 1;
const gCUSTOMER_COLS_VOUCHER_CODE_COL = 2;
const gCUSTOMER_COLS_VOUCHER_NAME_COL = 3;
const gCUSTOMER_COLS_TYPE_COL = 4;
const gCUSTOMER_COLS_VOUCHER_VALUE_COL = 5;
const gCUSTOMER_COLS_USEABLE_COL = 6;
const gCUSTOMER_COLS_CREATEDATE_COL = 7;
const gCUSTOMER_COLS_EXPIREDATE_COL = 8;

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

var gVoucherTable = $("#voucher-table")
  .DataTable({
    responsive: true,
    lengthChange: false,
    autoWidth: false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    columns: [
      { data: gVOUCHER_COLS[gCUSTOMER_COLS_ACTION_COL] },
      { data: gVOUCHER_COLS[gCUSTOMER_COLS_ID_COL] },
      { data: gVOUCHER_COLS[gCUSTOMER_COLS_VOUCHER_CODE_COL] },
      { data: gVOUCHER_COLS[gCUSTOMER_COLS_VOUCHER_NAME_COL] },
      { data: gVOUCHER_COLS[gCUSTOMER_COLS_TYPE_COL] },
      { data: gVOUCHER_COLS[gCUSTOMER_COLS_VOUCHER_VALUE_COL] },
      { data: gVOUCHER_COLS[gCUSTOMER_COLS_USEABLE_COL] },
      { data: gVOUCHER_COLS[gCUSTOMER_COLS_CREATEDATE_COL] },
      { data: gVOUCHER_COLS[gCUSTOMER_COLS_EXPIREDATE_COL] },
    ],
    columnDefs: [
      {
        target: gCUSTOMER_COLS_ACTION_COL,
        render: renderAction,
      },
      {
        target: gCUSTOMER_COLS_USEABLE_COL,
        render: renderUseable,
      },
      {
        target: gCUSTOMER_COLS_VOUCHER_VALUE_COL,
        render: renderVoucherValue,
      },
      {
        target: gCUSTOMER_COLS_TYPE_COL,
        render: renderVoucherType,
      },
      {
        target: gCUSTOMER_COLS_CREATEDATE_COL,
        render: renderDateTime,
      },
      {
        target: gCUSTOMER_COLS_EXPIREDATE_COL,
        render: renderDateTime,
      },
    ],
  })

//OnpageLoading
//Đặt ngày trong modal tạo mới là ngày hiện tại
$("#modal-add-expireDate").val(dayjs(new Date()).format('YYYY-MM-DD'));
$("#modal-add-expireDate").prop("min", dayjs(new Date()).format('YYYY-MM-DD'))
$("#modal-info-expireDate").prop("min", dayjs(new Date()).format('YYYY-MM-DD'))

$("#btn-create-voucher").click(function () {
  clearData();
  $("#add-voucher-modal").modal();
});

//Gán sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
$("#btn-modal-add-voucher").click(function () {
  onBtnAddVoucherModalClick();
});

//Gán sự kiện khi ấn vào dòng
//Gán sự kiện khi ấn vào dòng trên table
$("#voucher-table").on("click", ".btn-edit", function () {
  onVoucherRowClick(this);
});

//Gán sự kiện khi ấn nút cập nhật thông tin trên modal info
$("#btn-modal-edit-voucher").click(function () {
  onBtnUpdateVoucherClick();
});

$("#voucher-table").on("click", ".btn-delete", function () {
  onDeleteVoucherClick(this);
});


$("#btn-modal-confirm-delete-voucher").click(function () {
  onBtnDeleteVoucherConfirmClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Lấy về danh sách thành phố
//Tải toàn bộ khách hàng về
loadAllVoucher();

//hàm xử lý sự kiện khi ấn icon thùng rác
function onDeleteVoucherClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gVoucherTable.row(vTableRow).data();
  gVoucherId = vRowData.id;
  $("#delete-voucher-modal").modal();
}

//Hàm xử lý sự kiện khi ấn vào icon kính lúp
function onVoucherRowClick(elementRow) {
  $("#info-voucher-modal").modal();
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gVoucherTable.row(vTableRow).data();
  gVoucherId = vRowData.id;
  //(gVoucherId);
  showVoucherInfoToForm(vRowData);
}
//Xử lý sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
function onBtnAddVoucherModalClick() {
  var vObj = {
    voucherCode: $("#modal-add-voucherCode").val().trim(),
    voucherName: $("#modal-add-voucherName").val().trim(),
    type: $("#modal-add-type").val(),
    voucherValue: $("#modal-add-voucherValue").val(),
    expireDate: $("#modal-add-expireDate").val(),
  };
  (vObj);
  if (validateVoucher(vObj)) {
    $.ajax({
      url: gBASE_URL + "/vouchers",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Thêm mới khách hàng thành công");
        loadAllVoucher();
        $("#add-voucher-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút cập nhật thông tin
function onBtnUpdateVoucherClick() {
  var vObj = {
    voucherCode: $("#modal-info-voucherCode").val().trim(),
    voucherName: $("#modal-info-voucherName").val().trim(),
    type: $("#modal-info-type").val(),
    voucherValue: $("#modal-info-voucherValue").val(),
    expireDate: $("#modal-info-expireDate").val(),
    useable: $("#modal-info-useable").val(),
  };

  if (validateVoucher(vObj)) {
    $.ajax({
      url: gBASE_URL + "/vouchers/" + gVoucherId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Cập nhật voucher thành công");
        loadAllVoucher();
        $("#info-voucher-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút xóa khách hàng
function onBtnDeleteVoucherConfirmClick() {
  $.ajax({
    url: gBASE_URL + "/vouchers/" + gVoucherId,
    type: "DELETE",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Xóa Voucher thành công");
      loadAllVoucher();
      $("#delete-voucher-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
    },
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function loadAllVoucher() {
  $.ajax({
    url: gBASE_URL + "/vouchers",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showVoucherToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
    },
  });
}

//Hàm hiển thị dữ liệu trả về lên table
function showVoucherToTable(data) {
  gStt = 0;
  gVoucherTable.clear();
  gVoucherTable.rows.add(data);
  gVoucherTable.draw();
  gVoucherTable.buttons().container().appendTo("#voucher-table");
}

//Hàm kiểm tra dữ liệu Khách hàng
function validateVoucher(paramObj) {
  if (paramObj.voucherCode == "") {
    toastr.warning("Mã voucher không được để trống");
    return false;
  }

  if (paramObj.voucherName == "") {
    toastr.warning("Tên voucher không được để trống");
    return false;
  }

  if (paramObj.voucherValue <= 0) {
    toastr.warning("Giá trị voucher cần lớn hơn 0");
    return false;
  }

  return true;
}

//Xóa dữ liệu modal thêm mới
function clearData() {
  $("#modal-add-voucherName").val("");
  $("#modal-add-voucherCode").val("");
  $("#modal-add-type").val(1);
  $("#modal-add-expireDate").val(dayjs(new Date()).format('YYYY-MM-DD'));
}

//Hàm hiển thị dữ liệu khách hàng qua form
function showVoucherInfoToForm(paramData) {
  $("#modal-info-voucherName").val(paramData.voucherName);
  $("#modal-info-voucherCode").val(paramData.voucherCode);
  $("#modal-info-type").val(paramData.type);
  $("#modal-info-expireDate").val(renderDateTimeInput(paramData.expireDate));
  $("#modal-info-voucherValue").val(paramData.voucherValue);
  $("#modal-info-createDate").val(renderDateTimeInput(paramData.createDate));
  $("#modal-info-useable").val(paramData.useable);
}
