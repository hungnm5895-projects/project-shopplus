/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gUrl = "http://localhost:8080";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    //Gán sự kiện khi ấn nút quên mật khẩu
    $("#btn-change-password").click(function () {
        onBtnChangePasswordClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm xử lý ự kiện khi ấn reset mật khẩu
function onBtnChangePasswordClick() {
    var vUserData = {
        username: $("#inp-username").val().trim(),
        password: $("#inp-cr-pw").val(),
    };
    var vNewPassword = $("#inp-new-pw").val();
    (vUserData);
    (vNewPassword);
    if (validateUserData(vUserData)) {
        
        $.ajax({
            url: gUrl + "/users/changePassword/" + vNewPassword,
            type: "PUT",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vUserData),
            success: function (responseObject) {
                toastr.success("Đổi mật khẩu thành công");
            },
            error: function (xhr) {
                toastr.error("Tài khoản hoặc mật khẩu không chính xác");
            },
        });
    }
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm kiểm tra dữ liệu đầu vào
function validateUserData(paramObj) {
    if (paramObj.username == "") {
        toastr.warning("Cần nhập  tài khoản");
        return false;
    }

    if (paramObj.password == "") {
        toastr.warning("Cần nhập mật khẩu hiện tại");
        return false;
    }

    if (paramObj.password == $("#inp-new-pw").val()) {
        toastr.warning("Mật khẩu mới không được trùng mật khẩu hiện tại");
        return false;
    }

    if ($("#inp-new-pw").val() == "") {
        toastr.warning("Cần nhập mật khẩu mới");
        return false;
    }

    if ($("#inp-re-pw").val() == "") {
        toastr.warning("Cần nhập lại mật khẩu mới");
        return false;
    }

    if ($("#inp-re-pw").val() != $("#inp-new-pw").val()) {
        toastr.warning("Cần nhập lại mật khẩu trùng mật khẩu mới");
        return false;
    }

    return true;
}