//RG1

//RG2
var gCartLst;
//OnpageLoading
getCart();

$(document).on("click", ".btn-card", function () {
    var vId = $(this).data("mydata");
    ((vId));
    addProduct(vId, 1); //Mỗi lần ấn thì thêm sản phẩm 1
    toastr.success('Đã thêm sản phẩm vào giỏ hàng')

});

//RG3
//Kiểm tra local Storeage
function getCart() {
    vCartLocal = localStorage.getItem("localCart");
    if (vCartLocal == null) {
        vCartLocal = "[]";
    }
    gCartLst = JSON.parse(vCartLocal);
    (gCartLst);
    showCartIcon(gCartLst);
}

//RG4
//Check Product đã trong rỏ hàng chưa
function checkProduct(paramsId) {
    var found = false;
    var index = 0;
    if (gCartLst.length == 0) {
        return found;
    } else {
        while (!found && index < gCartLst.length) {
            if (gCartLst[index].id == paramsId) {
                found = true;
            } else {
                index++;
            }
        }
    }
    return found;
}


//Thêm sản phẩm
function addProduct(paramsId, paramQty) {
    if (gCartLst.length == 0) {
        gCartLst.push({ id: paramsId, qty: paramQty });
        (gCartLst);
        localStorage.setItem("localCart", JSON.stringify(gCartLst));
        getCart();
    } else if (checkProduct(paramsId) == false) {
        gCartLst.push({ id: paramsId, qty: paramQty });
        localStorage.setItem("localCart", JSON.stringify(gCartLst));
        getCart();
    } else {
        for (let index = 0; index < gCartLst.length; index++) {
            if (gCartLst[index].id == paramsId) {
                gCartLst[index].qty += paramQty;
                localStorage.setItem("localCart", JSON.stringify(gCartLst));
            }
        }
        getCart();
    }
}
//Giảm sản phẩm

function minusProduct(paramsId) {
    for (let index = 0; index < gCartLst.length; index++) {
        if (gCartLst[index].id == paramsId && gCartLst[index].qty >= 2) {
            gCartLst[index].qty = gCartLst[index].qty - 1;
            localStorage.setItem("localCart", JSON.stringify(gCartLst));
            getCart();
        } else if (gCartLst[index].id == paramsId && gCartLst[index].qty == 1) {
            gCartLst.splice(index, 1);
            localStorage.setItem("localCart", JSON.stringify(gCartLst));
            getCart();
        }
    }
}

//Giảm sản phẩm

function removeProduct(paramsId) {
    for (let index = 0; index < gCartLst.length; index++) {
        if (gCartLst[index].id == paramsId) {
            gCartLst.splice(index, 1);
            localStorage.setItem("localCart", JSON.stringify(gCartLst));
        }
    }
    getCart();
}

//Xóa toàn bộ local

function clearCart() {
    localStorage.removeItem("localCart");
    getCart();
}

//Hàm hiển thị icon cart
function showCartIcon(paramData) {
    if (paramData.length > 0) {
        (paramData.length);
        $("#cart-sum").html(paramData.length);
        $("#cart-info-sum").html(paramData.length);
    } else {
        $("#cart-sum").html("0");
        $("#cart-info-sum").html(0);
    }
}
