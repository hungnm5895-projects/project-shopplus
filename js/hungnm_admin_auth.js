/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gIsLogin = getCookie("loginSession");

var gLoginSesstion ;
var gtoken ;
var gRole ;
var gId ;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {

    onPageLoadingAdminAuth();

    

});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoadingAdminAuth() {
    if (gIsLogin == "") {
        window.location.href = "index.html";
    } else {

        gLoginSesstion = JSON.parse(gIsLogin);
        gtoken = gLoginSesstion.token;
        gRole = gLoginSesstion.authorities;
        gId = gLoginSesstion.id;
        //console.log(gLoginSesstion);

        //Hiển thị các menu trên icon avatar theo từng case
        switch (gRole[0]) {
            case "ROLE_ADMIN":
                //$("#view-real-estate").removeClass("d-none"); //Hiện nút cho phép xem DS BDS
                //$("#view-customer").removeClass("d-none"); //Hiện nút cho phép xem DS BDS
                //$("#view-admin-lte").removeClass("d-none"); //Hiện nút cho phép xem DS BDS
                break;
            case "ROLE_CUSTOMER":
                window.location.href = "index.html";
                break;
            case "ROLE_AGENCY":
                //$("#view-real-estate").removeClass("d-none"); //Hiện nút cho phép xem DS BDS
                //$("#view-customer").removeClass("d-none");
                break;
            case "ROLE_MANAGER":
                //$("#view-real-estate").removeClass("d-none"); //Hiện nút cho phép xem DS BDS
                //$("#view-customer").removeClass("d-none"); //Hiện nút cho phép xem DS BDS
                //$("#view-admin-lte").removeClass("d-none");
                break;
            default:
                //$("#view-real-estate").removeClass("d-none");
        }
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

//Hàm get Cookie
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  
  //Hàm setCookie
  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  