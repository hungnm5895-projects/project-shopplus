/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Dummy Data

var gNotiId = 0;
var gImgProductUrl = "";
var gBrandLst = [];
var gNotiLst = JSON.parse(`[{"id":1,"customerId":3,"notiContent":"Test noti edit","createDate":"2023-11-19T00:20:24.000+00:00","notiLink":"gôgle","notiSeen":0}]`);
var gCountriesLst = [];

//Các cột của bảng
const gNOTI_COLS = [
  "id",
  "id",
  "customerId",
  "notiContent",
  "createDate",
  "notiSeen"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gNOTI_COLS_ACTION_COL = 0;
const gNOTI_COLS_ID_COL = 1;
const gNOTI_COLS_CUSTOMER_COL = 2;
const gNOTI_COLS_CONTENT_COL = 3;
const gNOTI_COLS_CREATE_COL = 4;
const gNOTI_COLS_SEEN_COL = 5;
;

var gNotiTable = $("#notification-table").DataTable({
  responsive: true,
  lengthChange: false,
  autoWidth: false,
  buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
  columns: [
    { data: gNOTI_COLS[gNOTI_COLS_ACTION_COL] },
    { data: gNOTI_COLS[gNOTI_COLS_ID_COL] },
    { data: gNOTI_COLS[gNOTI_COLS_CUSTOMER_COL] },
    { data: gNOTI_COLS[gNOTI_COLS_CONTENT_COL] },
    { data: gNOTI_COLS[gNOTI_COLS_CREATE_COL] },
    { data: gNOTI_COLS[gNOTI_COLS_SEEN_COL] }
  ],
  columnDefs: [
    {
      target: gNOTI_COLS_ACTION_COL,
      render: renderAction
    },
    {
      target: gNOTI_COLS_CREATE_COL,
      render: renderFullDateTime
    }
    ,
    {
      target: gNOTI_COLS_SEEN_COL,
      render: renderSeenNoti
    }
  ],
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

//OnpageLoading

//Gán sự kiện khi ấn nút thêm mới campain
$("#btn-create-notification").click(function () {
  clearData();
  $("#add-notification-modal").modal();
});


//Gán sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
$("#btn-modal-add-noti").click(function () {
  onBtnAddNotiModalClick();
});

//Gán sự kiện khi ấn vào biểu tượng kính lúp trên bảng
$("#notification-table").on("click", ".btn-edit", function () {
  onIconInfoInRowClick(this);
});

//Gán sự kiện khi ấn nút cập nhật thông tin trên modal info
$("#btn-modal-edit-noti").click(function () {
  onBtnUpdateNotiClick();
});

//Gán sự kiện khi ấn vào biểu tượng thùng rác
$("#product-table").on("click", ".btn-delete", function () {
  onDeleteProductClick(this);
});

$("#btn-modal-confirm-delete-product").click(function () {
  onBtnDeleteProductClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */


//Tải toàn bộ khách hàng về
loadAllNoti();


//Hàm xử lý sự kiện khi ấn vào icon kính lúp
function onIconInfoInRowClick(elementRow) {
  $("#info-notification-modal").modal();
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gNotiTable.row(vTableRow).data();
  gNotiId = vRowData.id;
  (vRowData);
  showNotiInfoToForm(vRowData);
}
//Xử lý sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
function onBtnAddNotiModalClick() {
  var vObj = {
    notiContent: $("#input-add-notiContent").val().trim(),
    notiLink: $("#input-add-notiLink").val().trim(),
    customerId: $("#input-add-customerLst").val().split(";")
  };

  (JSON.stringify(vObj));


  if (validateNoti(vObj)) {
    $.ajax({
      url: gBASE_URL + "/notifications/sendNoti",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Gửi noti thành công");
        loadAllNoti();
        $("#add-notification-modal").modal("hide");
      },
      error: function (ajaxContext) {
        var errorLog = JSON.parse(ajaxContext.responseText)
        toastr.error(errorLog.errors[0]);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút cập nhật thông tin
function onBtnUpdateNotiClick() {
  var vObj = {
    customerId: $("#input-info-customerId").val().trim(),
    notiLink: $("#input-info-notiLink").val().trim(),
    notiContent: $("#input-info-notiContent").val(),
  };

  if (validateNoti(vObj)) {
    $.ajax({
      url: gBASE_URL + "/notifications/" + gNotiId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Cập nhật Noti thành công");
        loadAllNoti();
        $("#info-notification-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//hàm xử lý sự kiện khi ấn icon thùng rác
function onDeleteProductClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gNotiTable.row(vTableRow).data();
  gNotiId = vRowData.id;
  $("#delete-product-modal").modal();
}

//Hàm xử lý sự kiện khi ấn nút xóa khách hàng
function onBtnDeleteProductClick() {
  $.ajax({
    url: gBASE_URL + "/products/" + gNotiId,
    type: "DELETE",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Xóa thương hiệu thành công");
      loadAllNoti();
      $("#delete-product-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
    },
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/


function loadAllNoti() {
  $.ajax({
    url: gBASE_URL + "/notifications",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showNotiLstToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      showNotiLstToTable(gNotiLst);
    },
  });
}

//Hàm hiển thị dữ liệu trả về lên table
function showNotiLstToTable(data) {
  gStt = 0;
  gNotiTable.clear();
  gNotiTable.rows.add(data);
  gNotiTable.draw();
}

//Hàm kiểm tra dữ liệu Khách hàng
function validateNoti(paramObj) {
  if (paramObj.customerId == "") {
    toastr.warning("Danh sách ID khách hàng không được để trống");
    return false;
  }

  if (paramObj.notiLink == "") {
    toastr.warning("Link đính kèm không được để trống");
    return false;
  }

  if (paramObj.notiContent == "") {
    toastr.warning("Nội dung noti không được để trống");
    return false;
  }

  return true;
}

//Xóa dữ liệu modal thêm mới
function clearData() {
  $("#input-add-notiContent").val("");
  $("#input-add-notiLink").val("");
  $("#input-add-customerLst").val("");
}

//Hàm hiển thị dữ liệu khách hàng qua form
function showNotiInfoToForm(paramData) {
  $("#input-info-customerId").val(paramData.customerId);
  $("#input-info-notiLink").val(paramData.notiLink);
  $("#input-info-notiContent").val(paramData.notiContent);
  $("#input-info-createDate").html(renderFullDateTime(paramData.createDate));
  $("#input-info-notiSeen").html(renderSeenNoti(paramData.notiSeen));
}

