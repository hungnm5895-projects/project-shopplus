/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Dummy Data
var gCustomerList = JSON.parse(
  `[{"name":"hung","address":"Ngọc Thụy","phone":"0964272535","createDate":"11/12/2023","email":"hungnm@gmail.com","userId":1,"id":1,"provinceId":2,"createdBy":0},{"name":"hung","address":"Ngọc Thụy","phone":"0964272535","email":"hungnm@gmail.com","userId":2,"id":2,"provinceId":2,"createdBy":0,"createDate":"11/12/2023"},{"name":"hung","address":"Ngọc Thụy","phone":"0964272535","email":"hungnm@gmail.com","userId":3,"id":3,"provinceId":2,"createdBy":0,"createDate":"11/12/2023"},{"name":"hung","address":"Ngọc Thụy","phone":"0964272535","email":"hungnm@gmail.com","userId":3,"id":4,"provinceId":2,"createdBy":0,"createDate":"11/12/2023"}]`
);

var gStt = 0;
var gCustomerId = 0;

//Các cột của bảng
const gCUSTOMER_COLS = [
  "id",
  "name",
  "address",
  "phone",
  "email",
  "provinceId",
  "id"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCUSTOMER_COLS_ID_COL = 0;
const gCUSTOMER_COLS_NAME_COL = 1;
const gCUSTOMER_COLS_ADDRESS_COL = 2;
const gCUSTOMER_COLS_PHONE_COL = 3;
const gCUSTOMER_COLS_EMAIL_COL = 4;
const gCUSTOMER_COLS_PROVINCE_COL = 5;
const gCUSTOMER_COLS_ORDER_COL = 5;


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
/* 
$("#customer-table")
  .DataTable({
    responsive: true,
    lengthChange: false,
    autoWidth: false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    columns: [
      { data: gCUSTOMER_COLS[gCUSTOMER_COLS_STT_COL] },
      { data: gCUSTOMER_COLS[gCUSTOMER_COLS_ID_COL] },
      { data: gCUSTOMER_COLS[gCUSTOMER_COLS_NAME_COL] },
      { data: gCUSTOMER_COLS[gCUSTOMER_COLS_ADDRESS_COL] },
      { data: gCUSTOMER_COLS[gCUSTOMER_COLS_PHONE_COL] },
      { data: gCUSTOMER_COLS[gCUSTOMER_COLS_EMAIL_COL] },
      { data: gCUSTOMER_COLS[gCUSTOMER_COLS_PROVINCE_COL] },
    ],
    columnDefs: [
      {
        target: gCUSTOMER_COLS_STT_COL,
        render: renderStt,
        class: "text-center align-items-center",
      },
      {
        target: gCUSTOMER_COLS_PROVINCE_COL,
        render: renderProvince
      }
    ],
  })
  .buttons()
  .container()
  .appendTo("#customer-table_wrapper .col-md-6:eq(0)");

  */

var gCustomerTable = $("#customer-table").DataTable({
  responsive: true,
  lengthChange: false,
  autoWidth: false,
  buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
  columns: [
    { data: gCUSTOMER_COLS[gCUSTOMER_COLS_ID_COL] },
    { data: gCUSTOMER_COLS[gCUSTOMER_COLS_NAME_COL] },
    { data: gCUSTOMER_COLS[gCUSTOMER_COLS_ADDRESS_COL] },
    { data: gCUSTOMER_COLS[gCUSTOMER_COLS_PHONE_COL] },
    { data: gCUSTOMER_COLS[gCUSTOMER_COLS_EMAIL_COL] },
    { data: gCUSTOMER_COLS[gCUSTOMER_COLS_PROVINCE_COL] },
    { data: gCUSTOMER_COLS[gCUSTOMER_COLS_ORDER_COL] },

  ],
  columnDefs: [
    {
      target: gCUSTOMER_COLS_PROVINCE_COL,
      render: renderProvince
    },
    {
      target: gCUSTOMER_COLS_ORDER_COL,
      render: renderOrderCount
    }
  ],
});
//OnpageLoading
$("#btn-create-customer").click(function () {
  clearData();
  $("#add-customer-modal").modal();
});

//Gán sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
$("#btn-modal-add-customer").click(function () {
  onBtnAddCustomerModalClick();
});

//Gán sự kiện khi ấn vào dòng
//Gán sự kiện khi ấn vào dòng trên table
$("#customer-table").on("click", "tr", function () {
  onVoucherRowClick(this);
});

//Gán sự kiện khi ấn nút cập nhật thông tin trên modal info
$("#btn-modal-edit-customer").click(function () {
  onBtnUpdateCustomerClick();
});

//Gán sự kiện khi ấn nút xóa thông tin trên modal info
$("#btn-modal-delete-customer").click(function () {
  $("#delete-customer-modal").modal();
});

$("#btn-modal-confirm-delete-customer").click(function () {
  onBtnDeleteCustomerClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Lấy về danh sách thành phố
loadAllProvince();

//Tải toàn bộ khách hàng về
loadAllCustomer();

//Hàm xử lý sự kiện khi ấn vào dòng trên table
function onVoucherRowClick(elementRow) {
  var vRowData = gCustomerTable.row(elementRow).data();
  gCustomerId = vRowData.id;
  $("#info-customer-modal").modal();
  showCustomerInfoToForm(vRowData);
}
//Xử lý sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
function onBtnAddCustomerModalClick() {
  var vObj = {
    name: $("#modal-add-name").val().trim(),
    address: $("#modal-add-address").val().trim(),
    phone: $("#modal-add-phone").val().trim(),
    email: $("#modal-add-email").val().trim(),
    provinceId: $("#modal-add-province").val().trim(),
  };

  if (validateCustomer(vObj)) {
    $.ajax({
      url: gBASE_URL + "/customers",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Thêm mới khách hàng thành công");
        loadAllCustomer();
        $("#add-customer-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút cập nhật thông tin
function onBtnUpdateCustomerClick() {
  var vObj = {
    name: $("#modal-info-name").val().trim(),
    address: $("#modal-info-address").val().trim(),
    phone: $("#modal-info-phone").val().trim(),
    email: $("#modal-info-email").val().trim(),
    provinceId: $("#modal-info-province").val().trim(),
  };

  if (validateCustomer(vObj)) {
    $.ajax({
      url: gBASE_URL + "/customers/" + gCustomerId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Cập nhật khách hàng thành công");
        loadAllCustomer();
        $("#info-customer-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút xóa khách hàng
function onBtnDeleteCustomerClick() {
  $.ajax({
    url: gBASE_URL + "/customers/" + gCustomerId,
    type: "DELETE",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Xóa khách hàng khách hàng thành công");
      loadAllCustomer();
      $("#info-customer-modal").modal("hide");
      $("#delete-customer-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
    },
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function loadAllCustomer() {
  $.ajax({
    url: gBASE_URL + "/customers",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showCustomerToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      //showCustomerToTable(gCustomerList);
    },
  });
}

//Hàm hiển thị dữ liệu trả về lên table
function showCustomerToTable(customerData) {
  gStt = 0;
  gCustomerTable.clear();
  gCustomerTable.rows.add(customerData);
  gCustomerTable.draw();
  gCustomerTable.buttons().container().appendTo("#customer-table");
}

//Hàm gọi api lấy ra provinces
function loadAllProvince() {
  $.ajax({
    url: gBASE_URL + "/provinces",
    method: "GET",
    async: false,
    success: function (pObjRes) {
      gProvinceLst = pObjRes;
      loadDataToNewProvinceSelect(gProvinceLst);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
    },
  });
}

//Hàm kiểm tra dữ liệu Khách hàng
function validateCustomer(paramObj) {
  if (paramObj.name == "") {
    toastr.warning("Tên khách hàng không được để trống");
    return false;
  }

  if (paramObj.address == "") {
    toastr.warning("Địa chỉ khách hàng không được để trống");
    return false;
  }

  if (paramObj.phone == "") {
    toastr.warning("Số điện thoại khách hàng không được để trống");
    return false;
  }

  if (paramObj.email == "") {
    toastr.warning("Email khách hàng không được để trống");
    return false;
  }

  if (!validateEmail(paramObj.email)) {
    toastr.warning("Email không hợp lệ");
    return false;
  }

  if (paramObj.provinceId == 0) {
    toastr.warning("Tỉnh/Thành phố cần được chọn");
    return false;
  }

  return true;
}

//Xóa dữ liệu modal thêm mới
function clearData() {
  $("#modal-add-name").val("");
  $("#modal-add-address").val("");
  $("#modal-add-phone").val("");
  $("#modal-add-email").val("");
  $("#modal-add-province").val(0);
}

//Hàm hiển thị dữ liệu khách hàng qua form
function showCustomerInfoToForm(paramData) {
  $("#modal-info-name").val(paramData.name);
  $("#modal-info-address").val(paramData.address);
  $("#modal-info-phone").val(paramData.phone);
  $("#modal-info-email").val(paramData.email);
  $("#modal-info-province").val(paramData.provinceId);
  $("#modal-info-userId").html(paramData.userId);
  $("#modal-info-createDate").html(renderDateTime(paramData.createDate));
  $("#modal-info-createdBy").html(paramData.createdBy);
}

function loadDataToNewProvinceSelect(paramProvinceData) {
  var vProvinceSelectElement = $("#modal-add-province");
  for (i = 0; i < paramProvinceData.length; i++) {
    let bProvinceOption = $("<option/>");
    bProvinceOption.prop("value", paramProvinceData[i].id);
    bProvinceOption.prop("text", paramProvinceData[i].name);
    vProvinceSelectElement.append(bProvinceOption);
  };

  var vProvinceSelectElement2 = $("#modal-info-province");
  for (i = 0; i < paramProvinceData.length; i++) {
    let bProvinceOption = $("<option/>");
    bProvinceOption.prop("value", paramProvinceData[i].id);
    bProvinceOption.prop("text", paramProvinceData[i].name);
    vProvinceSelectElement2.append(bProvinceOption);
  };
}

function renderOrderCount(id) {
  
  $.ajax({
    url: gBASE_URL + "/orders/customer/" + id,
    method: "GET",
    async: false,
    success: function (pObjRes) {
      return pObjRes.length;
    },
    error: function (pXhrObj) {
      return 0;
    },
  });
}