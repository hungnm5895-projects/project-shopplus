/*/
Write by Hungnm
/*/

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gDataTest = JSON.parse(
  `[{"id":1,"name":"Á Đông","description":"Á Đông à công ty được thành lập từ những năm 1995. Đơn vị này đã hình thành nên một bộ máy làm việc nhiều năm kinh nghiệm, nhiệt tình trách nhiệm cao. ","projects":"1,2","address":123526,"phone":"02838447096 ","phone2":"0969993037","fax":"02838447096 ","email":"dongaAbc@gmail.com","website":"https://adong.com.vn/","note":"Công ty lớn"},{"id":2,"name":"Aeros","description":"Công Ty Chuyên Thi công Nội Thất Tại Sài Gòn.","projects":null,"address":123526,"phone":"0901 806 999 ","phone2":"0901 806 888","fax":"0901 806 999 ","email":"abc@gmail.com","website":"https://aeros.vn/","note":"Công ty lớn"},{"id":3,"name":"Hải Nam","description":"abcd","projects":"1","address":0,"phone":"0965356231","phone2":"0965356232","fax":"0965356231","email":"","website":"","note":""}]`
);

var gBASE_URL = "http://localhost:8080";
var gStt = 0;
var gUserId = 0;

/*Các biến auth
var gIsLogin = getCookie("loginSession");
var gLoginSesstion = JSON.parse(gIsLogin);;
var gtoken = gLoginSesstion.token;;
var gRole = gLoginSesstion.authorities;;
var gId = gLoginSesstion.id;
*/
//Các cột của bảng
const gUSER_COLS = [
  "stt",
  "id",
  "username",
  "password",
  "email",
  "secretAnswer",
  "createdAt",
  "roles[0].roleName",
  "deleted"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gUSER_STT_COL = 0;
const gUSER_COLS_ID_COL = 1;
const gUSER_COLS_USERNAME_COL = 2;
const gUSER_COLS_PASSWORD_COL = 3;
const gUSER_COLS_EMAIL_COL = 4;
const gUSER_COLS_SERCRET_ANSWER_COL = 5;
const gUSER_COLS_CREATE_COL = 6;
const gUSER_COLS_ROLE_COL = 7;
const gUSER_COLS_DEL_COL = 8;

//Khai báo dataTable Customer
var gContructionUnitTable = $("#table-user").DataTable({
  columns: [
    { data: gUSER_COLS[gUSER_STT_COL] },
    { data: gUSER_COLS[gUSER_COLS_ID_COL] },
    { data: gUSER_COLS[gUSER_COLS_USERNAME_COL] },
    { data: gUSER_COLS[gUSER_COLS_PASSWORD_COL] },
    { data: gUSER_COLS[gUSER_COLS_EMAIL_COL] },
    { data: gUSER_COLS[gUSER_COLS_SERCRET_ANSWER_COL] },
    { data: gUSER_COLS[gUSER_COLS_CREATE_COL] },
    { data: gUSER_COLS[gUSER_COLS_ROLE_COL] },
    { data: gUSER_COLS[gUSER_COLS_DEL_COL] },
  ],
  columnDefs: [
    {
      target: gUSER_STT_COL,
      render: rederStt,
      class: "text-center align-items-center",
    },
    {
      target: gUSER_COLS_DEL_COL,
      render: renderCheck,
      class: "text-center align-items-center",
    },
    {
      target: gUSER_COLS_CREATE_COL,
      render: function (params) {
        if (params != null) {
          return dayjs(params).format('DD/MM/YYYY')
        } else {
          return ""
        }
      },
      class: "text-center align-items-center",
    },
  ],
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  //gán sự kiện khi tải trang
  onPageLoading();

  //Gán sự kiện khi ấn nút thêm mới user
  $("#btn-create-user").click(function () {
    $("#add-user-modal").modal();
  });

  //Gán sự kiện khi ấn nút thêm mới user
  $("#btn-modal-add-user").click(function () {
    //Xóa trắng dữ liệu cũ
    onBtnAddUserOnModalClick();
  });

  //Gán sự kiện khi ấn vào dòng trên table
  $("#table-user").on("click", "tr", function () {
    onUserRowClick(this);
  });

  
  //Gán ự kiện khi ấn nút cập nhật user
  $("#btn-modal-edit-user").click(function () {
    onBtnUpdateUserOnModalClick();
  });



  //Gán ự kiện khi ấn nút xóa user
  $("#btn-modal-delete-user").click(function () {
    $("#delete-user-modal").modal();
  });

  //Gán sự kiện khi confirm xóa user
  $("#btn-modal-confirm-delete-user").click(function () {
    onBtnDeleteContructionOnModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm xử lý sự kiện khi xác nhận xóa user
// input/start: Không
// ouput/end: user bị xóa
function onBtnDeleteContructionOnModalClick() {
  $.ajax({
    url: gBASE_URL + "/contructionContructors/" + gUserId,
    type: "DELETE",
    //headers: {
    //  "Authorization": "Token " + gtoken
    //},
    success: function (Res) {
      toastr.success("Xóa nhà thầu thành công");
      loadAllUser();
      $("#delete-user-modal").modal("hide");
      $("#info-user-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error("Có lỗi xảy ra, vui lòng thử lại sau");
    },
  });
}

// Hàm xử lý sự kiện khi ấn vào nút cập nhật user
// input/start: Không
// ouput/end: user được cập nhật
function onBtnUpdateUserOnModalClick() {
  //01) đọc dữ liệu và lưu vào một đối tượng
  var vDataObj = {
    email: $("#modal-info-email").val().trim(),
    secretAnswer: $("#modal-info-secretAnswer").val().trim(),
    username: $("#modal-info-username").val().trim(),
    password: $("#modal-info-password").val().trim(),
  };

  var vRoleKey = $("#modal-info-role").val();
  //02) kiểm tra dữ liệu. Nếu hợp lệ thì thực hiện bước 03 và 04
  var vIsValite = validateUserData(vDataObj);
  //03) gọi server, api (gửi yêu cầu request tới server)
  if (vIsValite) {
    $.ajax({
      url: gBASE_URL + "/users/update/" + gUserId + "/" + vRoleKey,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vDataObj),
      //headers: {
      //  "Authorization": "Token " + gtoken
      //},
      success: function (Res) {
        toastr.success("Cập nhật tài khoản thành công");
        loadAllUser();
        $("#info-user-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error("Có lỗi xảy ra, vui lòng thử lại sau");
      },
    });
  }
}

// Hàm xử lý sự kiện khi ấn vào các dòng
// input/start: Không
// ouput/end: dữ liệu được hiển thị lên
function onUserRowClick(row) {
  var vRowData = gContructionUnitTable.row(row).data();
  gUserId = vRowData.id;
  $("#info-user-modal").modal();
  showUserInfoToForm(vRowData);
}

// Hàm xử lý sự kiện khi ấn nút thêm mới trên modal thêm mới
// input/start: Không
// ouput/end: user được tạo mới
function onBtnAddUserOnModalClick() {
  //01) đọc dữ liệu và lưu vào một đối tượng
  var vDataObj = {
    username: $("#modal-add-username").val().trim(),
    password: $("#modal-add-password").val(),
    email: $("#modal-add-email").val().trim(),
    secretAnswer: $("#modal-add-secretAnswer").val().trim()
  };

  var vRoleKey = $("#modal-add-role").val();
  //02) kiểm tra dữ liệu. Nếu hợp lệ thì thực hiện bước 03 và 04
  var vIsValite = validateUserData(vDataObj);
  //03) gọi server, api (gửi yêu cầu request tới server)
  if (vIsValite) {
    $.ajax({
      url: gBASE_URL + "/createUser/" + vRoleKey,
      type: "POST",
      //headers: {
      //  "Authorization": "Token " + gtoken
      //},
      contentType: "application/json",
      data: JSON.stringify(vDataObj),
      success: function (Res) {
        toastr.success("Tạo tài khoản thành công");
        loadAllUser();
        $("#add-user-modal").modal("hide");
        clearNewModal();
      },
      error: function (ajaxContext) {
        toastr.error("Có lỗi xảy ra, vui lòng thử lại sau");
      },
    });
  }
  //04) xử lý dữ liệu front-end, hiển thị khi server phản hồi về
}

// Hàm xử lý sự kiện khi tải trang
// input/start: Không
// ouput/end: kết quả trả lại, hoặc trạng thái cuối
function onPageLoading() {
  loadAllUser();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm thu thập dữ liệu dữ liệu user đổ vào Obj
// input/start: Obj chưa có dữ liệu
// ouput/end: Obj  có dữ liệu
function collectUpdateContructionData(paramObj) {
  paramObj.name = $("#modal-info-name").val().trim();
  paramObj.description = $("#modal-info-description").val().trim();
  paramObj.projects = $("#modal-info-projects").val().trim();
  paramObj.address = $("#modal-info-address").val().trim();
  paramObj.phone = $("#modal-info-phone").val().trim();
  paramObj.phone2 = $("#modal-info-phone2").val().trim();
  paramObj.fax = $("#modal-info-fax").val().trim();
  paramObj.email = $("#modal-info-email").val().trim();
  paramObj.website = $("#modal-info-website").val().trim();
  paramObj.note = $("#modal-info-note").val().trim();
  return paramObj;
}

// hàm hiển thị chi tiết user
// input/start: Obj có dữ liệu
// ouput/end: hiển thị lên modal info
function showUserInfoToForm(paramData) {
  $("#modal-info-username").val(paramData.username);
  $("#modal-info-email").val(paramData.email);
  $("#modal-info-password").val(paramData.password);
  $("#modal-info-secretAnswer").val(paramData.secretAnswer);
  $("#modal-info-createdAt").val(dayjs(paramData.createdAt).format('DD/MM/YYYY'));
  $("#modal-info-role").val(paramData.roles[0].roleKey);
}

// hàm kiểm tra dữ liệu dữ liệu user đổ vào Obj
// input/start: Obj có dữ liệu
// ouput/end: true/failse
function validateUserData(paramObj) {
  if (paramObj.username == "") {
    toastr.warning('Cần nhập tên đăng nhập')
    return false;
  }

  if (!validateEmail(paramObj.email)) {
    toastr.warning('Cần nhập thông tin email hợp lệ')
    return false;
  }

  if (paramObj.secretAnswer == "") {
    toastr.warning('Cần nhập câu hỏi bảo mật')
    return false;
  }

  if (paramObj.password == "") {
    toastr.warning('Cần nhập mật khẩu')
    return false;
  }

  return true;
}


// hàm hiển thị dữ liệu user ra table
// input/start: dữ liệu user
// ouput/end: dữ liệu được hiển thị
function showUserToTable(paramData) {
  gStt = 0;
  gContructionUnitTable.clear();
  gContructionUnitTable.rows.add(paramData);
  gContructionUnitTable.draw();
}

//hàm xử lý tải về toan bộ user
function loadAllUser() {
  $.ajax({
    url: gBASE_URL + "/usersLst",
    method: "GET",
    success: function (pObjRes) {
      showUserToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj.responseText);
    },
  });
}

//Hàm render tạo STT tăng dần
function rederStt() {
  gStt++;
  return gStt;
}


//Hàm xóa trắng dữ liệu modal thêm mới
function clearNewModal() {
  $("#modal-add-username").val("");
  $("#modal-add-email").val("");
  $("#modal-add-secretAnswer").val("");
  $("#modal-add-password").val("");
  $("#modal-add-role").val("ROLE_SELLER");
}

function renderCheck(vTime) {
  if (vTime == 1) {
    return `<input type="checkbox" class="form-check-input" checked disabled>`
  } else {
    return `<input type="checkbox" class="form-check-input" disabled>`
  }
}
