/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gUrl = "http://localhost:8080/";

var gRegisterOkData = "";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  //Gán sự kiện khi ấn nút đăng ký
  $("#btn-register").click(function () {
    registerErrorClear(); //Ẩn thông báo lỗi
    onBtnRegisterClick();
  });
});

//Khi ấn đăng nhập ngay
$("#btn-modal-confirm-login").click(function () {
  var vLoginData = {
    username: $("#inp-username").val().trim(),
    password: $("#inp-password").val(),
  };

  if (true) {
    $.ajax({
      url: gUrl + "login",
      type: "POST",
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(vLoginData),
      success: function (responseObject) {
        setCookie("loginSession", JSON.stringify(responseObject), 1);
        window.location.href = "index.html";
      },
      error: function (xhr) {
       toastr.error("Có lỗi xảy ra, vui lòng thử lại");
      },
    });
  }
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onBtnRegisterClick() {
  var vRoleKey = $("#input-role-key").val();
  var vUserData = {
    username: $("#inp-username").val().trim(),
    email: $("#inp-email").val().trim(),
    secretAnswer: $("#inp-secretAnswer").val().trim(),
    password: $("#inp-password").val(),
  };

  //Validate dữ liệu truyền vào
  var vUserIsValide = validateUser(vUserData);
  if (vUserIsValide) {
    $.ajax({
      url: gUrl + "register/"+ vRoleKey,
      type: "POST",
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(vUserData),
      success: function (responseObject) {
        gRegisterOkData = responseObject;
        $("#register-ok-modal").modal();
      },
      error: function (xhr) {
        $("#register-nok-modal").modal();
        (xhr);
        $("#error-message").html(xhr.responseText);
      },
    });
  }
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm kiểm tra dữ liệu user nhập vào
function validateUser(vUserData) {
  var vCheck = true;
  if (vUserData.username == "") {
    $("#error-username").removeClass("d-none");
    vCheck = false;
  }
  if (vUserData.email == "") {
    $("#error-email").removeClass("d-none");
    vCheck = false;
  }
  if (vUserData.secretAnswer == "") {
    $("#error-secretAnswer").removeClass("d-none");
    vCheck = false;
  }

  if ($("#checkbox-agree").prop("checked") == false) {
    $("#error-policy").removeClass("d-none");
    vCheck = false;
  }

  if (vUserData.password.trim().length < 6) {
    $("#error-password")
      .html("Bạn cần nhập mật khẩu tổi thiểu 6 ký tự")
      .removeClass("d-none");
    vCheck = false;
  }

  if (vUserData.password != $("#inp-confirm-password").val()) {
    $("#error-password")
      .html("Mật khẩu nhập lại không khớp")
      .removeClass("d-none");
    vCheck = false;
  }

  return vCheck;
}

//Hàm ẩn các thông báo lỗi
function registerErrorClear() {
  $("#error-username").addClass("d-none");
  $("#error-email").addClass("d-none");
  $("#error-secretAnswer").addClass("d-none");
  $("#error-password").addClass("d-none");
  $("#error-policy").addClass("d-none");
}

//Hàm get Cookie
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

//Hàm setCookie
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
