/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
"use strict"

var gTotalRecords;
var gTotalPages;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onShopGridLoading();

$("#view-record").on("change", function () {
    loadAllShopGridProduct(0);
})

$("#btn-wireless").click(function () {
    loadAllProductByTypeGrid(0,0);
})

$("#btn-earphone").click(function () {
    loadAllProductByTypeGrid(0,1);
})

$("#btn-wired").click(function () {
    loadAllProductByTypeGrid(0,2);
})

$(".btn-brand").click(function () {
    var brandId = $(this).data("mydata");
    (brandId)
    loadAllProductByBrandGrid(0,brandId);
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onShopGridLoading() {
    loadRecentProduct();

    loadAllShopGridProduct(0);
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Gọi dữ liệu trả về 8 sản phẩm mục Hot
function loadRecentProduct() {
    $.ajax({
        url: gBASE_URL + "/products/TopNew",
        method: "GET",
        success: function (pObjRes) {
            (pObjRes);
            loadDataToRecentTab(pObjRes);
        },
        error: function (pXhrObj) {
            toastr.error('Có lỗi xảy ra, vui lòng thử lại sau!')
        }
    });
}

//Hàm đẩy dữ liệu vào mục các sản phẩm mới
function loadDataToRecentTab(paramProductLst) {
    var vDivRecent = $("#div-recent").html("");
    if (paramProductLst.length > 0) {
        paramProductLst.forEach(productElement => {
            $("<div>", { class: "single-post first" }).html(renderRecentCard(productElement)).appendTo(vDivRecent);
        });
    } else {
        $("#div-recent").addClass("text-danger").html("Hiện không có sản phẩm khả dụng");
    }
}

function renderRecentCard(paramData) {
    return `<div class="image">
    <img src="${paramData.photo}" alt="#" width="100%">
</div>
<div class="content">
    <h5><a href="product-details.html?id=${paramData.id}">${paramData.productName}</a></h5>
    <p class="price text-danger font-weight-bold">${formatCurrencyVND(paramData.byPrice)}</p>
    <ul class="reviews text-warning">
    ${renderStar(paramData.star)}
    </ul>
</div>`;
}

//Hàm load danh sách sản phẩm khi tải trang 0
function loadAllShopGridProduct(vPagenum) {
    const vQueryParams = new URLSearchParams({
        "size": $("#view-record").val(),    //mặc định 12 phần tử 1 trang
        "page": vPagenum        //Lấy page đầu tiên
    });

    $.ajax({
        url: gBASE_URL + "/products/grid/" + "?" + vQueryParams.toString(),
        method: "GET",
        success: function (Res) {
            (Res);
            gTotalRecords = Res.total;
            gTotalPages = Math.ceil(gTotalRecords / $("#view-record").val());
            var paramData = Res.data;
            showProductToGrid(paramData);
            createpagination(vPagenum);
        },
        error: function (pXhrObj) {
            toastr.error("Có lỗi xảy ra, vui lòng thử lại");
        }
    });
}

function showProductToGrid(productLst) {
    var vShopGrid = $("#grid-div").html("");
    if (productLst.length > 0) {
        productLst.forEach(Ele => {
            $("<div>", { class: "col-lg-4 col-md-6 col-12" }).html(RenderProductCard(Ele))
                .appendTo(vShopGrid);
        });
    } else {
        $("#grid-div").addClass("text-danger").html("Không có sản phẩm khả dụng");
    }
}

//Hàm tạo thanh phân trang
function createpagination(paramPage) {
    //(gTotalPages);
    var vDivPagination = $("#div-paging").html("");

    if (paramPage == 0) {
        vDivPagination.append(`<button class='page-item disabled previous btn'><a href='javascript:void(0)' > Trang trước </a></button>`);

    } else {
        vDivPagination.append("<button class='page-item btn' onclick='loadAllShopGridProduct(" + (paramPage - 1) + ")'><a href='javascript:void(0)' > Trang trước </a></button>");

    }

    if (paramPage == gTotalPages - 1) { // do page 1 co chi so = 0 nen gTotalPages phai -1
        vDivPagination.append("<button class='page-item disabled previous btn'><a href='javascript:void(0)'>Trang kế</a></button>");

    } else {
        vDivPagination.append("<button class='page-item btn' onclick='loadAllShopGridProduct(" + (paramPage + 1) + ")'><a href='javascript:void(0)'>Trang sau</a></button>");

    }
}

//hàm xử lý sự kiện khi ấn nút wireLess trên categories
function loadAllProductByTypeGrid(vPagenum, vProductType) {
    const vQueryParams = new URLSearchParams({
        "size": $("#view-record").val(),    //mặc định 12 phần tử 1 trang
        "page": vPagenum        //Lấy page đầu tiên
    });

    $.ajax({
        url: gBASE_URL + "/products/type/" + vProductType + "/?" + vQueryParams.toString(),
        method: "GET",
        success: function (Res) {
            (Res);
            gTotalRecords = Res.total;
            gTotalPages = Math.ceil(gTotalRecords / $("#view-record").val());
            var paramData = Res.data;
            showProductToGrid(paramData);
            createpaginationType(vPagenum, vProductType);
        },
        error: function (pXhrObj) {
            toastr.error("Có lỗi xảy ra, vui lòng thử lại");
        }
    });
}

//Hàm tạo thanh phân trang
function createpaginationType(paramPage, vProductType) {
    //(gTotalPages);
    var vDivPagination = $("#div-paging").html("");
    if (paramPage == 0) {
        vDivPagination.append(`<button class='page-item disabled previous btn'><a href='javascript:void(0)' > Trang trước </a></button>`);

    } else {
        vDivPagination.append("<button class='page-item btn' onclick='loadAllShopGridProduct(" + (paramPage - 1) + ","+ vProductType + ")'><a href='javascript:void(0)' > Trang trước </a></button>");

    }

    if (paramPage == gTotalPages - 1) { // do page 1 co chi so = 0 nen gTotalPages phai -1
        vDivPagination.append("<button class='page-item disabled previous btn'><a href='javascript:void(0)'>Trang kế</a></button>");

    } else {
        vDivPagination.append("<button class='page-item btn' onclick='loadAllShopGridProduct(" + (paramPage + 1) + ","+ vProductType + ")'><a href='javascript:void(0)'>Trang sau</a></button>");

    }
}

//hàm xử lý sự kiện khi ấn nút wireLess trên categories
function loadAllProductByBrandGrid(vPagenum, vProductBrandId) {
    const vQueryParams = new URLSearchParams({
        "size": $("#view-record").val(),    //mặc định 12 phần tử 1 trang
        "page": vPagenum        //Lấy page đầu tiên
    });

    $.ajax({
        url: gBASE_URL + "/products/brand/" + vProductBrandId + "/?" + vQueryParams.toString(),
        method: "GET",
        success: function (Res) {
            (Res);
            gTotalRecords = Res.total;
            gTotalPages = Math.ceil(gTotalRecords / $("#view-record").val());
            var paramData = Res.data;
            showProductToGrid(paramData);
            createpaginationBrand(vPagenum, vProductBrandId);
        },
        error: function (pXhrObj) {
            toastr.error("Có lỗi xảy ra, vui lòng thử lại");
        }
    });
}

//Hàm tạo thanh phân trang
function createpaginationBrand(paramPage, vProductBrand) {
    //(gTotalPages);
    var vDivPagination = $("#div-paging").html("");
    if (paramPage == 0) {
        vDivPagination.append(`<button class='page-item disabled previous btn'><a href='javascript:void(0)' > Trang trước </a></button>`);

    } else {
        vDivPagination.append("<button class='page-item btn' onclick='loadAllProductByBrandGrid(" + (paramPage - 1) + ","+ vProductBrand + ")'><a href='javascript:void(0)' > Trang trước </a></button>");

    }

    if (paramPage == gTotalPages - 1) { // do page 1 co chi so = 0 nen gTotalPages phai -1
        vDivPagination.append("<button class='page-item disabled previous btn'><a href='javascript:void(0)'>Trang kế</a></button>");

    } else {
        vDivPagination.append("<button class='page-item btn' onclick='loadAllProductByBrandGrid(" + (paramPage + 1) + ","+ vProductBrand + ")'><a href='javascript:void(0)'>Trang sau</a></button>");

    }
}