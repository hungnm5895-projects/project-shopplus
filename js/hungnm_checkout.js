//RG1
var gIsLogin = getCookie("loginSession");

var gLoginSesstion;
var gtoken;
var gRole;
var gId;
var gCusId;
var gPrice;
//RG2
onCheckOutLoading();

$("#btn-pay").click(function () {

    onBtnPayClick();
});
//RG3
//Hàm xử lý sự kiện khi tải trang
function onCheckOutLoading() {

    //Tải danh sách thành phố
    loadAllProvince();

    //lOad các thông tin thanh toán
    loadCartInfo();

    //Hiển thị thông tin khách hàng
    loadCustomerInfo();
}

//Hàm xử lý sự kiện khi ấn nút thanh toán
function onBtnPayClick() {
    //Nếu đã đăng nhập
    if (gIsLogin != "") {
        orderWithLogin();
    } else {
        ("case 2");
    }
    //Nếu chưa đăng nhập
}
//RG4
//Order đơn hàng nếu đã login
function orderWithLogin() {
    var vOrder = {
        customerId: gCusId,
        price: gPrice,
        orderDetails: getOrderDetails(gCartLst),
        comments: ""
    }

    (JSON.stringify(vOrder));

    if (vOrder.orderDetails != []) {
        $.ajax({
            url: gBASE_URL + "/orders",
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify(vOrder),
            success: function (pObjRes) {
                toastr.success("Đặt hàng và thanh toán thành công.");
                clearCart();
            },
            error: function (pXhrObj) {
                toastr.error("Có lỗi xảy ra, vui lòng kiểm tra lại!");
            },
        });
    } else toastr.error("Bạn cần chọn hàng để mua");
}

function loadCartInfo() {
    var vVoucherCode = (localStorage.getItem("voucherCode"));
    var vVoucherValue = (localStorage.getItem("voucherValue"));
    gPrice = (localStorage.getItem("price"));
    $("#text-price").html(formatCurrencyVND(gPrice));
    $("#text-voucherCode").html((vVoucherCode));
    if (vVoucherCode != null) {
        var bDiscountCash = gPrice * vVoucherValue;
        gPrice = gPrice - bDiscountCash;
        $("#text-voucherValue").html(formatCurrencyVND(bDiscountCash));
        $("#text-price-last").html(formatCurrencyVND(gPrice));
    } else $("#text-price-last").html(formatCurrencyVND(gPrice));

    //$("#text-price").html(formatCurrencyVND(vPrice));
}

function loadCustomerInfo() {
    if (gIsLogin == "") { //Nếu chưa đăng nhập
        ("chưa đăng nhập");
    } else { //nếu đã đăng nhập
        gLoginSesstion = JSON.parse(gIsLogin);
        gtoken = gLoginSesstion.token;
        gRole = gLoginSesstion.authorities;
        gId = gLoginSesstion.id;
        gCusId = gLoginSesstion.customerId;
        (gLoginSesstion);


        $.ajax({
            url: gBASE_URL + "/customers/" + gCusId,
            method: "GET",
            async: false,
            success: function (pObjRes) {
                (pObjRes);
                showCusInfo(pObjRes);
            },
            error: function (pXhrObj) {
                toastr.error("Có lỗi xảy ra, vui lòng kiểm tra lại!");
            },
        });
    }
}

//Hàm gọi API lấy danh sách thành phố
function loadAllProvince() {
    $.ajax({
        url: gBASE_URL + "/provinces",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            loadDataToProvinceSelect(pObjRes); //Đầy dữ liệu nhận về vào select Filter
        },
        error: function (pXhrObj) {
            //(pXhrObj);
        }
    });
}

//hàm hiển thị dữ liệu thành phố
//Input: Data các thành phố
//Output: Dữ liệu được đẩy vào Obj Fiter
function loadDataToProvinceSelect(paramProvinceData) {
    var vProvinceSelectElement = $("#inp-province").html("");
    for (i = 0; i < paramProvinceData.length; i++) {
        var bProvinceOption = $("<option/>");
        bProvinceOption.prop("value", paramProvinceData[i].id);
        bProvinceOption.prop("text", paramProvinceData[i].name);
        vProvinceSelectElement.append(bProvinceOption);
    };
}

//Hiển thị dữ liệu khách hàng
function showCusInfo(cusData) {
    $("#inp-name").val(cusData.name).prop("disabled", true);
    $("#inp-phone").val(cusData.phone).prop("disabled", true);
    $("#inp-email").val(cusData.email).prop("disabled", true);
    $("#inp-address").val(cusData.address).prop("disabled", true);
    $("#inp-province").val(cusData.provinceId).prop("disabled", true);
    $("#check-create-user").addClass("d-none");

}

//Hàm trả về orderdetail
function getOrderDetails(paramCart) {
    var vOrderDetails = [];
    paramCart.forEach(element => {
        $.ajax({
            url: gBASE_URL + "/products/" + element.id,
            method: "GET",
            async: false,
            success: function (pObjRes) {
                var bOrderDetai = {
                    quantityOrder: element.qty,
                    priceEach: pObjRes.byPrice,
                    productId: pObjRes.id
                }

                vOrderDetails.push(bOrderDetai);
            },
            error: function (pXhrObj) {
                toastr.error("Có lỗi xảy ra, vui lòng kiểm tra lại!");
            },
        });
    });

    return vOrderDetails;
}