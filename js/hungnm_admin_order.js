/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */


var gStt = 0;
var gOrderId = 0;

//Các cột của bảng
const gORDER_COLS = [
  "id",
  "id",
  "status",
  "customerId",
  "orderDate",
  "requiredDate",
  "shippedDate",
  "price"
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gORDER_COLS_ACTION_COL = 0;
const gORDER_COLS_ID_COL = 1;
const gORDER_COLS_STATUS_COL = 2;
const gORDER_COLS_CUS_COL = 3;
const gORDER_COLS_ORDERDATE_COL = 4;
const gORDER_COLS_REQUIREDATE_COL = 5;
const gORDER_COLS_SHIPPED_COL = 6;
const gORDER_COLS_PRICE_COL = 7;


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

var gOrderTable = $("#order-table")
  .DataTable({
    responsive: true,
    lengthChange: false,
    autoWidth: false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    columns: [
      { data: gORDER_COLS[gORDER_COLS_ACTION_COL] },
      { data: gORDER_COLS[gORDER_COLS_ID_COL] },
      { data: gORDER_COLS[gORDER_COLS_STATUS_COL] },
      { data: gORDER_COLS[gORDER_COLS_CUS_COL] },
      { data: gORDER_COLS[gORDER_COLS_ORDERDATE_COL] },
      { data: gORDER_COLS[gORDER_COLS_REQUIREDATE_COL] },
      { data: gORDER_COLS[gORDER_COLS_SHIPPED_COL] },
      { data: gORDER_COLS[gORDER_COLS_PRICE_COL] }
    ],
    columnDefs: [
      {
        target: gORDER_COLS_ACTION_COL,
        render: renderOrderAction,
      },
      {
        target: gORDER_COLS_PRICE_COL,
        render: formatCurrencyVND,
      },
      {
        target: gORDER_COLS_ORDERDATE_COL,
        render: renderFullDateTime,
      }
      ,
      {
        target: gORDER_COLS_STATUS_COL,
        render: renderOrderStatus,
      }
      ,
      {
        target: gORDER_COLS_CUS_COL,
        render: renderCustomerName,
      }
      
    ],
  })

//OnpageLoading

$("#btn-create-order").click(function () {
  clearData();
  $("#add-order-modal").modal();
});

//Gán sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
$("#btn-modal-add-order").click(function () {
  onBtnAddOrderModalClick();
});

//Gán sự kiện khi ấn vào dòng
//Gán sự kiện khi ấn vào dòng trên table
$("#country-table").on("click", ".btn-edit", function () {
  onCountryRowClick(this);
});

//Gán sự kiện khi ấn nút cập nhật thông tin trên modal info
$("#btn-modal-edit-country").click(function () {
  onBtnUpdateCountryClick();
});

$("#order-table").on("click", ".btn-delete", function () {
  onDeleteOrderClick(this);
});


$("#btn-modal-confirm-delete-order").click(function () {
  onBtnDeleteOrderConfirmClick();
});

$("#order-table").on("click", ".btn-cancel", function () {
  onCancelOrderClick(this);
});

$("#order-table").on("click", ".btn-required", function () {
  onRequiredOrderClick(this);
});

$("#order-table").on("click", ".btn-shipped", function () {
  onShippedOrderClick(this);
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Lấy về danh sách thành phố
//Tải toàn bộ khách hàng về
loadAllOrders();

loadAllCustomer();

loadAllProduct();

//Hàm xử lý sự kiện khi ấn icon hủy đơn hàng
function onRequiredOrderClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gOrderTable.row(vTableRow).data();
  gOrderId = vRowData.id;
  $.ajax({
    url: gBASE_URL + "/orders/required/" + gOrderId,
    type: "PUT",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Đơn hàng đang được vận chuyển");
      loadAllOrders();
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
      (ajaxContext.responseText);
    },
  });
}

//Hàm xử lý sự kiện khi ấn icon hủy đơn hàng
function onShippedOrderClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gOrderTable.row(vTableRow).data();
  gOrderId = vRowData.id;
  $.ajax({
    url: gBASE_URL + "/orders/shipped/" + gOrderId,
    type: "PUT",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Đơn hàng đã xử lý hàng thành công");
      loadAllOrders();
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
      (ajaxContext.responseText);
    },
  });
}

//Hàm xử lý sự kiện khi ấn icon hủy đơn hàng
function onCancelOrderClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gOrderTable.row(vTableRow).data();
  gOrderId = vRowData.id;
  $.ajax({
    url: gBASE_URL + "/orders/cancel/" + gOrderId,
    type: "PUT",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Hủy đơn hàng thành công");
      loadAllOrders();
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
      (ajaxContext.responseText);
    },
  });
}

//hàm xử lý sự kiện khi ấn icon thùng rác
function onDeleteOrderClick(elementRow) {
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gOrderTable.row(vTableRow).data();
  gCountryId = vRowData.id;
  $("#delete-order-modal").modal();
}

//Hàm xử lý sự kiện khi ấn vào icon kính lúp
function onCountryRowClick(elementRow) {
  $("#info-country-modal").modal();
  var vTableRow = $(elementRow).parents("tr");
  var vRowData = gOrderTable.row(vTableRow).data();
  gCountryId = vRowData.id;
  //(gVoucherId);
  showCountryInfoToForm(vRowData);
}
//Xử lý sự kiện khi ấn nút thêm khách hàng trên modal thêm mới
function onBtnAddOrderModalClick() {
  var vObj = {
    customerId: $("#modal-add-customerId").val(),
    comments: $("#modal-add-comments").val().trim(),
    price: 0.0,
    orderDetails: getProductDetailLst()

  };
  (vObj);
  if (validateOrder(vObj)) {
    $.ajax({
      url: gBASE_URL + "/orders/admin",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Thêm mới đơn hàng thành công");
        loadAllOrders();
        $("#add-order-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút cập nhật thông tin
function onBtnUpdateCountryClick() {
  var vObj = {
    voucherCode: $("#modal-info-countryCode").val().trim(),
    voucherName: $("#modal-info-countryName").val().trim(),
  };

  if (validateOrder(vObj)) {
    $.ajax({
      url: gBASE_URL + "/countries/" + gCountryId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(vObj),
      success: function (Res) {
        toastr.success("Cập nhật Quốc gia thành công");
        loadAllOrders();
        $("#info-country-modal").modal("hide");
      },
      error: function (ajaxContext) {
        toastr.error(ajaxContext.responseText);
      },
    });
  }
}

//Hàm xử lý sự kiện khi ấn nút xóa khách hàng
function onBtnDeleteOrderConfirmClick() {
  $.ajax({
    url: gBASE_URL + "/orders/" + gCountryId,
    type: "DELETE",
    contentType: "application/json",
    success: function (Res) {
      toastr.success("Xóa đơn hàng thành công");
      loadAllOrders();
      $("#delete-order-modal").modal("hide");
    },
    error: function (ajaxContext) {
      toastr.error(ajaxContext.responseText);
    },
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function loadAllOrders() {
  $.ajax({
    url: gBASE_URL + "/orders",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showOrdersLstToTable(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      //showOrdersLstToTable(gCountryList);
    },
  });
}

//Hàm hiển thị dữ liệu trả về lên table
function showOrdersLstToTable(data) {
  gStt = 0;
  gOrderTable.clear();
  gOrderTable.rows.add(data);
  gOrderTable.draw();
}

//Hàm kiểm tra dữ liệu Khách hàng
function validateOrder(paramObj) {
  if (paramObj.customerId < 1) {
    toastr.warning("Bạn cần chọn khách hàng");
    return false;
  }

  return true;
}

//Xóa dữ liệu modal thêm mới
function clearData() {
  $("#modal-add-customerId").val("");
  $("#modal-add-comments").val("");
  $("#modal-add-product").val("");
}

//Hàm hiển thị dữ liệu khách hàng qua form
function showCountryInfoToForm(paramData) {
  $("#modal-info-id").val(paramData.id);
  $("#modal-info-countryName").val(paramData.countryName);
  $("#modal-info-countryCode").val(paramData.countryCode);
}


//Hàm render nút action
function renderOrderAction(params) {
  return `<i class="ti-search btn-edit mr-2 text-info" style="cursor: pointer"></i>
  <i class="ti-shopping-cart-full text-info btn-delete mr-2" style="cursor: pointer"></i>
  <i class="ti-trash text-danger btn-delete mr-2" style="cursor: pointer"></i>
  <i class="ti-truck text-info btn-required mr-2" style="cursor: pointer"></i>
  <i class="ti-check text-success btn-shipped" style="cursor: pointer"></i>
  <i class="ti-close text-warning btn-cancel" style="cursor: pointer"></i>
  `;
}

//Hàm load toàn bộ khách hàng
function loadAllCustomer() {
  $.ajax({
    url: gBASE_URL + "/customers",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showCustomerToSelect(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      //showCustomerToTable(gCustomerList);
    },
  });
}

function showCustomerToSelect(data) {
  var vCustomerSelectElement = $("#modal-add-customerId");
  for (i = 0; i < data.length; i++) {
    let bCusOption = $("<option/>");
    bCusOption.prop("value", data[i].id);
    bCusOption.prop("text", data[i].name);
    vCustomerSelectElement.append(bCusOption);
  };
}

//Hàm load toàn bộ khách hàng
function loadAllProduct() {
  $.ajax({
    url: gBASE_URL + "/products",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      showProductToSelect(pObjRes);
    },
    error: function (pXhrObj) {
      toastr.error(pXhrObj);
      //showCustomerToTable(gCustomerList);
    },
  });
}

function showProductToSelect(data) {
  var vCustomerSelectElement = $("#modal-add-product");
  for (i = 0; i < data.length; i++) {
    let bCusOption = $("<option/>");
    bCusOption.prop("value", data[i].id);
    bCusOption.prop("text", data[i].productName);
    vCustomerSelectElement.append(bCusOption);
  };
}

function getProductDetailLst() {
  var vProductVal = $("#modal-add-product").val();
  (vProductVal);
  var vProductDetal = [];
  vProductVal.forEach(element => {
    vProductDetal.push({ productId: element, quantityOrder: 1 })
  });

  return vProductDetal;
}
