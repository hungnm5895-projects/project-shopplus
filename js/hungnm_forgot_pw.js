/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gUrl = "http://localhost:8080";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    //Gán sự kiện khi ấn nút quên mật khẩu
    $("#btn-reset-password").click(function () {
        onBtnResetPasswordClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm xử lý ự kiện khi ấn reset mật khẩu
function onBtnResetPasswordClick() {
    var vUserData = {
        username: $("#inp-username").val().trim(),
        email: $("#inp-email").val().trim(),
        secretAnswer: $("#inp-secretAnswer").val().trim(),
    };

    if (validateUserData(vUserData)) {
        $.ajax({
            url: gUrl + "/resetPassword",
            type: "PUT",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vUserData),
            success: function (responseObject) {
                //$("#new-pw").html(responseObject);
                $("#new-password-modal").modal();
            },
            error: function (xhr) {
                toastr.error(xhr.responseText);
            },
        });
    }
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm kiểm tra dữ liệu đầu vào
function validateUserData(paramObj) {
    if (paramObj.username == "") {
        toastr.warning("Cần nhập  tài khoản");
        return false;
    }

    if (paramObj.email == "") {
        toastr.warning("Cần nhập email");
        return false;
    }

    if (!validateEmail(paramObj.email)) {
        toastr.warning("Cần nhập email hợp lệ");
        return false;
    }

    if (paramObj.secretAnswer == "") {
        toastr.warning("Cần nhập câu trả lời bí mật");
        return false;
    }

    return true;
}