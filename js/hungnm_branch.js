var gLat = 21.05297860880881;
var gLng = 105.86699552563336;

function initMap() {
  var map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: gLat, lng: gLng },
    zoom: 15
  });

  var latLng =  { lat: gLat, lng: gLng }

  // create map with center is latLng
  // code

  // each marker define one point
  var marker = new google.maps.Marker({
    position: latLng,
    map: map,
  });

}

var gBranchLst = [];

//RG2
//onpageLoading

//Gán sự kiện khi thay đổi giá trị Tỉnh/Thành phố
$("#inp-province").on("change", function () {
  $("#inp-branch").html("");
  var vProvinceId = $(this).val();
  onSelectProvinceChange(vProvinceId);
});

$("#div-branch").on("click",".btn-road", function () {
  var vId = $(this).data("mydata");
  onBtnFindRoad(vId);
})

//RG3

loadAllProvinces();


//RG4
function loadAllProvinces() {
  $.ajax({
    url: "http://localhost:8080/provinces",
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      loadDataToProvinceSelect(pObjRes); //Đầy dữ liệu nhận về vào select Filter
    },
    error: function (pXhrObj) {
      //(pXhrObj);
    }
  });
}

//hàm hiển thị dữ liệu thành phố
//Input: Data các thành phố
//Output: Dữ liệu được đẩy vào Obj Fiter
function loadDataToProvinceSelect(paramProvinceData) {
  var vProvinceSelectElement = $("#inp-province").css("display", "block");
  for (i = 0; i < paramProvinceData.length; i++) {
    var bProvinceOption = $("<option/>");
    bProvinceOption.prop("value", paramProvinceData[i].id);
    bProvinceOption.prop("text", paramProvinceData[i].name);
    vProvinceSelectElement.append(bProvinceOption);
  };
}

//Hàm xử lý sự kiện khi thay đổi thành phố
function onSelectProvinceChange(paramProvinceId) {
  $.ajax({
    url: "http://localhost:8080/" + "offices/province/" + paramProvinceId,
    method: "GET",
    success: function (pObjRes) {
      (pObjRes);
      gBranchLst = pObjRes;
      loadDataToBranchSelect(pObjRes);
    },
    error: function (pXhrObj) {
      //(pXhrObj);
    }
  });
}

//hàm đổ dữ liệu vào select quận huyện
function loadDataToBranchSelect(paramDistrictData) {
  var vdivB = $("#div-branch").html("");
  for (i = 0; i < paramDistrictData.length; i++) {
    $("<div/>", { class: "card mt-3", style: "width: 100%" }).html(`<h5 class="card-header">${paramDistrictData[i].name}</h5>
    <div class="card-body">
      <p class="card-text mb-2">${paramDistrictData[i].address}.</p>
      <a class="btn btn-primary text-white btn-road" data-mydata="${paramDistrictData[i].id}">Chỉ đường</a>
    </div>`).appendTo(vdivB);
  };
}

function onBtnFindRoad(paramBrandId) {
  for (let index = 0; index < gBranchLst.length; index++) {
    if (paramBrandId == gBranchLst[index].id) {
      gLat = parseFloat(gBranchLst[index].lat);
      gLng = parseFloat(gBranchLst[index].lng);
      initMap();
    }
    
  }
}