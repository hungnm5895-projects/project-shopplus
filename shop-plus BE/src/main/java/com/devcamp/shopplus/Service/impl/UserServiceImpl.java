package com.devcamp.shopplus.Service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.*;

import com.devcamp.shopplus.Respository.*;
import com.devcamp.shopplus.Sercurity.*;
import com.devcamp.shopplus.Service.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    RoleRespository roleRespository;

    @Autowired
    CustomerService customerService;

    @Autowired
    SendMailService sendMailService;

    @Autowired
    NotificationService notificationService;
    
    @Override
    public User createUser(User user, String keyRole) {
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        newUser.setEmail(user.getEmail());
        newUser.setSecretAnswer(user.getSecretAnswer());
        newUser.setCreatedAt(new Date());
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        Set<Role> newRoles = new HashSet<Role>();
        newRoles.add(roleRespository.findByRoleKey(keyRole));
        newUser.setRoles(newRoles);

        newUser = userRepository.saveAndFlush(newUser);
        if (keyRole.equals("ROLE_CUSTOMER")) {
            CCustomer newCus = new CCustomer();
            newCus.setEmail(user.getEmail());
            newCus.setName("test");
            newCus.setPhone("00000000");
            newCus.setProvinceId(1);
            newCus.setAddress("");
            newCus.setUserId(newUser.getId());
            newCus = customerService.createCustomerService(newCus);
        }
        return newUser;
    }

    @Override
    public User createUserIndex(User user, String keyRole) {
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        newUser.setEmail(user.getEmail());
        newUser.setSecretAnswer(user.getSecretAnswer());
        newUser.setCreatedAt(new Date());
        Set<Role> newRoles = new HashSet<Role>();
        newRoles.add(roleRespository.findByRoleKey(keyRole));
        newUser.setRoles(newRoles);
        return userRepository.saveAndFlush(newUser);
    }

    @Override
    public UserPrincipal findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        UserPrincipal userPrincipal = new UserPrincipal();
        if (null != user) {
            Set<String> authorities = new HashSet<>();
            if (null != user.getRoles())
                user.getRoles().forEach(r -> {
                    authorities.add(r.getRoleKey());
                    r.getPermissions().forEach(p -> authorities.add(p.getPermissionKey()));
                });

            userPrincipal.setUserId(user.getId());
            userPrincipal.setUsername(user.getUsername());
            userPrincipal.setPassword(user.getPassword());
            userPrincipal.setAuthorities(authorities);
        }
        return userPrincipal;
    }

    @Override
    public List<User> getAllUserService() {
        return userRepository.findAll();
    }

    @Override
    public User lockUser(long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            User savUser = userOptional.get();
            savUser.setDeleted(true);
            return userRepository.saveAndFlush(savUser);
        } else {
            return null;
        }
    }

    @Override
    public User updateUser(User user, long id, String roleKey) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            User savUser = userOptional.get();
            savUser.setEmail(user.getEmail());
            savUser.setSecretAnswer(user.getSecretAnswer());
            savUser.setUpdatedAt(new Date());
            Set<Role> newRoles = new HashSet<Role>();
            newRoles.add(roleRespository.findByRoleKey(roleKey));
            savUser.setRoles(newRoles);
            return userRepository.saveAndFlush(savUser);
        } else {
            return null;
        }
    }
}
