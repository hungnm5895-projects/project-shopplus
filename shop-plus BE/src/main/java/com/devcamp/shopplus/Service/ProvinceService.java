package com.devcamp.shopplus.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CProvince;
import com.devcamp.shopplus.Respository.ProvinceRepository;
/**
 * Service xử lý nghiệp vụ liên quan đến Tỉnh/Thành phố
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class ProvinceService {
    @Autowired
    ProvinceRepository provinceRepository;

    public List<CProvince> getAllProvinceService() {
        List<CProvince> lstProvinces = new ArrayList<>();
        provinceRepository.findAll().forEach(lstProvinces::add);
        return lstProvinces;
    }

    public Optional<CProvince> getProvinceByIdService(int id) {
        return provinceRepository.findById(id);
    }
}
