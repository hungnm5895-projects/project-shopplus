package com.devcamp.shopplus.Service;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CCustomer;
import com.devcamp.shopplus.Entity.CProduct;
import com.devcamp.shopplus.Respository.ProductRespository;

/**
 * Service xử lý nghiệp vụ liên quan đến sản phẩm
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class ProductService {
    @Autowired
    ProductRespository productRespository;

    /**
     * Service truy vấn toàn bộ sản phẩm
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ sản phẩm
     */
    public List<CProduct> getAllProductService() {
        return productRespository.findAll();
    }

    /**
     * Service truy vấn toàn bộ sản phẩm phân trang
     * 
     * @param : size
     * @param : page
     * 
     * @return Danh sách toàn bộ sản phẩm phân trang
     */
    public List<CProduct> getAllProductPagingService(String page, String size) {
        Pageable pageElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        return productRespository.findAllProductQuery(pageElements);
    }

    /**
     * Service truy vấn sản phẩm theo id, mỗi 1 lần truy vấn lượng view tăng thêm 1
     * 
     * @param : id của sản phẩm
     * 
     * @return sản phẩm có id tương ứng
     */
    public CProduct getProductByIdService(Long id) {
        Optional<CProduct> optProduct = productRespository.findById(id);
        if (optProduct.isPresent()) {
            CProduct sProduct = optProduct.get();
            sProduct.viewProduct();
            sProduct = productRespository.save(sProduct);
            return sProduct;
        } else {
            return null;
        }
    }

    /**
     * Service tạo mới sản phẩm
     * 
     * @param : obj sản phẩm
     * 
     * @return sản phẩm đã được tạo mới
     */
    public CProduct createProductService(CProduct cProduct) {

        Optional<CProduct> prodOptional = productRespository.findByProductCode(cProduct.getProductCode());
        if (!prodOptional.isPresent()) {
            CProduct nProduct = new CProduct();
            nProduct.setProductCode(cProduct.getProductCode());
            nProduct.setProductName(cProduct.getProductName());
            nProduct.setProductDescription(cProduct.getProductDescription());
            nProduct.setProductType(cProduct.getProductType());
            nProduct.setProductLine(cProduct.getProductLine());
            nProduct.setProductScale(cProduct.getProductScale());
            nProduct.setBrandId(cProduct.getBrandId());
            nProduct.setQuatityInStock(cProduct.getQuatityInStock());
            nProduct.setByPrice(cProduct.getByPrice());
            nProduct.setDiscount(cProduct.getDiscount());
            nProduct.setIsHot(cProduct.getIsHot());
            nProduct.setView(cProduct.getView());
            nProduct.setRateSum(0);
            nProduct.setRateCount(0);
            nProduct.setPhoto(cProduct.getPhoto());
            nProduct.setBoughtSum(0);
            nProduct.setAudioTechnology(cProduct.getAudioTechnology());
            nProduct.setBoxTime(cProduct.getBoxTime());
            nProduct.setCompatible(cProduct.getCompatible());
            nProduct.setConnectSameTime(cProduct.getConnectSameTime());
            nProduct.setConnectTechnology(cProduct.getConnectTechnology());
            nProduct.setGateCharge(cProduct.getGateCharge());
            nProduct.setHighZ(cProduct.getHighZ());
            nProduct.setLongX(cProduct.getLongX());
            nProduct.setMadeIn(cProduct.getMadeIn());
            nProduct.setPinTime(cProduct.getPinTime());
            nProduct.setWidthY(cProduct.getWidthY());
            nProduct.setIsTrend(cProduct.getIsTrend());
            nProduct.setSaleDate(new Date());

            nProduct = productRespository.save(nProduct);
            return nProduct;
        } else {
            return null;
        }
    }

    /**
     * Service cập nhật sản phẩm
     * 
     * @param : obj sản phẩm và id tương ứng
     * 
     * @return sản phẩm đã được câp nhật
     */
    public CProduct updateProductService(Long productId, CProduct cProduct) {
        Optional<CProduct> prodOptional = productRespository.findById(productId);
        if (prodOptional.isPresent()) {
            CProduct nProduct = prodOptional.get();

            nProduct.setProductName(cProduct.getProductName());
            nProduct.setProductDescription(cProduct.getProductDescription());
            nProduct.setProductType(cProduct.getProductType());
            nProduct.setProductLine(cProduct.getProductLine());
            nProduct.setProductScale(cProduct.getProductScale());
            nProduct.setBrandId(cProduct.getBrandId());
            nProduct.setQuatityInStock(cProduct.getQuatityInStock());
            nProduct.setByPrice(cProduct.getByPrice());
            nProduct.setDiscount(cProduct.getDiscount());
            nProduct.setIsHot(cProduct.getIsHot());
            // nProduct.setView(cProduct.getView());
            // nProduct.setRateSum(0);
            // nProduct.setRateCount(0);
            nProduct.setPhoto(cProduct.getPhoto());
            // nProduct.setBoughtSum(0);
            nProduct.setAudioTechnology(cProduct.getAudioTechnology());
            nProduct.setBoxTime(cProduct.getBoxTime());
            nProduct.setCompatible(cProduct.getCompatible());
            nProduct.setConnectSameTime(cProduct.getConnectSameTime());
            nProduct.setConnectTechnology(cProduct.getConnectTechnology());
            nProduct.setGateCharge(cProduct.getGateCharge());
            nProduct.setHighZ(cProduct.getHighZ());
            nProduct.setLongX(cProduct.getLongX());
            nProduct.setMadeIn(cProduct.getMadeIn());
            nProduct.setPinTime(cProduct.getPinTime());
            nProduct.setWidthY(cProduct.getWidthY());
            nProduct.setIsTrend(cProduct.getIsTrend());
            // nProduct.setSaleDate(new Date());
            nProduct = productRespository.save(nProduct);
            return nProduct;
        } else {
            return null;
        }
    }

    /**
     * Service xóa sản phẩm
     * 
     * @param : id sản phẩm cần xóa
     * 
     * @return : void
     * 
     */
    public void deleteProductService(long id) {
        productRespository.deleteById(id);
    }

    /**
     * Service truy vấn sản phẩm Xu hướng trên LPD, hiển thị sản phẩm theo type và
     * số bản ghi truyền vào, chỉ hiển thị các sản phẩm còn hàng và thứ tự từ gần
     * đến xa
     * 
     * @param : int type và int size - số lượng bản ghi
     * 
     * @return danh sách sản phẩm tương ứng
     */
    public List<CProduct> getProductLdpService(int type, int size) {
        return productRespository.findProductByTypeAndSizeQuery(type, size);
    }

    /**
     * Service truy vấn sản phẩm HOT trên LPD, hiển thị sản phẩm có trạng thái HOT,
     * chỉ hiển thị các sản phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param : int size - số lượng bản ghi
     * 
     * @return danh sách sản phẩm tương ứng
     */
    public List<CProduct> getProductHotLdpService(int size) {
        return productRespository.findProductHotWithSizeQuery(size);
    }

    /**
     * Service truy vấn sản phẩm sale trên LPD, hiển thị top 5 sản phẩm có discount
     * giảm dần
     * chỉ hiển thị các sản phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param :
     * 
     * @return danh sách sản phẩm tương ứng
     */
    public List<CProduct> getProductSaleLdpService() {
        return productRespository.findProductSaleQuery();
    }

    /**
     * Service truy vấn sản phẩm BestSeller trên LPD, hiển thị top 5 sản phẩm có số
     * lượng bán giảm dần
     * chỉ hiển thị các sản phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param :
     * 
     * @return danh sách sản phẩm tương ứng
     */
    public List<CProduct> getProductBestSallerLdpService() {
        return productRespository.findProductBestSellerQuery();
    }

    /**
     * Service truy vấn sản phẩm Top view trên LPD, hiển thị top 5 sản phẩm có lượng
     * xem giảm dần
     * chỉ hiển thị các sản phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param :
     * 
     * @return danh sách sản phẩm tương ứng
     */
    public List<CProduct> getProductTopViewLdpService() {
        return productRespository.findProductTopViewQuery();
    }

    /**
     * Service truy vấn sản phẩm Mới bán trên shop grid, hiển thị top 5 sản phẩm
     * thời gian đăng giảm dần chỉ hiển thị các sản phẩm còn hàng
     * 
     * @param :
     * 
     * @return danh sách sản phẩm tương ứng
     */
    public List<CProduct> getProductNewService() {
        return productRespository.findProductNewQuery();
    }

    /**
     * Service thực hiện vote sao sản phẩm. Truy vấn sản phẩm và thực hiện gọi
     * method voteStar() của đối tượng
     * 
     * @param : id của sản phẩm
     * @param : số sao vote tương ứng
     * 
     * @return void
     */
    public CProduct voteStarService(long productId, int star) {
        Optional<CProduct> prodOptional = productRespository.findById(productId);
        if (prodOptional.isPresent()) {
            CProduct nProduct = prodOptional.get();
            nProduct.voteStar(star);
            nProduct = productRespository.save(nProduct);
            return nProduct;
        } else
            return null;
    }

    /**
     * Service truy vấn sản phẩm Mới bán trên shop grid theo product type
     * 
     * @param : product_type
     * @param : size, page
     * 
     * @return danh sách sản phẩm tương ứng
     */
    public List<CProduct> getProductLstByTypeService(int type, String page, String size) {
        Pageable pageElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        return productRespository.findAllProductByTypeQuery(type, pageElements);
    }

    /**
     * Service truy vấn sản phẩm Mới bán trên shop grid theo product type
     * 
     * @param : product_brand
     * @param : size, page
     * 
     * @return danh sách sản phẩm tương ứng
     */
    public List<CProduct> getProductLstByBrandService(int brandId, String page, String size) {
        Pageable pageElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        return productRespository.findAllProductByBrandQuery(brandId, pageElements);
    }
    
    /**
     * Service truy vấn số lượng sản phẩm bán hôm nay
     * 
     * @param : 
     * 
     * @return Danh sách sản phẩm bán hôm hôm nay
     */
    public List<CProduct> getProductSaleTodayService(){
        return productRespository.findProductSaleTodayQuery();
    }

    /**
     * Service truy vấn số lượng sản phẩm sắp hết hàng, truy vấn số sản phẩm có quantity_in_stock < 50
     * 
     * @param : 
     * 
     * @return Danh sách sản phẩm bán hôm hôm nay
     */
    public List<CProduct> getProductOutStockService(){
        return productRespository.findProductOutStockQuery();
    }

    /**
     * Service truy vấn số lượng lượt xem sản phẩm
     * 
     * @param : 
     * 
     * @return số lượng lượt xem sản phẩm
     */
    public BigInteger getTotalViewService(){
        return productRespository.findTotalViewQuery();
    }
}
