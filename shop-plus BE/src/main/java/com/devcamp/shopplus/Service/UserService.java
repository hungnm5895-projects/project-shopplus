package com.devcamp.shopplus.Service;

import java.util.List;

import com.devcamp.shopplus.Entity.User;
import com.devcamp.shopplus.Sercurity.UserPrincipal;

public interface UserService {
    com.devcamp.shopplus.Entity.User createUser(User user, String keyRole);

    UserPrincipal findByUsername(String username);

    User createUserIndex(User user, String keyRole);

    //Lấy danh sách user
    List<User> getAllUserService();

    User lockUser(long id);

    User updateUser(User user, long id, String roleKey); 
}
