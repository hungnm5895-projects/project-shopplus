package com.devcamp.shopplus.Service;

import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class SendMailService {
    private String host = "smtp.gmail.com";
    private int port = 587;
    private String username = "hungnm5895@gmail.com";
    private String password = "gudbkpbfnbbotfgs";

    public void sendEmail(String emailTo, String subject, String message) {
        try {

            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setHost(host);
            mailSender.setPort(port);
            mailSender.setUsername(username);
            mailSender.setPassword(password);
            mailSender.setDefaultEncoding("UTF-8");

            // Set the mail properties
            Properties props = mailSender.getJavaMailProperties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");

            MimeMessage mess = mailSender.createMimeMessage();
            MimeMessageHelper helper;
            helper = new MimeMessageHelper(mess, "UTF-8");
            helper.setFrom(username);
            helper.setTo(emailTo);
            helper.setText(message, true);
            mess.setSubject(subject);

            mailSender.send(mess);

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void createAndSendEmailVerify(long id,String email,String verifyCode){
        String subject="Xác thực tài khoản sử dụng tại Bất động sản Hoàng Gia";
        
        String linkRef="http://127.0.0.1:5500/login.html?id="+id+"&code="+verifyCode;

        String mess="Vui lòng click vào nút \"Xác thực\" phía dưới để kích hoạt tài khoản <br><br>"
        +"<a href=\""+linkRef+"\" target=\"_blank\">\r\n" 
        +"<button style=\"width: 100px; height: 40px; font-size: 20px;  background-color:#89CFF3; border-radius: 5px;\">Xác thực</button>\r\n" + //
                "        </a>";
        sendEmail(email,subject,mess);
    }

    public void sendEmailResetPassword(String email, String newPassWord){
        String subject="Reset Mat khau ShopPlus";

        String linkRef="http://127.0.0.1:5500/changePassword.html";
        
        String mess="Mật khẩu mới của bạn là " + newPassWord +" . Vui lòng click vào nút \"Đến trang đổi mật khẩu\" phía dưới để thực hiện việc đổi mật khẩu <br><br>"
        +"<a href=\""+linkRef+"\" target=\"_blank\">\r\n" 
        +"<button style=\"padding-left:10px;padding-right:10px; height: 40px; font-size: 20px;  background-color:#89CFF3; border-radius: 5px;\">Đến trang đổi mật khẩu</button>\r\n" + //
                "        </a>";
        sendEmail(email,subject,mess);
    }
}
