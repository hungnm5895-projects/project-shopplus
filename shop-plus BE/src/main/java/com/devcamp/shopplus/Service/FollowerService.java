package com.devcamp.shopplus.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CFollower;
import com.devcamp.shopplus.Respository.FollowerRepository;

@Service
public class FollowerService {
    @Autowired
    FollowerRepository followerRepository;

    /**
     * Service truy vấn toàn bộ người theo dõi
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ người theo dõi
     */
    public List<CFollower> getAllFollowersService() {
        return followerRepository.findAll();
    }

    /**
     * Service tạo mới người theo dõi
     * 
     * @param : obj người theo dõi
     * 
     * @return người theo dõi đã được tạo mới
     */
    public CFollower createFollowerService(CFollower cFollower) {

        CFollower cFollowe = new CFollower();
        cFollowe.setEmail(cFollower.getEmail());
        cFollowe.setCreateDate(new Date());

        cFollowe = followerRepository.save(cFollowe);
        return cFollowe;
    }

    /**
     * Service cập nhật người theo dõi
     * 
     * @param : obj người theo dõi
     * 
     * @return người theo dõi đã được cập nhật
     */
    public CFollower updateFollowerService(long followersId, CFollower cFollower) {
        Optional<CFollower> fOptional = followerRepository.findById(followersId);
        if (fOptional.isPresent()) {
            CFollower CFollowe = fOptional.get();
            CFollowe.setEmail(cFollower.getEmail());
            CFollowe = followerRepository.save(CFollowe);
            return CFollowe;
        } else {
            return null;
        }
    }

    /**
     * Service xóa người theo dõi
     * 
     * @param : id của noti
     * 
     * @return người theo dõi đã được xóa
     */
    public void deletePaymentService(long id) {
        followerRepository.deleteById(id);
    }
}
