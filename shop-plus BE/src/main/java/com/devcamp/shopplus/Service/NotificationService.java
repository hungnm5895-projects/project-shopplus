package com.devcamp.shopplus.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CNotication;
import com.devcamp.shopplus.Model.MNotificationRequest;
import com.devcamp.shopplus.Respository.NotificationRepository;

/**
 * Service xử lý nghiệp vụ liên quan đến push noti cho khách hàng
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class NotificationService {
    @Autowired
    NotificationRepository notificationRepository;

    /**
     * Service truy vấn toàn bộ noti
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ noti
     */
    public List<CNotication> getAllNotiService() {
        return notificationRepository.findAll();
    }

    /**
     * Service truy vấn toàn bộ noti của 1 khách hàng
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ noti
     */
    public List<CNotication> getAllNotiServiceOfCustomer(Long customerId) {
        return notificationRepository.findByCustomerIdQuery(customerId);
    }

    /**
     * Service tạo mới noti
     * 
     * @param : obj noti
     * 
     * @return noti đã được tạo mới
     */
    public CNotication createNotiService(CNotication cNotication) {
        CNotication nCNotication = new CNotication();
        nCNotication.setCreateDate(new Date());
        nCNotication.setCustomerId(cNotication.getCustomerId());
        nCNotication.setNotiContent(cNotication.getNotiContent());
        nCNotication.setNotiLink(cNotication.getNotiLink());
        nCNotication.setNotiSeen(0);

        nCNotication = notificationRepository.save(nCNotication);
        return nCNotication;
    }

    /**
     * Service tạo mới noti theo list
     * 
     * @param : obj notirequest
     * 
     * @return noti đã được tạo mới với từng item của list customerId
     */
    public void createNotiListService(MNotificationRequest cNotificationRequest) {
        for (Long customerId : cNotificationRequest.getCustomerId()) {
            CNotication nCNotication = new CNotication();
            nCNotication.setCreateDate(new Date());
            nCNotication.setCustomerId(customerId);
            nCNotication.setNotiContent(cNotificationRequest.getNotiContent());
            nCNotication.setNotiLink(cNotificationRequest.getNotiLink());
            nCNotication.setNotiSeen(0);

            nCNotication = createNotiService(nCNotication);
        }

    }

    /**
     * Service cập nhật noti
     * 
     * @param : obj noti
     * 
     * @return noti đã được cập nhật
     */
    public CNotication updateNotiService(long notiId, CNotication cNotication) {
        Optional<CNotication> notiOptional = notificationRepository.findById(notiId);
        if (notiOptional.isPresent()) {
            CNotication nCNotication = notiOptional.get();
            nCNotication.setCreateDate(new Date());
            nCNotication.setCustomerId(cNotication.getCustomerId());
            nCNotication.setNotiContent(cNotication.getNotiContent());
            nCNotication.setNotiLink(cNotication.getNotiLink());
            nCNotication.setNotiSeen(0);

            nCNotication = notificationRepository.save(nCNotication);
            return nCNotication;
        } else {
            return null;
        }
    }

    /**
     * Service xóa noti
     * 
     * @param : id của noti
     * 
     * @return noti đã được xóa
     */
    public void deleteNotiService(long id) {
        notificationRepository.deleteById(id);
    }

    /**
     * Service xem noti. truy vấn noti theo id, sau đó chuyển trạng thái noti về 1
     * 
     * @param : id của noti
     * 
     * @return noti đã được xóa
     */
    public void seeNotiService(long id) {
        Optional<CNotication> notication = notificationRepository.findById(id);
        if (notication.isPresent()) {
            CNotication nCNotication = notication.get();
            nCNotication.setNotiSeen(1);
            nCNotication = notificationRepository.save(nCNotication);
        }
    }

    /**
     * Service tạo mới đơn hàng
     * 
     * @param : obj notirequest
     * 
     * @return noti đã được tạo mới với từng item của list customerId
     */
    public void createNotiOrderService(long cusId) {
        CNotication nCNotication = new CNotication();
        nCNotication.setCreateDate(new Date());
        nCNotication.setCustomerId(cusId);
        nCNotication.setNotiContent("Đơn của bạn đã đặt thành công!");
        nCNotication.setNotiLink("user_details.html");
        nCNotication.setNotiSeen(0);
        nCNotication = createNotiService(nCNotication);
    }

    

}
