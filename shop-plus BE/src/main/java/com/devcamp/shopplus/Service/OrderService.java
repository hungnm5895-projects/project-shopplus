package com.devcamp.shopplus.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CCustomer;
import com.devcamp.shopplus.Entity.CLoyalty;
import com.devcamp.shopplus.Entity.COrder;
import com.devcamp.shopplus.Entity.CProduct;
import com.devcamp.shopplus.Model.MOrderRequest;
import com.devcamp.shopplus.Model.OrderReponse;
import com.devcamp.shopplus.Respository.LoyaltyRepository;
import com.devcamp.shopplus.Respository.OrderRepository;

/**
 * Service xử lý nghiệp vụ liên quan đến đặt hàng
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    OrderDetailService orderDetailService;

    @Autowired
    LoyaltyService loyaltyService;

    @Autowired
    LoyaltyRepository loyaltyRepository;

    @Autowired
    ProductService productService;

    @Autowired
    NotificationService notificationService;
    /**
     * Service truy vấn toàn bộ order
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ order
     */
    public List<COrder> getAllOrderService() {
        return orderRepository.findAll();
    }

    /**
     * Service truy vấn toàn bộ order của 1 khách hàng
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ order
     */
    public List<COrder> getAllOrderServiceOfCustomer(Long customerId) {
        return orderRepository.findByCustomerId(customerId);
    }

    /**
     * Service tạo mới order
     * 
     * @param : obj order
     * 
     * @return order đã được tạo mới
     */
    public COrder createOrderService(MOrderRequest cOrder) {
        COrder nOrder = new COrder();
        nOrder.setCustomerId(cOrder.getCustomerId());
        nOrder.setOrderDate(new Date());
        nOrder.setStatus(0);
        nOrder.setPrice(cOrder.getPrice());
        nOrder.setComments(cOrder.getComments());

        nOrder = orderRepository.save(nOrder);
        Long cusId = nOrder.getId();
        for (int i = 0; i < cOrder.getOrderDetails().size(); i++) {
            orderDetailService.createOrderDetail(cOrder.getOrderDetails().get(i), cusId);
        }
        // Tăng tổng lượng mua của loyalty lên
        loyaltyService.createOrderLoyalty(cOrder.getCustomerId(), cOrder.getPrice());
        return nOrder;
    }

    /**
     * Service tạo mới order
     * 
     * @param : obj order
     * 
     * @return order đã được tạo mới
     */
    public COrder createOrderAdminService(MOrderRequest cOrder) {
        COrder nOrder = new COrder();
        nOrder.setCustomerId(cOrder.getCustomerId());
        nOrder.setOrderDate(new Date());
        nOrder.setStatus(0);
        nOrder.setComments(cOrder.getComments());
        nOrder = orderRepository.save(nOrder);
        Long cusId = nOrder.getId();
        Double vPrice = 0.0;
        for (int i = 0; i < cOrder.getOrderDetails().size(); i++) {
            orderDetailService.createOrderDetail(cOrder.getOrderDetails().get(i), cusId);
            CProduct cProduct = productService.getProductByIdService(cOrder.getOrderDetails().get(i).getProductId());
            vPrice += cProduct.getByPrice();
        }

        nOrder.setPrice(vPrice);
        nOrder = orderRepository.save(nOrder);

        notificationService.createNotiOrderService(nOrder.getCustomerId());
        // Tăng tổng lượng mua của loyalty lên
        loyaltyService.createOrderLoyalty(cOrder.getCustomerId(), cOrder.getPrice());

        return nOrder;
    }

    /**
     * Service cập nhật noti
     * 
     * @param : obj noti
     * 
     * @return noti đã được cập nhật
     */
    public COrder updateOrderService(long notiId, COrder cOrder) {
        Optional<COrder> notiOptional = orderRepository.findById(notiId);
        if (notiOptional.isPresent()) {
            COrder nOrder = notiOptional.get();
            nOrder.setCustomerId(cOrder.getCustomerId());
            nOrder.setStatus(cOrder.getStatus());
            nOrder.setPrice(cOrder.getPrice());
            nOrder.setComments(cOrder.getComments());

            nOrder = orderRepository.save(nOrder);
            return nOrder;
        } else {
            return null;
        }
    }

    /**
     * Service khi required Order
     * 
     * @param : orderId
     * 
     * @return Order đã được cập nhật
     */
    public void requireOrderService(long orderId) {
        Optional<COrder> notiOptional = orderRepository.findById(orderId);
        if (notiOptional.isPresent()) {
            COrder nOrder = notiOptional.get();
            nOrder.setStatus(1);
            nOrder.setRequiredDate(new Date());

            nOrder = orderRepository.save(nOrder);
        } 
    }

    /**
     * Service khi shipped Order
     * 
     * @param : orderId
     * 
     * @return Order đã được cập nhật
     */
    public void shippedOrderService(long orderId) {
        Optional<COrder> notiOptional = orderRepository.findById(orderId);
        if (notiOptional.isPresent()) {
            COrder nOrder = notiOptional.get();
            nOrder.setStatus(5);
            nOrder.setShippedDate(new Date());

            nOrder = orderRepository.save(nOrder);

            Optional<CLoyalty> cLoyaltyOptional = loyaltyService.getCustomerByUserIdService(nOrder.getCustomerId());
            if (cLoyaltyOptional.isPresent()) {
                CLoyalty cLoyalty = cLoyaltyOptional.get();
                cLoyalty.buyOrder(nOrder.getPrice());
                cLoyalty = loyaltyRepository.save(cLoyalty);
            }
        } 
    }

    /**
     * Service khi hủy bỏ Order
     * 
     * @param : orderId
     * 
     * @return Order đã được cập nhật
     */
    public void cancelOrderService(long orderId) {
        Optional<COrder> notiOptional = orderRepository.findById(orderId);
        if (notiOptional.isPresent()) {
            COrder nOrder = notiOptional.get();
            nOrder.setStatus(6);

            nOrder = orderRepository.save(nOrder);
        } 
    }

    /**
     * Service xóa order
     * 
     * @param : id của noti
     * 
     * @return noti đã được xóa
     */
    public void deleteOrderService(long id) {
        orderRepository.deleteById(id);
    }

    /**
     * Service truy vấn số lượng đơn hàng đặt hôm nay
     * 
     * @param :
     * 
     * @return Danh sách đơn hàng đặt hôm hôm nay
     */
    public List<COrder> getOrderTodayService() {
        return orderRepository.findOrderTodayQuery();
    }

    /**
     * Service truy vấn số lượng đơn hàng chưa hoàn thành
     * 
     * @param :
     * 
     * @return Danh sách đơn hàng chưa hoàn thành
     */
    public List<COrder> getOrderNonComService() {
        return orderRepository.findOrderNonCompleteTodayQuery();
    }

    /**
     * Service truy vấn số lượng đơn hàng trong y 7 ngày
     * 
     * @param :
     * 
     * @return Danh sách đơn hàng chưa hoàn thành
     */
    public List<OrderReponse> getOrderWithinDayService() {
        return orderRepository.findOrderWithinDayQuery();
    }

    /**
     * Service truy vấn số lượng đơn hàng trong tháng này
     * 
     * @param :
     * 
     * @return Danh sách đơn hàng chưa hoàn thành
     */
    public List<OrderReponse> getOrderWithinMonthService() {
        return orderRepository.findOrderWithinMonthQuery();
    }
}
