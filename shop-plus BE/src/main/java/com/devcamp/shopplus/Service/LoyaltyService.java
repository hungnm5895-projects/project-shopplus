package com.devcamp.shopplus.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CCustomer;
import com.devcamp.shopplus.Entity.CLoyalty;
import com.devcamp.shopplus.Model.MCustomerRankResponse;
import com.devcamp.shopplus.Respository.LoyaltyRepository;

/**
 * Service xử lý nghiệp vụ liên quan đến khách hàng
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class LoyaltyService {
    @Autowired
    LoyaltyRepository loyaltyRepository;

    /**
     * Service truy vấn toàn bộ customer loyalty
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ customer loyalty
     */
    public List<CLoyalty> getAllLoyaltyService() {
        return loyaltyRepository.findAll();
    }

    /**
     * Service truy vấn loyalty theo ID của customer
     * 
     * @param : user id
     * 
     * @return Optional loyalty
     */
    public Optional<CLoyalty> getCustomerByUserIdService(long id) {
        return loyaltyRepository.findByCustomerId(id);
    }

    /**
     * Service tạo mới
     * 
     * @param : Obj Customer
     * 
     * @return loyalty
     */
    public void createLoyaltyService(long customerId) {
        CLoyalty nCustomer = new CLoyalty();
        nCustomer.setLoyaltyPoint(0);
        nCustomer.setAmmountBuy(0);
        nCustomer.setConsumptionPoint(0);
        nCustomer.setRefundRate(0.01);
        nCustomer.setCustomerId(customerId);

        nCustomer = loyaltyRepository.save(nCustomer);
    }

    /**
     * Service update Loaylty
     * 
     * @param : id của loyalty
     * 
     * @return loyalty đã được sửa
     */
    public CLoyalty updateLoyaltyService(long loyId, CLoyalty cLoyalty) {
        Optional<CLoyalty> nLoyaltyOptional = loyaltyRepository.findById(loyId);
        if (nLoyaltyOptional.isPresent()) {
            CLoyalty nLoyalty = nLoyaltyOptional.get();
            nLoyalty.setAmmountBuy(cLoyalty.getAmmountBuy());
            nLoyalty.setConsumptionPoint(cLoyalty.getConsumptionPoint());
            nLoyalty.setRefundRate(cLoyalty.getRefundRate());
            nLoyalty = loyaltyRepository.save(nLoyalty);
            return nLoyalty;
        } else {
            return null;
        }
    }

    /**
     * Service delete loyalty
     * 
     * @param : id của loyalty và obj loyalty
     * 
     * @return loyalty được xóa
     */
    public void deleteLoyalty(long id) {
        loyaltyRepository.deleteById(id);
    }

    /**
     * Service khi mua hàng
     * 
     * @param : id của khách hàng
     * @param : tổng số tiền đã mua
     * 
     * @return ammountBuy tăng lên 1 lượng amount
     */
    public void createOrderLoyalty(long customerId, double ammount) {
        Optional<CLoyalty> nOptional = getCustomerByUserIdService(customerId);
        if (nOptional.isPresent()) {
            CLoyalty cLoyalty = nOptional.get();
            cLoyalty.buyOrder(ammount);
            cLoyalty = loyaltyRepository.save(cLoyalty);
        }
    }

    /**
     * Service truy vấn số lượng khách hàng theo rank
     * 
     * @param :
     * 
     * @return Danh sách khách hàng theo rank
     */
    public MCustomerRankResponse getCustomerRankService() {
        int rankNormal = 0, rankGold = 0, rankSilver = 0, rankVip = 0, rankPlatinum = 0;
        List<CLoyalty> loyList = this.getAllLoyaltyService();
        for (CLoyalty cLoyalty : loyList) {
            switch (cLoyalty.getRank()) {
                case 0:
                    rankNormal++;
                    break;
                case 1:
                    rankVip++;
                    break;
                case 2:
                    rankSilver++;
                    break;
                case 3:
                    rankGold++;
                    break;
                case 4:
                    rankPlatinum++;
                    break;
            }
        }
        MCustomerRankResponse nCustomerRankResponse = new MCustomerRankResponse(rankNormal, rankVip, rankSilver, rankGold, rankPlatinum);
        return nCustomerRankResponse;
    }
}
