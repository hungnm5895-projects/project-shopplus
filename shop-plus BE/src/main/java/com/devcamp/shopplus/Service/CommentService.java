package com.devcamp.shopplus.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CComment;
import com.devcamp.shopplus.Respository.CommentRepository;

/**
 * Service xử lý nghiệp vụ liên quan đến comment của user
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class CommentService {
    @Autowired
    CommentRepository commentRepository;

    /**
     * Service truy vấn toàn bộ comment
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ comment
     */
    public List<CComment> getAllCommentService() {
        return commentRepository.findAll();
    }

    /**
     * Service truy vấn toàn bộ comment của 1 user
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ comment
     */
    public List<CComment> getAllCommentOfUserService(Long userId) {
        return commentRepository.findByUserId(userId);
    }

    /**
     * Service truy vấn toàn bộ comment của 1 product
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ comment của product
     */
    public List<CComment> getAllCommentOfProductService(Long productId) {
        return commentRepository.findByProductId(productId);
    }

    /**
     * Service truy vấn toàn bộ comment của 1 product có phân trang
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ comment của product
     */
    public List<CComment> getAllCommentOfProductPagingService(Long productId,String page, String size) {
        Pageable pageElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        return commentRepository.findByProductIdDetail(productId,pageElements);
    }

    /**
     * Service post comment
     * 
     * @param : obj comment
     * 
     * @return comment đã được tạo mới
     */
    public CComment postCommentService(CComment cComment) {
        CComment nComment = new CComment();
        nComment.setComment(cComment.getComment());
        nComment.setProductId(cComment.getProductId());
        nComment.setUserId(cComment.getUserId());
        nComment.setEmonation(cComment.getEmonation());
        nComment.setCreateDate(new Date());

        nComment = commentRepository.save(nComment);
        return nComment;
    }

    /**
     * Service sửa comment
     * 
     * @param : obj comment và id
     * 
     * @return comment đã được sửa
     */
    public CComment updateCommentService(Long commentId, CComment cComment) {
        Optional<CComment> notiOptional = commentRepository.findById(commentId);
        if (notiOptional.isPresent()) {
            CComment nComment = notiOptional.get();
            nComment.setComment(cComment.getComment());
            nComment.setEmonation(cComment.getEmonation());
            nComment.setCreateDate(new Date());

            nComment = commentRepository.save(nComment);
            return nComment;
        } else {
            return null;
        }
    }

    /**
     * Service xóa comment
     * 
     * @param : id của comment
     * 
     * @return comment đã được xóa
     */
    public void deleteCommentService(Long id) {
        commentRepository.deleteById(id);
    }


}
