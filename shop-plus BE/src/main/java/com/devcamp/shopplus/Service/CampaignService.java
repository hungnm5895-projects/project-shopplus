package com.devcamp.shopplus.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CCampaign;
import com.devcamp.shopplus.Respository.CampaignRespository;

/**
 * Service xử lý nghiệp vụ liên quan đến chiến dịch khuyến mại
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class CampaignService {
    @Autowired
    CampaignRespository cCampaignRespository;

    /**
     * Service truy vấn toàn bộ campaign
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ campain
     */
    public List<CCampaign> getAllCampaignService() {
        return cCampaignRespository.findAll();
    }

    /**
     * Service tạo mới campaign. Kiểm tra campainCode của obj campaign truyền vào nếu tồn trại thì trả về null. Nếu chưa tồn tại thì tạo mới campaign và lưu vào DB
     * 
     * @param : obj campaign
     * 
     * @return campaign đã được tạo mới
     */
    public CCampaign createCampaignService(CCampaign cCampaign){
        Optional<CCampaign> cpOptional = cCampaignRespository.findByCampaignCode(cCampaign.getCampaignCode());
        if (!cpOptional.isPresent()) {
            CCampaign nCampaign = new CCampaign();
            nCampaign.setCampaignCode(cCampaign.getCampaignCode());
            nCampaign.setCampaignName(cCampaign.getCampaignName());
            nCampaign.setCampaignDescription(cCampaign.getCampaignDescription());
            nCampaign.setStartDate(cCampaign.getStartDate());
            nCampaign.setEndDate(cCampaign.getEndDate());
            nCampaign.setNote(cCampaign.getNote());
            nCampaign.setPhoto(cCampaign.getPhoto());

            nCampaign = cCampaignRespository.save(nCampaign);
            return nCampaign;
        } else {
            return null;
        }
    }

    /**
     * Service sửa thông tin CTKM, Kiểm tra campainCode của obj campaign truyền vào nếu không tồn trại thì trả về null. Nếu đã tồn tại thì sửa thông tin campaign và lưu vào DB
     * 
     * @param : id của voucher và obj voucher tương ứng
     * 
     * @return :
     *         Null nếu voucher không tồn tại
     *         Voucher đã sửa hông tin
     */
    public CCampaign updateCampainService(int campaignId, CCampaign cCampaign) {
        Optional<CCampaign> cpOptional = cCampaignRespository.findById(campaignId);
        if (cpOptional.isPresent()) {
            CCampaign nCampaign = cpOptional.get();
            nCampaign.setCampaignCode(cCampaign.getCampaignCode());
            nCampaign.setCampaignName(cCampaign.getCampaignName());
            nCampaign.setCampaignDescription(cCampaign.getCampaignDescription());
            nCampaign.setStartDate(cCampaign.getStartDate());
            nCampaign.setEndDate(cCampaign.getEndDate());
            nCampaign.setNote(cCampaign.getNote());
            nCampaign.setPhoto(cCampaign.getPhoto());

            nCampaign = cCampaignRespository.save(nCampaign);
            return nCampaign;
        } else {
            return null;
        }
    }

    /**
     * Service xóa Campaign. Xóa campain trong DB theo ID truyền vào 
     * 
     * @param : id Campaign cần xóa
     * 
     * @return : void
     * 
     */
    public void deleteCampainService(int id) {
        cCampaignRespository.deleteById(id);
    }

    /**
     * Service truy vấn Campaign sắp hết hạn. Endate trong vòng 7 ngày tới
     * 
     * @param : 
     * 
     * @return Danh sách đơn hàng chưa hoàn thành
     */
    public List<CCampaign> getCampaignExpService(){
        return cCampaignRespository.findCampaignExpireCode();
    }
}
