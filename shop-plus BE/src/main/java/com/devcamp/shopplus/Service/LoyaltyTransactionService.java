package com.devcamp.shopplus.Service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.devcamp.shopplus.Entity.CLoyaltyTransaction;
import com.devcamp.shopplus.Respository.LoyaltyDetailRepository;

/**
 * Service xử lý nghiệp vụ liên quan đến chi tiết giao dịch loyalty
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class LoyaltyTransactionService {
    @Autowired
    LoyaltyDetailRepository loyaltyDetailRepository;

    /**
     * Service truy vấn toàn bộ loyalty transaction
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ customer loyalty
     */
    public List<CLoyaltyTransaction> getAllLoyaltyTransService() {
        return loyaltyDetailRepository.findAll();
    }

    /**
     * Service truy vấn toàn bộ loyalty transaction của 1 customerr
     * 
     * @param : id của customer
     * 
     * @return Danh sách toàn bộ customer loyalty theo id
     */
    public List<CLoyaltyTransaction> getLoyaltyTranOfCustomerId(Long id) {
        return loyaltyDetailRepository.findByLoyaltyId(id);
    }

    /**
     * Service lưu lại loyalty transaction của 1 customerr
     * 
     * @param : Obj chưa dữ liệu
     * 
     * @return loyalty transaction mới tạo mới
     */
    public CLoyaltyTransaction createLoyaltyTransaction(CLoyaltyTransaction cLoyaltyTransaction){
        CLoyaltyTransaction nCLoyaltyTransaction = new CLoyaltyTransaction();
        nCLoyaltyTransaction.setLoyaltyPoint(cLoyaltyTransaction.getLoyaltyPoint());
        nCLoyaltyTransaction.setConsumptionPoint(cLoyaltyTransaction.getConsumptionPoint());
        nCLoyaltyTransaction.setTransactionName(cLoyaltyTransaction.getTransactionName());
        nCLoyaltyTransaction.setTransactionDate(new Date());
        nCLoyaltyTransaction.setLoyaltyId(cLoyaltyTransaction.getId());
        nCLoyaltyTransaction = loyaltyDetailRepository.save(nCLoyaltyTransaction);

        return nCLoyaltyTransaction;
    }

}
