package com.devcamp.shopplus.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CCountry;
import com.devcamp.shopplus.Respository.CountryRepository;

/**
 * Service xử lý nghiệp vụ liên quan đến Quốc gia
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class CountryService {
    @Autowired
    CountryRepository countryRepository;

    public List<CCountry> getAllCountryService() {
        List<CCountry> lstProvinces = new ArrayList<>();
        countryRepository.findAll().forEach(lstProvinces::add);
        return lstProvinces;
    }

    public Optional<CCountry> getCountryByIdService(int id) {
        return countryRepository.findById(id);
    }

    /**
     * Service tạo mới quốc gia
     * 
     * @param : obj quốc gia
     * 
     * @return quốc gia đã được tạo mới
     */
    public CCountry createCountryService(CCountry cCountry) {
        Optional<CCountry> cOptional = countryRepository.findByCountryCode(cCountry.getCountryCode());
        if (!cOptional.isPresent()) {
            CCountry nCountry = new CCountry();
            nCountry.setCountryCode(cCountry.getCountryCode());
            nCountry.setCountryName(cCountry.getCountryName());
            nCountry = countryRepository.save(nCountry);
            return nCountry;
        } else {
            return null;
        }

    }
}
