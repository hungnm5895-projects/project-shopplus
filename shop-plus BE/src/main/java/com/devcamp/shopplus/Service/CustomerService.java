package com.devcamp.shopplus.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CCustomer;
import com.devcamp.shopplus.Model.MCustomerRankResponse;
import com.devcamp.shopplus.Respository.CustomerRespository;

/**
 * Service xử lý nghiệp vụ liên quan đến khách hàng
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class CustomerService {
    @Autowired
    CustomerRespository customerRespository;

    @Autowired
    LoyaltyService loyaltyService;

    /**
     * Service truy vấn toàn bộ khách hàng
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ khách hàng
     */
    public List<CCustomer> getAllCustomerService() {
        return customerRespository.findAll();
    }

    /**
     * Service truy vấn khách hàng theo ID
     * 
     * @param : ID của Customer
     * 
     * @return Optional CCustomer
     */
    public Optional<CCustomer> getCustomerByIdService(long id) {
        return customerRespository.findById(id);
    }

    /**
     * Service truy vấn khách hàng theo ID của user
     * 
     * @param : user id
     * 
     * @return Optional CCustomer
     */
    public Optional<CCustomer> getCustomerByUserIdService(long id) {
        return customerRespository.findByUserId(id);
    }

    /**
     * Service tạo mới
     * 
     * @param : Obj Customer
     * 
     * @return Optional CCustomer
     */
    public CCustomer createCustomerService(CCustomer cCustomer) {
        CCustomer nCustomer = new CCustomer();
        nCustomer.setName(cCustomer.getName());
        nCustomer.setEmail(cCustomer.getEmail());
        nCustomer.setAddress(cCustomer.getAddress());
        nCustomer.setPhone(cCustomer.getPhone());
        nCustomer.setProvinceId(cCustomer.getProvinceId());
        nCustomer.setCreateDate(new Date());
        nCustomer.setUserId(cCustomer.getUserId());

        nCustomer = customerRespository.save(nCustomer);
        loyaltyService.createLoyaltyService(nCustomer.getId());
        return nCustomer;
    }

    /**
     * Service cập nhật khách hàng
     * 
     * @param : Obj Customer và id của customer
     * 
     * @return Optional CCustomer
     */
    public CCustomer updateCustomerService(long id, CCustomer cCustomer) {
        Optional<CCustomer> cusOptional = customerRespository.findById(id);
        if (cusOptional.isPresent()) {
            CCustomer nCustomer = cusOptional.get();
            nCustomer.setName(cCustomer.getName());
            nCustomer.setEmail(cCustomer.getEmail());
            nCustomer.setAddress(cCustomer.getAddress());
            nCustomer.setPhone(cCustomer.getPhone());
            nCustomer.setProvinceId(cCustomer.getProvinceId());
            nCustomer.setCreateDate(new Date());

            nCustomer = customerRespository.save(nCustomer);
            return nCustomer;
        } else
            return null;
    }

    /**
     * Service xóa khách hàng trên DB
     * 
     * @param : id của customer cần xóa
     * 
     * @return void
     */
    public void deleteCustomer(long id) {
        customerRespository.deleteById(id);
    }

    /**
     * Service truy vấn số lượng khách hàng đăng ký hôm nay
     * 
     * @param : 
     * 
     * @return Danh sách khách hàng đăng ký hôm nay
     */
    public List<CCustomer> getCustomerCreateTodayService(){
        return customerRespository.findCustomerCreateTodayQuery();
    }

    
}
