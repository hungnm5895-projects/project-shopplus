package com.devcamp.shopplus.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CBrand;
import com.devcamp.shopplus.Respository.BrandRepository;

/**
 * Service xử lý nghiệp vụ liên quan đến quốc gia
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class BrandService {
    @Autowired
    BrandRepository brandRepository;

    /**
     * Service truy vấn toàn bộ thương hiệu
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ thương hiệu
     */
    public List<CBrand> getAllContriesService() {
        return brandRepository.findAll();
    }

    /**
     * Service truy vấn toàn bộ Brand theo id
     * 
     * @param : brand id
     * 
     * @return brand tương ứng
     */
    public Optional<CBrand> getBrandByIdService(int brandId) {
        return brandRepository.findById(brandId);
    }

    /**
     * Service tạo mới brand
     * 
     * @param : obj brand
     * 
     * @return brand đã được tạo mới
     */
    public CBrand createBrandService(CBrand cBrand) {
        CBrand nBrand = new CBrand();
        nBrand.setBrandCountryId(cBrand.getBrandCountryId());
        nBrand.setBrandDescription(cBrand.getBrandDescription());
        nBrand.setBrandName(cBrand.getBrandName());

        nBrand = brandRepository.save(nBrand);
        return nBrand;
    }

    /**
     * Service cập nhật brand
     * 
     * @param : obj brand và id
     * 
     * @return brand đã được cập nhật
     */
    public CBrand updateBrandService(int brandId, CBrand cBrand) {
        Optional<CBrand> notiOptional = brandRepository.findById(brandId);
        if (notiOptional.isPresent()) {
            CBrand nBrand = notiOptional.get();
            nBrand.setBrandCountryId(cBrand.getBrandCountryId());
            nBrand.setBrandDescription(cBrand.getBrandDescription());
            nBrand.setBrandName(cBrand.getBrandName());

            nBrand = brandRepository.save(nBrand);
            return nBrand;
        } else {
            return null;
        }
    }

    /**
     * Service xóa brand
     * 
     * @param : id của brand
     * 
     * @return brand đã được xóa
     */
    public void deleteBrandService(int id) {
        brandRepository.deleteById(id);
    }



}
