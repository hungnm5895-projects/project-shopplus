package com.devcamp.shopplus.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CCountry;
import com.devcamp.shopplus.Entity.COffice;
import com.devcamp.shopplus.Respository.CountryRepository;
import com.devcamp.shopplus.Respository.OfficeRepository;

/**
 * Service xử lý nghiệp vụ liên quan đến Chi nhánh
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class OfficeService {
    @Autowired
    OfficeRepository officeRepository;

    public List<COffice> getAllOfficeService() {
        List<COffice> lstProvinces = new ArrayList<>();
        officeRepository.findAll().forEach(lstProvinces::add);
        return lstProvinces;
    }

    public Optional<COffice> getOfficeByIdService(long id) {
        return officeRepository.findById(id);
    }

    public List<COffice> getOfficeByProvinceIdService(int id) {
        return officeRepository.findByProvinceId(id);
    }
}
