package com.devcamp.shopplus.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CVoucher;
import com.devcamp.shopplus.Respository.VoucherRespository;

/**
 * Service xử lý nghiệp vụ liên quan đến voucher
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class VoucherService {
    @Autowired
    VoucherRespository voucherRespository;

    /**
     * Service truy vấn toàn bộ voucher
     * 
     * @param : mã voucher
     * 
     * @return Danh sách toàn bộ voucher
     */
    public List<CVoucher> getAllVoucherService() {
        return voucherRespository.findAll();
    }

    /**
     * Service truy vấn voucher theo voucher code
     * 
     * @param : voucher code
     * 
     * @return voucher có code tương ứng
     */
    public CVoucher getVoucherByVoucherCodeService(String voucherCode) {
        Optional<CVoucher> voucherOptional = voucherRespository.findByVoucherCode(voucherCode);
        if (voucherOptional.isPresent()) {
            if (voucherOptional.get().getUseable() == 1 && voucherOptional.get().getExpireDate().compareTo(new Date()) > 0) { //Nếu voucher còn HSD và chưa sử dụng
                return voucherOptional.get();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Service tạo mới voucher
     * 
     * @param : obj voucher
     * 
     * @return Voucher đã được tạo mới
     */
    public CVoucher createVoucherService(CVoucher cVoucher) {
        
        Optional<CVoucher> voucherOptional = voucherRespository.findByVoucherCode(cVoucher.getVoucherCode());
        if (!voucherOptional.isPresent()) {
            CVoucher newVoucher = new CVoucher();
            newVoucher.setVoucherCode(cVoucher.getVoucherCode());
            newVoucher.setVoucherName(cVoucher.getVoucherName());
            newVoucher.setVoucherValue(cVoucher.getVoucherValue());
            newVoucher.setCreateDate(new Date()); // ngày tạo là ngày hiện tại
            newVoucher.setExpireDate(cVoucher.getExpireDate());
            newVoucher.setUseable(1); // Voucher tạo mới là dùng được
            newVoucher.setType(cVoucher.getType());

            newVoucher = voucherRespository.save(newVoucher);
            return newVoucher;
        } else {
            return null;
        }
    }

    /**
     * Service sửa thông tin voucher voucher
     * 
     * @param : id của voucher và obj voucher tương ứng
     * 
     * @return :
     *         Null nếu voucher không tồn tại
     *         Voucher đã sửa hông tin
     */
    public CVoucher updateVoucherService(long voucherId, CVoucher cVoucher) {
        Optional<CVoucher> voucherOptional = voucherRespository.findById(voucherId);
        if (voucherOptional.isPresent()) {
            CVoucher newVoucher = voucherOptional.get();
            newVoucher.setVoucherCode(cVoucher.getVoucherCode());
            newVoucher.setVoucherName(cVoucher.getVoucherName());
            newVoucher.setVoucherValue(cVoucher.getVoucherValue());
            newVoucher.setCreateDate(new Date()); // ngày cập nhật là ngày hiện tại
            newVoucher.setExpireDate(cVoucher.getExpireDate());
            newVoucher.setUseable(cVoucher.getUseable());
            newVoucher.setType(cVoucher.getType());

            newVoucher = voucherRespository.save(newVoucher);
            return newVoucher;
        } else {
            return null;
        }
    }

    /**
     * Service xóa voucher
     * 
     * @param : id voucher cần xóa
     * 
     * @return : void
     * 
     */
    public void deleteVoucherService(long id) {
        voucherRespository.deleteById(id);
    }

    /**
     * Service sử dụng voucher
     * 
     * @param : mã voucher sử dụng
     * 
     * @return :
     *         Null nếu voucher không tồn tại
     *         Voucher đã sử dụng
     * 
     */
    public CVoucher useVoucherService(String voucherCode) {
        Optional<CVoucher> voucherOptional = voucherRespository.findByVoucherCode(voucherCode);
        if (voucherOptional.isPresent()) {
            CVoucher saVoucher = voucherOptional.get();
            saVoucher.setUseable(2);
            saVoucher = voucherRespository.save(saVoucher);
            return saVoucher;
        } else {
            return null;
        }
    }
}
