package com.devcamp.shopplus.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CDistrict;
import com.devcamp.shopplus.Entity.CProvince;
import com.devcamp.shopplus.Respository.DistrictRepository;
import com.devcamp.shopplus.Respository.ProvinceRepository;

/**
 * Service xử lý nghiệp vụ liên quan đến Quận/huyện
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class DistrictService {
    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    ProvinceRepository provinceRepository;

    // hàm truyền vào Province Id và trả ra list Districts
    public Set<CDistrict> getDistrictByProvinceIdService(int provinceId) {
        Optional<CProvince> provOptional = provinceRepository.findById(provinceId);
        if (provOptional.isPresent()) {
            return provOptional.get().getDistricts();
        } else {
            return null;
        }
    }

    // Service Lấy toàn bộ district
    public List<CDistrict> getAllDistrictsService() {
        return districtRepository.findAll();
    }

    // Lấy district theo id
    public Optional<CDistrict> getDistrictByIdService(int id) {
        return districtRepository.findById(id);
    }
}
