package com.devcamp.shopplus.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.COrderDetail;
import com.devcamp.shopplus.Entity.CProduct;
import com.devcamp.shopplus.Respository.OrderDetailRepository;
import com.devcamp.shopplus.Respository.ProductRespository;

/**
 * Service xử lý nghiệp vụ liên quan đến chi tiết order
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class OrderDetailService {
    @Autowired
    OrderDetailRepository orderDetailRepository;

    @Autowired
    ProductRespository productRespository;

    /**
     * Service truy vấn toàn bộ order của 1 khách hàng
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ order
     */
    public List<COrderDetail> getAllOrderDetailServiceOfOrder(Long orderId) {
        return orderDetailRepository.findByOrderId(orderId);
    }

    /**
     * Service tạo mới order detail. Sau khi tạo số lượng sản phẩm tương ứng trừ 1
     *
     * @param : obj orderDetail, order Id
     * 
     * @return void
     */
    public void createOrderDetail(COrderDetail cOrderDetail, Long orderId) {
        COrderDetail nOrderDetail = new COrderDetail();
        nOrderDetail.setOrderId(orderId);
        nOrderDetail.setProductId(cOrderDetail.getProductId());
        nOrderDetail.setPriceEach(cOrderDetail.getPriceEach());
        nOrderDetail.setQuantityOrder(cOrderDetail.getQuantityOrder());
        nOrderDetail = orderDetailRepository.save(nOrderDetail);

        Optional<CProduct> cProduct = productRespository.findById(cOrderDetail.getProductId());
        if (cProduct.isPresent()) {
            CProduct nCProduct = cProduct.get();
            nCProduct.orderProduct(cOrderDetail.getQuantityOrder());
            nCProduct = productRespository.save(nCProduct);
        }
    }

}
