package com.devcamp.shopplus.Service;

public interface TokenService {
    
    com.devcamp.shopplus.Entity.Token createToken(com.devcamp.shopplus.Entity.Token token);

    com.devcamp.shopplus.Entity.Token findByToken(String token);
}
