package com.devcamp.shopplus.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entity.CPayment;
import com.devcamp.shopplus.Respository.PaymentRepository;

/**
 * Service xử lý nghiệp vụ liên quan đến thanh toán
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@Service
public class PaymentService {
    @Autowired
    PaymentRepository paymentRepository;

    /**
     * Service truy vấn toàn bộ payment
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ payment
     */
    public List<CPayment> getAllPaymentService() {
        return paymentRepository.findAll();
    }

    /**
     * Service truy vấn toàn bộ payment của 1 khách hàng theo id của khách hàng
     * 
     * @param : id của khách hàng
     * 
     * @return Danh sách toàn bộ payment
     */
    public List<CPayment> getAllPaymentOfCustomerService(Long customer_id) {
        return paymentRepository.findByCustomer_id(customer_id);
    }

    /**
     * Service tạo mới payment
     * 
     * @param : obj payment
     * 
     * @return payment đã được tạo mới
     */
    public CPayment createPaymentService(CPayment cPayment) {

        CPayment nPayment = new CPayment();
        nPayment.setAmmount(cPayment.getAmmount());
        nPayment.setCheckNumber(cPayment.getCheckNumber());
        nPayment.setCustomer_id(cPayment.getCustomer_id());
        nPayment.setPaymentDate(new Date());// ngày tạo là ngày hiện tại

        nPayment = paymentRepository.save(nPayment);
        return nPayment;
    }

    /**
     * Service cập nhật payment
     * 
     * @param : obj payment
     * 
     * @return payment đã được cập nhật
     */
    public CPayment updatePaymentService(long paymentId, CPayment cPayment) {
        Optional<CPayment> payOptional = paymentRepository.findById(paymentId);
        if (payOptional.isPresent()) {
            CPayment nPayment = payOptional.get();
            nPayment.setAmmount(cPayment.getAmmount());
            nPayment.setCheckNumber(cPayment.getCheckNumber());
            nPayment.setCustomer_id(cPayment.getCustomer_id());
            nPayment.setPaymentDate(new Date());

            nPayment = paymentRepository.save(nPayment);
            return nPayment;
        } else {
            return null;
        }
    }

    /**
     * Service xóa payment
     * 
     * @param : id của noti
     * 
     * @return payment đã được xóa
     */
    public void deletePaymentService(long id) {
        paymentRepository.deleteById(id);
    }

}
