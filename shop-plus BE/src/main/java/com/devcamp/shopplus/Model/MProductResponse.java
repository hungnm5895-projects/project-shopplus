package com.devcamp.shopplus.Model;

import java.util.List;

import com.devcamp.shopplus.Entity.CProduct;

public class MProductResponse {
    private List<CProduct> data;
    private int total;

    public MProductResponse(List<CProduct> data, int total) {
        this.data = data;
        this.total = total;
    }

    public List<CProduct> getData() {
        return data;
    }

    public void setData(List<CProduct> data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

}
