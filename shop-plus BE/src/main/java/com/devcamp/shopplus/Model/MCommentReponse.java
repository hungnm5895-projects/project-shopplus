package com.devcamp.shopplus.Model;

import java.util.List;

import com.devcamp.shopplus.Entity.CComment;

public class MCommentReponse {
    private List<CComment> data;
    private int total;

    public MCommentReponse(List<CComment> data, int total) {
        this.data = data;
        this.total = total;
    }

    public List<CComment> getData() {
        return data;
    }

    public void setData(List<CComment> data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

}
