package com.devcamp.shopplus.Model;

public class MCustomerRankResponse {
    private int rankNormal;
    private int rankVip;
    private int rankSilver;
    private int rankGold;
    private int rankPlatinum;

    public MCustomerRankResponse() {
    }

    public MCustomerRankResponse(int rankNormal, int rankVip, int rankSilver, int rankGold, int rankPlatinum) {
        this.rankNormal = rankNormal;
        this.rankVip = rankVip;
        this.rankSilver = rankSilver;
        this.rankGold = rankGold;
        this.rankPlatinum = rankPlatinum;
    }

    public int getRankNormal() {
        return rankNormal;
    }

    public void setRankNormal(int rankNormal) {
        this.rankNormal = rankNormal;
    }

    public int getRankVip() {
        return rankVip;
    }

    public void setRankVip(int rankVip) {
        this.rankVip = rankVip;
    }

    public int getRankSilver() {
        return rankSilver;
    }

    public void setRankSilver(int rankSilver) {
        this.rankSilver = rankSilver;
    }

    public int getRankGold() {
        return rankGold;
    }

    public void setRankGold(int rankGold) {
        this.rankGold = rankGold;
    }

    public int getRankPlatinum() {
        return rankPlatinum;
    }

    public void setRankPlatinum(int rankPlatinum) {
        this.rankPlatinum = rankPlatinum;
    }

}
