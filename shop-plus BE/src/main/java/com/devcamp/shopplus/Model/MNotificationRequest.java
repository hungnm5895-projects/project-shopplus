package com.devcamp.shopplus.Model;

import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class MNotificationRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private List<Long> customerId;

    private String notiContent;

    private String notiLink;

    public MNotificationRequest() {
    }

    public List<Long> getCustomerId() {
        return customerId;
    }

    public void setCustomerId(List<Long> customerId) {
        this.customerId = customerId;
    }

    public String getNotiContent() {
        return notiContent;
    }

    public void setNotiContent(String notiContent) {
        this.notiContent = notiContent;
    }

    public String getNotiLink() {
        return notiLink;
    }

    public void setNotiLink(String notiLink) {
        this.notiLink = notiLink;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
