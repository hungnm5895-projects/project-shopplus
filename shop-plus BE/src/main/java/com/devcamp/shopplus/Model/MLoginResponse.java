package com.devcamp.shopplus.Model;

import java.util.Collection;

public class MLoginResponse {
    private String token;
    private Collection authorities;
    private long id;
    private long customerId;

    public MLoginResponse() {
    }

    public MLoginResponse(String token, Collection authorities, long id) {
        this.token = token;
        this.authorities = authorities;
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Collection getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection authorities) {
        this.authorities = authorities;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    
}
