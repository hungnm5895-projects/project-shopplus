package com.devcamp.shopplus.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.Entity.CFollower;
import com.devcamp.shopplus.Service.FollowerService;

/**
 * Controller xử lý nghiệp vụ liên quan đến thanh toán
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@CrossOrigin
@RestController
public class FollowerController {
    @Autowired
    FollowerService followerService;

    /**
     * API truy vấn toàn bộ người theo dõi
     * 
     * @return Danh sách toàn bộ người theo dõi
     */
    @GetMapping("/followers")
    public ResponseEntity<Object> getAllFollowers() {
        try {
            List<CFollower> lst = followerService.getAllFollowersService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    
    /**
     * API tạo mới người theo dõi
     * 
     * @param : obj người theo dõi
     * 
     * @return : người theo dõi được tạo mới
     * 
     */
    @PostMapping("/followers")
    public ResponseEntity<Object> createFollower(@Valid @RequestBody CFollower cFollower) {
        try {

            CFollower nPayment = followerService.createFollowerService(cFollower);
            if (nPayment != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(nPayment);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Có lỗi xảy ra, vui lòng thử lại sau");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API sửa payments
     * 
     * @param : obj payments
     * 
     * @return : payments được sửa
     * 
     */
    @PutMapping("/followers/{id}")
    public ResponseEntity<Object> updatepayment(@Valid @RequestBody CFollower CFollower,
            @PathVariable(name = "id") long id) {
        try {
            
            CFollower nPayment = followerService.updateFollowerService(id, CFollower);
            if (nPayment != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(nPayment);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Người theo dõi không tồn tại, vui lòng kiểm tra lại");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API xóa payment
     * 
     * @param :  id của payment
     * 
     * @return : payment được xóa
     * 
     */
    @DeleteMapping("/followers/{id}")
    public ResponseEntity<Object> deleteVoucher(@PathVariable(name = "id") long id) {
        try {
            followerService.deletePaymentService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }
}
