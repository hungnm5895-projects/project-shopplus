package com.devcamp.shopplus.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.COrderDetail;
import com.devcamp.shopplus.Service.OrderDetailService;

/**
 * Controller xử lý nghiệp vụ liên quan đến chi tiết đơn hàng
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class OrderDetailControler {
    @Autowired
    OrderDetailService orderDetailService;

    @GetMapping("orderDetais/orders/{id}")
    public ResponseEntity<Object> getOrderOfCustomer(@PathVariable Long id) {
        try {
            List<COrderDetail> lst = orderDetailService.getAllOrderDetailServiceOfOrder(id);
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }
}
