package com.devcamp.shopplus.Controller;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.CBrand;
import com.devcamp.shopplus.Service.BrandService;

/**
 * Controller xử lý nghiệp vụ liên quan đến thương hiệu
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class BrandController {
    @Autowired
    BrandService brandService;

    /**
     * API truy vấn toàn bộ Noti
     * 
     * @return Danh sách toàn bộ Noti
     */
    @GetMapping("/brands")
    public ResponseEntity<Object> getAllComment() {
        try {
            List<CBrand> lst = brandService.getAllContriesService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API tạo mới thương hiệu
     * 
     * @param : obj thương hiệu
     * 
     * @return : thương hiệu được tạo mới
     * 
     */
    @PostMapping("brands")
    public ResponseEntity<Object> createBrand(@Valid @RequestBody CBrand cBrand) {
        try {
            CBrand nBrand = brandService.createBrandService(cBrand);
            if (nBrand != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(nBrand);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Có lỗi xảy ra, vui lòng thử lại sau");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API sửa noti
     * 
     * @param : obj noti và id của noti tương ứng
     * 
     * @return : noti được sửa
     * 
     */
    @PutMapping("brands/{id}")
    public ResponseEntity<Object> updateNoti(@Valid @RequestBody CBrand cBrand,
            @PathVariable(name = "id") int id) {
        try {
            
            CBrand nBrand = brandService.updateBrandService(id, cBrand);
            if (nBrand != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(nBrand);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Thương hiệu không tồn tại, vui lòng kiểm tra lại");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API xóa payment
     * 
     * @param :  id của payment
     * 
     * @return : payment được xóa
     * 
     */
    @DeleteMapping("brands/{id}")
    public ResponseEntity<Object> deletePayment(@PathVariable(name = "id") int id) {
        try {
            brandService.deleteBrandService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }
}
