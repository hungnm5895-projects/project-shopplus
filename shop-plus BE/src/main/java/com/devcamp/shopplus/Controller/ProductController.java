package com.devcamp.shopplus.Controller;

import java.math.BigInteger;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.Entity.CProduct;
import com.devcamp.shopplus.Model.MProductResponse;
import com.devcamp.shopplus.Service.ProductService;

/**
 * Controller xử lý nghiệp vụ liên quan đến voucher
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class ProductController {
    @Autowired
    ProductService productService;

    /**
     * API truy vấn toàn bộ Product
     * 
     * @return Danh sách toàn bộ Product
     */
    @GetMapping("/products")
    public ResponseEntity<Object> getAllProduct() {
        try {
            List<CProduct> lstVouchers = productService.getAllProductService();
            return ResponseEntity.status(HttpStatus.OK).body(lstVouchers);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn toàn bộ Product phân trang
     * 
     * @return Danh sách toàn bộ Product
     */
    @GetMapping("/products/grid")
    public ResponseEntity<Object> getAllProductPaging(
            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "12") String size) {
        try {
            List<CProduct> lstAllProd = productService.getAllProductService();
            List<CProduct> lstProd = productService.getAllProductPagingService(page, size);
            MProductResponse nResponse = new MProductResponse(lstProd, lstAllProd.size());
            return ResponseEntity.status(HttpStatus.OK).body(nResponse);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn toàn bộ Product theo product_type phân trang
     * 
     * @return Danh sách toàn bộ Product
     */
    @GetMapping("/products/type/{type}")
    public ResponseEntity<Object> getProductLstByTypePaging(    
            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "12") String size,
            @PathVariable(name = "type") int  type) {
        try {
            List<CProduct> lstAllProd = productService.getProductLstByTypeService(type,"0","99999");
            List<CProduct> lstProd = productService.getProductLstByTypeService(type,page,size);
            MProductResponse nResponse = new MProductResponse(lstProd, lstAllProd.size());
            return ResponseEntity.status(HttpStatus.OK).body(nResponse);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn toàn bộ Product theo product_type phân trang
     * 
     * @return Danh sách toàn bộ Product
     */
    @GetMapping("/products/brand/{brandId}")
    public ResponseEntity<Object> getProductLstByBrandPaging(    
            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "12") String size,
            @PathVariable(name = "brandId") int  brandId) {
        try {
            List<CProduct> lstAllProd = productService.getProductLstByBrandService(brandId,"0","99999");
            List<CProduct> lstProd = productService.getProductLstByBrandService(brandId,page,size);
            MProductResponse nResponse = new MProductResponse(lstProd, lstAllProd.size());
            return ResponseEntity.status(HttpStatus.OK).body(nResponse);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn product theo product id
     * 
     * @param : product id
     * 
     * @return : product tương ứng với voucherCode
     */
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductInfo(@PathVariable(name = "id") Long id) {
        try {
            CProduct cProduct = productService.getProductByIdService(id);
            if (cProduct != null) {
                return ResponseEntity.status(HttpStatus.OK).body(cProduct);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body("Sản phẩm không tồn tại vui lòng kiểm tra lại! ");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Sản phẩm không tồn tại vui lòng kiểm tra lại!");
        }
    }

    /**
     * API truy vấn danh sách product trên LDP, mục xu hướng, chỉ hiển thị các sản
     * phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param : loại product và số lượng bản ghi
     * 
     * @return Danh sách toàn bộ Product loại product và số lượng bản ghi
     */
    @GetMapping("/products/index/{type}/{size}")
    public ResponseEntity<Object> getProductByTypeAndSize(@PathVariable(name = "type") int type,
            @PathVariable(name = "size") int size) {
        try {
            List<CProduct> cProductLst = productService.getProductLdpService(type, size);
            return ResponseEntity.status(HttpStatus.OK).body(cProductLst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Sản phẩm không tồn tại vui lòng kiểm tra lại!");
        }
    }

    /**
     * API truy vấn danh sách product Htrên LDP, mục HOT, chỉ hiển thị các sản
     * phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param : loại product và số lượng bản ghi
     * 
     * @return Danh sách toàn bộ Product loại product và số lượng bản ghi
     */
    @GetMapping("/products/Hot/{size}")
    public ResponseEntity<Object> getProductHotByTSize(
            @PathVariable(name = "size") int size) {
        try {
            List<CProduct> cProductLst = productService.getProductHotLdpService(size);
            return ResponseEntity.status(HttpStatus.OK).body(cProductLst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Sản phẩm không tồn tại vui lòng kiểm tra lại!");
        }
    }

    /**
     * API truy vấn danh sách product Htrên LDP, mục HOT, chỉ hiển thị các sản
     * phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param : loại product và số lượng bản ghi
     * 
     * @return Danh sách toàn bộ Product loại product và số lượng bản ghi
     */
    @GetMapping("/products/Sale")
    public ResponseEntity<Object> getProductSale() {
        try {
            List<CProduct> cProductLst = productService.getProductSaleLdpService();
            return ResponseEntity.status(HttpStatus.OK).body(cProductLst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Sản phẩm không tồn tại vui lòng kiểm tra lại!");
        }
    }

    /**
     * API truy vấn danh sách product Best Seller trên LDP, mục HOT, chỉ hiển thị
     * các sản
     * phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param : loại product và số lượng bản ghi
     * 
     * @return Danh sách toàn bộ Product loại product và số lượng bản ghi
     */
    @GetMapping("/products/BestSeller")
    public ResponseEntity<Object> getProductBestSaller() {
        try {
            List<CProduct> cProductLst = productService.getProductBestSallerLdpService();
            return ResponseEntity.status(HttpStatus.OK).body(cProductLst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Sản phẩm không tồn tại vui lòng kiểm tra lại!");
        }
    }

    /**
     * API truy vấn danh sách product Topview trên LDP, mục HOT, chỉ hiển thị các
     * sản
     * phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param : loại product và số lượng bản ghi
     * 
     * @return Danh sách toàn bộ Product loại product và số lượng bản ghi
     */
    @GetMapping("/products/TopView")
    public ResponseEntity<Object> getProductTopView() {
        try {
            List<CProduct> cProductLst = productService.getProductTopViewLdpService();
            return ResponseEntity.status(HttpStatus.OK).body(cProductLst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Sản phẩm không tồn tại vui lòng kiểm tra lại!");
        }
    }

    /**
     * API truy vấn danh sách product mới đăng, chỉ hiển thị các
     * sản
     * phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param : loại product và số lượng bản ghi
     * 
     * @return Danh sách toàn bộ Product loại product và số lượng bản ghi
     */
    @GetMapping("/products/TopNew")
    public ResponseEntity<Object> getProductNew() {
        try {
            List<CProduct> cProductLst = productService.getProductNewService();
            return ResponseEntity.status(HttpStatus.OK).body(cProductLst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Sản phẩm không tồn tại vui lòng kiểm tra lại!");
        }
    }

    /**
     * API tạo mới product
     * 
     * @param : obj product
     * 
     * @return : product được tạo mới
     * 
     */
    @PostMapping("products")
    public ResponseEntity<Object> createProduct(@Valid @RequestBody CProduct cProduct) {
        try {

            CProduct nProduct = productService.createProductService(cProduct);
            if (nProduct != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(nProduct);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Có lỗi xảy ra, vui lòng thử lại sau!");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API Sửa product
     * 
     * @param : obj voucher và id của product
     * 
     * @return : product được cập nhật
     * 
     */
    @PutMapping("products/{id}")
    public ResponseEntity<Object> updateProduct(@Valid @RequestBody CProduct cProduct,
            @PathVariable(name = "id") Long id) {
        try {
            CProduct nProduct = productService.updateProductService(id, cProduct);
            if (nProduct != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(nProduct);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API xóa product
     * 
     * @param : id của product
     * 
     * @return : product được xóa
     * 
     */
    @DeleteMapping("products/{id}")
    public ResponseEntity<Object> deleteVoucher(@PathVariable(name = "id") long id) {
        try {
            productService.deleteProductService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API khi người dùng thực hiện vote sao cho sản phẩm
     * phẩm còn hàng và thứ tự từ gần đến xa
     * 
     * @param : id của sản phẩm
     * @param : số sao vote
     * 
     * @return sản phẩm được tăng sao
     */
    @PutMapping("products/vote/{id}/{star}")
    public ResponseEntity<Object> voteProduct(@PathVariable(name = "id") long id,
            @PathVariable(name = "star") int star) {
        try {
            if (star > 5 || star < 1) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Dữ liệu sao không hợp lệ ");
            }
            CProduct nProduct = productService.voteStarService(id, star);
            return ResponseEntity.status(HttpStatus.OK).body(nProduct);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API danh sách sản phẩm bắt đầu bán hôm nay
     * 
     * @param :
     * 
     * @return danh sách sản phẩm bắt đầu bán hôm nay
     */
    @GetMapping("/products/today")
    public ResponseEntity<Object> getAllProductSaleTodayApi() {
        try {
            List<CProduct> lst = productService.getProductSaleTodayService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API danh sách sản phẩm sắp hết hàng
     * 
     * @param :
     * 
     * @return danh sách sản phẩm sắp hết hàng
     */
    @GetMapping("/products/outStock")
    public ResponseEntity<Object> getAllProductOutStockTodayApi() {
        try {
            List<CProduct> lst = productService.getProductOutStockService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API Số lượt lượt view sản phẩm
     * 
     * @param :
     * 
     * @return Số lượt lượt view sản phẩm
     */
    @GetMapping("/products/totalView")
    public ResponseEntity<Object> getTotalViewApi() {
        try {
            BigInteger lst = productService.getTotalViewService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }
}
