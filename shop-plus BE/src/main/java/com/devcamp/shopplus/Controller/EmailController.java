package com.devcamp.shopplus.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.Model.MEmailRequest;
import com.devcamp.shopplus.Service.SendMailService;

/**
 * Service xử lý nghiệp vụ liên quan đến gửi email
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class EmailController {
    @Autowired
    SendMailService sendMailService;

    @PostMapping("/email/send")
    public ResponseEntity<Object> SendEmail(@RequestBody MEmailRequest mEmailRequest) {
        try {
            for (String emailItem : mEmailRequest.getEmailList()) {
                sendMailService.sendEmail(emailItem, mEmailRequest.getEmailSubject(), mEmailRequest.getEmailContent());
            }
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }

    }

}
