package com.devcamp.shopplus.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.CProvince;
import com.devcamp.shopplus.Service.ProvinceService;

/**
 * Service xử lý nghiệp vụ liên quan đến voucher
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;

    // Api Lấy danh sách toàn bộ Tỉnh/Thành phố
    @GetMapping("/provinces")
    public ResponseEntity<Object> getAllProvinces() {
        try {
            List<CProvince> lstProvinces = provinceService.getAllProvinceService();
            return ResponseEntity.status(HttpStatus.OK).body(lstProvinces);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to load Provinces List: " + e.getCause().getCause().getMessage());
        }
    }

    @GetMapping("/provinces/{id}")
    public ResponseEntity<Object> getAllProvinces(@PathVariable(value = "id") int id) {
        try {
            Optional<CProvince> provinceOption = provinceService.getProvinceByIdService(id);
            if (provinceOption.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(provinceOption.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }
}
