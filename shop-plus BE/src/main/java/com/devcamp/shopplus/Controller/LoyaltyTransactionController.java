package com.devcamp.shopplus.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.CLoyaltyTransaction;
import com.devcamp.shopplus.Service.LoyaltyTransactionService;

/**
 * Controller xử lý nghiệp vụ liên quan đến chi tiết giao dịch loyalty
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@CrossOrigin
@RestController
public class LoyaltyTransactionController {
    @Autowired
    LoyaltyTransactionService loyaltyTransactionService;

    /**
     * API truy vấn toàn bộ loyalty transaction
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ transaction
     */
    @GetMapping("loyaltyTransactions")
    public ResponseEntity<Object> getAllLoyaltyTrans() {
        try {
            List<CLoyaltyTransaction> lst = loyaltyTransactionService.getAllLoyaltyTransService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API truy vấn toàn bộ loyalty transaction của 1 loyalty Id
     * 
     * @param : id của loyalty
     * 
     * @return Danh sách toàn bộ transaction của id tương ứng
     */
    @GetMapping("loyaltyTransactions/{id}")
    public ResponseEntity<Object> getAllLoyaltyTransOfLoyalty(@PathVariable Long id) {
        try {
            List<CLoyaltyTransaction> lst = loyaltyTransactionService.getLoyaltyTranOfCustomerId(id);
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API tạo mới loyalty transaction của 1 loyalty Id
     * 
     * @param : obj của loyalty transaction
     * 
     * @return loyalty transaction ddwwocj tạo mới
     */
    @PostMapping("loyaltyTransactions")
    public ResponseEntity<Object> createLoyaltyTransaction(@Valid @RequestBody CLoyaltyTransaction cTransaction) {
        try {
            CLoyaltyTransaction nLoyaltyTransaction = loyaltyTransactionService.createLoyaltyTransaction(cTransaction);
            return ResponseEntity.status(HttpStatus.CREATED).body(nLoyaltyTransaction);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

}
