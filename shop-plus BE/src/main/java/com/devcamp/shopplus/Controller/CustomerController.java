package com.devcamp.shopplus.Controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.CCustomer;
import com.devcamp.shopplus.Entity.CLoyalty;
import com.devcamp.shopplus.Service.CustomerService;
import com.devcamp.shopplus.Service.ExcelExporter;
import com.devcamp.shopplus.Service.LoyaltyService;

/**
 * Controller xử lý nghiệp vụ liên quan đến khách hàng
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @Autowired
    LoyaltyService loyaltyService;
    /**
     * API truy vấn toàn bộ khách hàng
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ khách hàng
     */
    @GetMapping("/customers")
    public ResponseEntity<Object> getAllCustomer() {
        try {
            List<CCustomer> lst = customerService.getAllCustomerService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API truy vấn khách hàng theo id
     * 
     * @param : id của khách hàng
     * 
     * @return khách hàng có id tương ứng
     */
    @GetMapping("/customers/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable long id) {
        try {
            Optional<CCustomer> cusOptional = customerService.getCustomerByIdService(id);
            if (cusOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(cusOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API truy vấn khách hàng theo id
     * 
     * @param : id của user
     * 
     * @return khách hàng có user id tương ứng
     */
    @GetMapping("/customers/user/{id}")
    public ResponseEntity<Object> getCustomerByUserId(@PathVariable long id) {
        try {
            Optional<CCustomer> cusOptional = customerService.getCustomerByUserIdService(id);
            if (cusOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(cusOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API tạo mới khách hàng
     * 
     * @param : obj khách hàng
     * 
     * @return khách hàng được tạo mới
     */
    @PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody CCustomer cCustomer) {
        try {
            CCustomer nCustomer = customerService.createCustomerService(cCustomer);
            return ResponseEntity.status(HttpStatus.OK).body(nCustomer);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API sửa thông tin khách hàng
     * 
     * @param : obj khách hàng và id
     * 
     * @return khách hàng được sửa
     */
    @PutMapping("/customers/{id}")
    public ResponseEntity<Object> updateCustomer(@Valid @RequestBody CCustomer cCustomer,
            @PathVariable(value = "id") long id) {
        try {
            CCustomer nCustomer = customerService.updateCustomerService(id, cCustomer);
            if (nCustomer != null) {
                return ResponseEntity.status(HttpStatus.OK).body(nCustomer);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Không tìm thấy khách hàng, vui lòng kiểm tra lại");
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API xóa thông tin khách hàng
     * 
     * @param : id khách hàng
     * 
     * @return khách hàng được xóa
     */
    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Object> deleteCustomer(@PathVariable(value = "id") long id) {
        try {
            customerService.deleteCustomer(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    @GetMapping("/export/customers/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=customers_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        List<CCustomer> customer = customerService.getAllCustomerService();
        ExcelExporter excelExporter = new ExcelExporter(customer);
        excelExporter.export(response);
    }

    /**
     * API danh sách khách hàng đăng ký hôm nay
     * 
     * @param :
     * 
     * @return danh sách khách hàng đăng ký hôm nay
     */
    @GetMapping("/customers/today")
    public ResponseEntity<Object> getAllCustomerCreateTodayApi() {
        try {
            List<CCustomer> lst = customerService.getCustomerCreateTodayService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API export danh sách khách hàng theo rank
     * 
     * @param :
     * 
     * @return export khách hàng theo rank
     */
    @GetMapping("/export/customers/rank/{rank}")
    public void exportToExcelRank(HttpServletResponse response, @PathVariable(value = "rank")int rank) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=customers_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        List<CCustomer> customerLst = new ArrayList<>();
        List<CLoyalty> loyLst = loyaltyService.getAllLoyaltyService();
        for (CLoyalty loyElement : loyLst) {
            if (loyElement.getRank() == rank) {
                Optional<CCustomer> newCustomerOptional = customerService.getCustomerByIdService(loyElement.getCustomerId());
                if (newCustomerOptional.isPresent()) {
                    customerLst.add(newCustomerOptional.get());
                }
            }
        }
        ExcelExporter excelExporter = new ExcelExporter(customerLst);
        excelExporter.export(response);
    }
    
}
