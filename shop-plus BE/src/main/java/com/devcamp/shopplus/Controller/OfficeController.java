package com.devcamp.shopplus.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.Entity.CNotication;
import com.devcamp.shopplus.Entity.COffice;
import com.devcamp.shopplus.Service.NotificationService;
import com.devcamp.shopplus.Service.OfficeService;

/**
 * Controller xử lý nghiệp vụ liên quan đến chi nhánh
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class OfficeController {
    @Autowired
    OfficeService officeService;

    /**
     * API truy vấn toàn bộ Noti
     * 
     * @return Danh sách toàn bộ Noti
     */
    @GetMapping("/offices")
    public ResponseEntity<Object> getAllOffices() {
        try {
            List<COffice> lst = officeService.getAllOfficeService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn toàn bộ Noti
     * 
     * @return Danh sách toàn bộ Noti
     */
    @GetMapping("/offices/province/{id}")
    public ResponseEntity<Object> getAllOffices(@PathVariable int id) {
        try {
            List<COffice> lst = officeService.getOfficeByProvinceIdService(id);
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }
}
