package com.devcamp.shopplus.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.CNotication;
import com.devcamp.shopplus.Model.MNotificationRequest;
import com.devcamp.shopplus.Service.NotificationService;

/**
 * Controller xử lý nghiệp vụ liên quan đến gửi noti
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class NotificationController {
    @Autowired
    NotificationService notificationService;

    /**
     * API truy vấn toàn bộ Noti
     * 
     * @return Danh sách toàn bộ Noti
     */
    @GetMapping("/notifications")
    public ResponseEntity<Object> getAllNoti() {
        try {
            List<CNotication> lst = notificationService.getAllNotiService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn Noti theo customer Id . Lấy ra 5 Noti gửi mới nhất
     * 
     * @param : customer Id
     * 
     * @return : Noti tương ứng với customer Id
     */
    @GetMapping("/notifications/customer/{id}")
    public ResponseEntity<Object> getNotiOfCustomer(@PathVariable Long id) {
        try {
            List<CNotication> lst = notificationService.getAllNotiServiceOfCustomer(id);
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API tạo mới Noti
     * 
     * @param : obj Noti
     * 
     * @return : Noti được tạo mới
     * 
     */
    @PostMapping("notifications")
    public ResponseEntity<Object> createNoti(@Valid @RequestBody CNotication cNotication) {
        try {

            CNotication nNoti = notificationService.createNotiService(cNotication);
            if (nNoti != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(nNoti);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Có lỗi xảy ra, vui lòng thử lại sau");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API sửa noti
     * 
     * @param : obj noti và id của noti tương ứng
     * 
     * @return : noti được sửa
     * 
     */
    @PutMapping("notifications/{id}")
    public ResponseEntity<Object> updateNoti(@Valid @RequestBody CNotication cNotication,
            @PathVariable(name = "id") long id) {
        try {

            CNotication nCNotication = notificationService.updateNotiService(id, cNotication);
            if (nCNotication != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(nCNotication);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Notification không tồn tại, vui lòng nhập mã voucher khác");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API xóa payment
     * 
     * @param : id của payment
     * 
     * @return : payment được xóa
     * 
     */
    @DeleteMapping("notifications/{id}")
    public ResponseEntity<Object> deletePayment(@PathVariable(name = "id") long id) {
        try {
            notificationService.deleteNotiService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API sửa noti
     * 
     * @param : obj noti và id của noti tương ứng
     * 
     * @return : noti được sửa
     * 
     */
    @PutMapping("notifications/see/{id}")
    public ResponseEntity<Object> seeNoti(@PathVariable(name = "id") long id) {
        try {

            notificationService.seeNotiService(id);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API tạo mới Noti
     * 
     * @param : obj Noti
     * 
     * @return : Noti được tạo mới
     * 
     */
    @PostMapping("/notifications/sendNoti")
    public ResponseEntity<Object> sendNotiList(@RequestBody MNotificationRequest mNotificationRequest) {
        try {

            notificationService.createNotiListService(mNotificationRequest);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }
}
