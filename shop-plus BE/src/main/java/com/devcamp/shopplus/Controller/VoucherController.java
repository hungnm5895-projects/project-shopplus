package com.devcamp.shopplus.Controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.Entity.CVoucher;
import com.devcamp.shopplus.Service.VoucherService;

/**
 * Controller xử lý nghiệp vụ liên quan đến voucher
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@CrossOrigin
@RestController
public class VoucherController {
    @Autowired
    VoucherService voucherService;

    /**
     * API truy vấn toàn bộ voucher
     * 
     * @return Danh sách toàn bộ voucher
     */
    @GetMapping("/vouchers")
    public ResponseEntity<Object> getAllVoucher() {
        try {
            List<CVoucher> lstVouchers = voucherService.getAllVoucherService();
            return ResponseEntity.status(HttpStatus.OK).body(lstVouchers);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn voucher theo voucher code
     * 
     * @param : voucherCode
     * 
     * @return : voucher tương ứng với voucherCode
     */
    @GetMapping("/vouchers/{voucherCode}")
    public ResponseEntity<Object> checkVoucher(@PathVariable(name = "voucherCode") String voucherCode) {
        try {
            CVoucher cVouchers = voucherService.getVoucherByVoucherCodeService(voucherCode);
            if (cVouchers != null) {
                return ResponseEntity.status(HttpStatus.OK).body(cVouchers);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body("Voucher không tồn tại, đã hết hạn hoặc đã được sử dụng, vui lòng kiểm tra lại! ");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Voucher không tồn tại, đã hết hạn hoặc đã được sử dụng, vui lòng kiểm tra lại! ");
        }
    }

    /**
     * API tạo mới voucher
     * 
     * @param : obj voucher
     * 
     * @return : voucher được tạo mới
     * 
     */
    @PostMapping("vouchers")
    public ResponseEntity<Object> createVoucher(@Valid @RequestBody CVoucher cVoucher) {
        try {
            // Validate ngày hết hạn phải sau ngày tạo
            if (cVoucher.getExpireDate().compareTo(new Date()) < 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Ngày hết hạn phải sau ngày hiện tại");
            }

            CVoucher cVouchers = voucherService.createVoucherService(cVoucher);
            if (cVouchers != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(cVouchers);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Mã voucher đã tồn tại, vui lòng nhập mã voucher khác");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API Sửa voucher
     * 
     * @param : obj voucher và id của voucher
     * 
     * @return : voucher được cập nhật
     * 
     */
    @PutMapping("vouchers/{id}")
    public ResponseEntity<Object> updateVoucher(@Valid @RequestBody CVoucher cVoucher,
            @PathVariable(name = "id") long id) {
        try {
            // Validate ngày hết hạn phải sau ngày tạo
            if (cVoucher.getExpireDate().compareTo(new Date()) < 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Ngày hết hạn phải sau ngày hiện tại");
            }

            CVoucher cVouchers = voucherService.updateVoucherService(id, cVoucher);
            if (cVouchers != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(cVouchers);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Mã voucher không tồn tại, vui lòng nhập mã voucher khác");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API xóa voucher
     * 
     * @param : obj voucher và id của voucher
     * 
     * @return : voucher được xóa
     * 
     */
    @DeleteMapping("vouchers/{id}")
    public ResponseEntity<Object> deleteVoucher(@PathVariable(name = "id") long id) {
        try {
            voucherService.deleteVoucherService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API Dùng voucher
     * 
     * @param : mã voucher voucher
     * 
     * @return : voucher có trạng thái useable chuyển sang 2
     * 
     */
    @PutMapping("/vouchers/redem/{voucherCode}")
    public ResponseEntity<Object> redemVoucher(@PathVariable(name = "voucherCode")String voucherCode) {
        try {
            CVoucher cVouchers = voucherService.useVoucherService(voucherCode);
            if (cVouchers != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(cVouchers);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body("Mã voucher không hợp lệ hoặc đã sử dụng, vui lòng nhập mã voucher khác");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    
}
