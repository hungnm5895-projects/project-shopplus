package com.devcamp.shopplus.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.CPayment;
import com.devcamp.shopplus.Service.PaymentService;

/**
 * Controller xử lý nghiệp vụ liên quan đến thanh toán
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@CrossOrigin
@RestController
public class PaymentController {
    @Autowired
    PaymentService paymentService;

    /**
     * API truy vấn toàn bộ payment
     * 
     * @return Danh sách toàn bộ payment
     */
    @GetMapping("/payments")
    public ResponseEntity<Object> getAllPayment() {
        try {
            List<CPayment> lst = paymentService.getAllPaymentService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn payment theo payment customer Id
     * 
     * @param : customer Id
     * 
     * @return : voucher tương ứng với customer Id
     */
    @GetMapping("/payments/customer/{id}")
    public ResponseEntity<Object> getAllPaymentOfCustomer(@PathVariable Long id) {
        try {
            List<CPayment> lst = paymentService.getAllPaymentOfCustomerService(id);
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API tạo mới payments
     * 
     * @param : obj payments
     * 
     * @return : payments được tạo mới
     * 
     */
    @PostMapping("payments")
    public ResponseEntity<Object> createPayment(@Valid @RequestBody CPayment cPayment) {
        try {

            CPayment nPayment = paymentService.createPaymentService(cPayment);
            if (nPayment != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(nPayment);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Có lỗi xảy ra, vui lòng thử lại sau");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API sửa payments
     * 
     * @param : obj payments
     * 
     * @return : payments được sửa
     * 
     */
    @PutMapping("payments/{id}")
    public ResponseEntity<Object> updatepayment(@Valid @RequestBody CPayment cPayment,
            @PathVariable(name = "id") long id) {
        try {
            
            CPayment nPayment = paymentService.updatePaymentService(id, cPayment);
            if (nPayment != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(nPayment);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Thanh toán không tồn tại, vui lòng nhập mã voucher khác");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API xóa payment
     * 
     * @param :  id của payment
     * 
     * @return : payment được xóa
     * 
     */
    @DeleteMapping("payments/{id}")
    public ResponseEntity<Object> deleteVoucher(@PathVariable(name = "id") long id) {
        try {
            paymentService.deletePaymentService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }
}
