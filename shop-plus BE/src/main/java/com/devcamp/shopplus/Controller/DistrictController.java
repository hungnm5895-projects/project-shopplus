package com.devcamp.shopplus.Controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.CDistrict;
import com.devcamp.shopplus.Service.DistrictService;

/**
 * Service xử lý nghiệp vụ liên quan đến voucher
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class DistrictController {
    @Autowired
    DistrictService districtService;

    // Api lấy các quận huyện theo Id của tỉnh/Thành phố
    @GetMapping("/provinces/{provinceId}/districts")
    public ResponseEntity<Object> getDistrictByProvinceId(@PathVariable(value = "provinceId") int provinceId) {
        try {
            Set<CDistrict> lstDistrict = districtService.getDistrictByProvinceIdService(provinceId);
            return ResponseEntity.status(HttpStatus.OK).body(lstDistrict);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }

    @GetMapping("/districts")
    public ResponseEntity<Object> getAllDistrict() {
        try {
            List<CDistrict> lstDistrict = districtService.getAllDistrictsService();
            return ResponseEntity.status(HttpStatus.OK).body(lstDistrict);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }

    @GetMapping("/districts/{id}")
    public ResponseEntity<Object> getDistrictById(@PathVariable(value = "id") int id) {
        try {
            Optional<CDistrict> distrOptional = districtService.getDistrictByIdService(id);
            if (distrOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(distrOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }
}
