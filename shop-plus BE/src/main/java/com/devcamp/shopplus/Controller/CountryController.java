package com.devcamp.shopplus.Controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.Entity.CCountry;
import com.devcamp.shopplus.Service.CountryService;

/**
 * Service xử lý nghiệp vụ liên quan đến voucher
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class CountryController {
    @Autowired
    CountryService countryService;

    /**
     * API truy vấn toàn bộ quốc gia
     * 
     * @return Danh sách toàn bộ quốc gia
     */
    @GetMapping("/countries")
    public ResponseEntity<Object> getAllCoutries() {
        try {
            List<CCountry> lst = countryService.getAllCountryService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API truy vấn quốc gia theo id
     * 
     * @param : id quốc gia
     * 
     * @return Quốc gia theo id tương ứng
     */
    @GetMapping("/countries/{id}")
    public ResponseEntity<Object> getCountryById(@PathVariable(value = "id") int id) {
        try {
            Optional<CCountry> provinceOption = countryService.getCountryByIdService(id);
            if (provinceOption.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(provinceOption.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API tạo mới quốc gia
     * 
     * @param : 
     * 
     * @return Quốc gia được tạo mới
     */
    @PostMapping("countries")
    public ResponseEntity<Object> createCountry(@Valid @RequestBody CCountry cCountry) {
        try {

            CCountry nCountry = countryService.createCountryService(cCountry);
            if (nCountry != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(nCountry);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Quốc gia đã tồn tại, vui lòng thử lại");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }
}
