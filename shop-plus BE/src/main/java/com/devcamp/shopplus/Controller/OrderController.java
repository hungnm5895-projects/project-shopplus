package com.devcamp.shopplus.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.COrder;
import com.devcamp.shopplus.Model.MOrderRequest;
import com.devcamp.shopplus.Model.OrderReponse;
import com.devcamp.shopplus.Service.OrderService;

/**
 * Controller xử lý nghiệp vụ liên quan đến đơn hàng
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class OrderController {
    @Autowired
    OrderService orderService;

    /**
     * API truy vấn toàn bộ Noti
     * 
     * @return Danh sách toàn bộ Noti
     */
    @GetMapping("/orders")
    public ResponseEntity<Object> getAllOrders() {
        try {
            List<COrder> lst = orderService.getAllOrderService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn Noti theo customer Id
     * 
     * @param : customer Id
     * 
     * @return : Noti tương ứng với customer Id
     */
    @GetMapping("/orders/customer/{id}")
    public ResponseEntity<Object> getOrderOfCustomer(@PathVariable Long id) {
        try {
            List<COrder> lst = orderService.getAllOrderServiceOfCustomer(id);
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API tạo mới order
     * 
     * @param : obj Noti
     * 
     * @return : order được tạo mới
     * 
     */
    @PostMapping("/orders")
    public ResponseEntity<Object> createOrder(@Valid @RequestBody MOrderRequest cOrder) {
        try {

            COrder nOrder = orderService.createOrderService(cOrder);
            if (nOrder != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(nOrder);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Có lỗi xảy ra, vui lòng thử lại sau");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API tạo mới order
     * 
     * @param : obj Noti
     * 
     * @return : order được tạo mới
     * 
     */
    @PostMapping("/orders/admin")
    public ResponseEntity<Object> createOrderAdmin(@Valid @RequestBody MOrderRequest cOrder) {
        try {

            COrder nOrder = orderService.createOrderAdminService(cOrder);
            if (nOrder != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(nOrder);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Có lỗi xảy ra, vui lòng thử lại sau");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API sửa noti
     * 
     * @param : obj noti và id của noti tương ứng
     * 
     * @return : noti được sửa
     * 
     */
    @PutMapping("orders/{id}")
    public ResponseEntity<Object> updateNoti(@Valid @RequestBody COrder cOrder,
            @PathVariable(name = "id") long id) {
        try {

            COrder nOrder = orderService.updateOrderService(id, cOrder);
            if (nOrder != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(nOrder);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Order không tồn tại, vui lòng thử lại");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    
    /**
     * API xóa order
     * 
     * @param : id của order
     * 
     * @return : order được xóa
     * 
     */
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable(name = "id") long id) {
        try {
            orderService.deleteOrderService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API Hủy order
     * 
     * @param : id của order
     * 
     * @return : order được hủy
     * 
     */
    @PutMapping("/orders/cancel/{id}")
    public ResponseEntity<Object> cancelOrder(@PathVariable(name = "id") long id) {
        try {
            orderService.cancelOrderService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API  required order
     * 
     * @param : id của order
     * 
     * @return : order được required
     * 
     */
    @PutMapping("/orders/required/{id}")
    public ResponseEntity<Object> requiredlOrder(@PathVariable(name = "id") long id) {
        try {
            orderService.requireOrderService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API shipped order
     * 
     * @param : id của order
     * 
     * @return : order được required
     * 
     */
    @PutMapping("/orders/shipped/{id}")
    public ResponseEntity<Object> shippedlOrder(@PathVariable(name = "id") long id) {
        try {
            orderService.shippedOrderService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }


    /**
     * API danh sách sản phẩm bắt đầu bán hôm nay
     * 
     * @param :
     * 
     * @return danh sách sản phẩm bắt đầu bán hôm nay
     */
    @GetMapping("/orders/today")
    public ResponseEntity<Object> getAllOrderTodayApi() {
        try {
            List<COrder> lst = orderService.getOrderTodayService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API danh sách sản phẩm bắt đầu bán hôm nay
     * 
     * @param :
     * 
     * @return danh sách sản phẩm bắt đầu bán hôm nay
     */
    @GetMapping("/orders/nonComplete")
    public ResponseEntity<Object> getAllOrderNonComTodayApi() {
        try {
            List<COrder> lst = orderService.getOrderNonComService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

     /**
     * API danh sách đơn hàng trong các ngày
     * 
     * @param :
     * 
     * @return danh sách sản phẩm bắt đầu bán hôm nay
     */
    @GetMapping("/orders/report7day")
    public ResponseEntity<Object> getAllOrderIndayApi() {
        try {
            List<OrderReponse> lst = orderService.getOrderWithinDayService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

     /**
     * API danh sách đơn hàng trong  tháng này
     * 
     * @param :
     * 
     * @return danh sách order  tháng này
     */
    @GetMapping("/orders/report30day")
    public ResponseEntity<Object> getAllOrderInMonthApi() {
        try {
            List<OrderReponse> lst = orderService.getOrderWithinMonthService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }
}
