package com.devcamp.shopplus.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.CLoyalty;
import com.devcamp.shopplus.Model.MCustomerRankResponse;
import com.devcamp.shopplus.Service.LoyaltyService;

/**
 * Controller xử lý nghiệp vụ liên quan đến loyalty
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class LoyaltyController {
    @Autowired
    LoyaltyService loyaltyService;

    /**
     * API truy vấn toàn bộ loyalty
     * 
     * @param : null
     * 
     * @return Danh sách toàn bộ loyalty
     */
    @GetMapping("/loyaltys")
    public ResponseEntity<Object> getAllCustomer() {
        try {
            List<CLoyalty> lst = loyaltyService.getAllLoyaltyService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * API update loyalty theo id
     * 
     * @param : id và obj loyalty
     * 
     * @return loyalty được cập nhật
     */
    @PutMapping("/loyaltys/{id}")
    public ResponseEntity<Object> updateCustomer(@Valid @RequestBody CLoyalty cLoyalty,
            @PathVariable(value = "id") long id) {
        try {
            CLoyalty nLoyalty = loyaltyService.updateLoyaltyService(id, cLoyalty);
            if (nLoyalty != null) {
                return ResponseEntity.status(HttpStatus.OK).body(nLoyalty);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Không tìm thấy khách hàng thân thiết, vui lòng kiểm tra lại");
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

    /**
     * Method lấy danh sách khách hàng theo rank, trả về danh sách giá trị hạng
     * khách hàng từ thường > vip > bạc > vàng > bạch kim
     * 
     * @param : void
     * @return : obj các giá trị số lương kh theo rank
     */
    @GetMapping("/loyaltys/ranks")
    public ResponseEntity<Object> getCustomerRankCustomer() {
        try {
            MCustomerRankResponse nCustomerRankResponse = loyaltyService.getCustomerRankService();
            return ResponseEntity.status(HttpStatus.OK).body(nCustomerRankResponse);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }

}
