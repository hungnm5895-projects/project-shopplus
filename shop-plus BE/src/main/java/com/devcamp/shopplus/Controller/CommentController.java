package com.devcamp.shopplus.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shopplus.Entity.CComment;
import com.devcamp.shopplus.Model.MCommentReponse;
import com.devcamp.shopplus.Service.CommentService;

/**
 * Controller xử lý nghiệp vụ liên quan đến gửi noti
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class CommentController {
    @Autowired
    CommentService commentService;

    /**
     * API truy vấn toàn bộ Noti
     * 
     * @return Danh sách toàn bộ Noti
     */
    @GetMapping("/comments")
    public ResponseEntity<Object> getAllComment() {
        try {
            List<CComment> lst = commentService.getAllCommentService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn toàn bộ Noti của user
     * 
     * @return Danh sách toàn bộ Noti
     */
    @GetMapping("/comments/user/{id}")
    public ResponseEntity<Object> getAllCommentOfUser(@PathVariable Long id) {
        try {
            List<CComment> lst = commentService.getAllCommentOfUserService(id);
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn toàn bộ Noti của sản phẩm
     * 
     * @return Danh sách toàn bộ sản phẩm
     */
    @GetMapping("/comments/product/{id}")
    public ResponseEntity<Object> getAllCommentOfProduct(@PathVariable Long id) {
        try {
            List<CComment> lst = commentService.getAllCommentOfProductService(id);
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API truy vấn toàn bộ Noti của sản phẩm
     * 
     * @return Danh sách toàn bộ sản phẩm
     */
    @GetMapping("/comments/productDetail/{id}")
    public ResponseEntity<Object> getAllCommentOfProductPaging(@PathVariable Long id,
            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "6") String size) {
        try {
            List<CComment> lst = commentService.getAllCommentOfProductService(id);
            List<CComment> lstComment = commentService.getAllCommentOfProductPagingService(id, page, size);
            MCommentReponse res = new MCommentReponse(lstComment, lst.size());
            return ResponseEntity.status(HttpStatus.OK).body(res);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API tạo mới Noti
     * 
     * @param : obj Noti
     * 
     * @return : Noti được tạo mới
     * 
     */
    @PostMapping("comments")
    public ResponseEntity<Object> createComment(@Valid @RequestBody CComment cComment) {
        try {

            CComment nComment = commentService.postCommentService(cComment);
            if (nComment != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(nComment);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Có lỗi xảy ra, vui lòng thử lại sau");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API sửa noti
     * 
     * @param : obj noti và id của noti tương ứng
     * 
     * @return : noti được sửa
     * 
     */
    @PutMapping("comments/{id}")
    public ResponseEntity<Object> updateComment(@Valid @RequestBody CComment cComment,
            @PathVariable(name = "id") Long id) {
        try {

            CComment nComment = commentService.updateCommentService(id, cComment);
            if (nComment != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(nComment);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Comment không tồn tại, vui lòng thử lại");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API xóa payment
     * 
     * @param : id của payment
     * 
     * @return : payment được xóa
     * 
     */
    @DeleteMapping("comments/{id}")
    public ResponseEntity<Object> deleteComment(@PathVariable(name = "id") long id) {
        try {
            commentService.deleteCommentService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

}
