package com.devcamp.shopplus.Controller;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shopplus.Entity.CCampaign;
import com.devcamp.shopplus.Service.CampaignService;

/**
 * Controller xử lý nghiệp vụ liên quan đến Campaign
 * 
 * @author hungnm
 * @version 1.0.0
 * @since 17
 */
@RestController
@CrossOrigin
public class CampaignController {
    @Autowired
    CampaignService campaignService;

    /**
     * API truy vấn toàn bộ voucher
     * 
     * @return Danh sách toàn bộ voucher
     */
    @GetMapping("/campaigns")
    public ResponseEntity<Object> getAllCampaign() {
        try {
            List<CCampaign> lst = campaignService.getAllCampaignService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API tạo mới campaign
     * 
     * @param : obj campaign
     * 
     * @return : campaign được tạo mới
     * 
     */
    @PostMapping("campaigns")
    public ResponseEntity<Object> createCampaign(@Valid @RequestBody CCampaign cCampaign) {
        try {
            // Validate kết thúc phải sau ngày bắt đầu
            if (cCampaign.getEndDate().compareTo(cCampaign.getStartDate()) < 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Ngày kết thúc phải sau ngày bắt đầu");
            }

            CCampaign nCampaign = campaignService.createCampaignService(cCampaign);
            if (nCampaign != null) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(nCampaign);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Chiến dịch khuyến mại đã tồn tại, vui lòng nhập mã Chiến dịch khác");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API Sửa campaign
     * 
     * @param : obj campaign và id của campaign
     * 
     * @return : campaign được cập nhật
     * 
     */
    @PutMapping("campaigns/{id}")
    public ResponseEntity<Object> updateCampaign(@Valid @RequestBody CCampaign cCampaign,
            @PathVariable(name = "id") int id) {
        try {
            // Validate ngày hết hạn phải sau ngày tạo
            if (cCampaign.getEndDate().compareTo(cCampaign.getStartDate()) < 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Ngày kết thúc phải sau ngày bắt đầu");
            }

            CCampaign nCampaign = campaignService.updateCampainService(id, cCampaign);
            if (nCampaign != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(nCampaign);
            } else {
                return ResponseEntity.status(HttpStatus.NO_CONTENT)
                        .body("Mã Chiến dịch khuyến mại không  tồn tại, vui lòng nhập mã voucher khác");
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API xóa campain, tìm kiếm campain theo id truyền vào và xóa trong DB
     * 
     * @param : id của campaign
     * 
     * @return : void
     * 
     */
    @DeleteMapping("campaigns/{id}")
    public ResponseEntity<Object> deleteCampaign(@PathVariable(name = "id") int id) {
        try {
            campaignService.deleteCampainService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(null);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    /**
     * API danh sách  campaign sắp hết hạn
     * 
     * @param :
     * 
     * @return danh sách campaign sắp hết hạn
     */
    @GetMapping("/campaigns/expire")
    public ResponseEntity<Object> getCampaignExpApi() {
        try {
            List<CCampaign> lst = campaignService.getCampaignExpService();
            return ResponseEntity.status(HttpStatus.OK).body(lst);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại!");
        }
    }
}
