package com.devcamp.shopplus.Respository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shopplus.Entity.CComment;
import java.util.List;


public interface CommentRepository extends JpaRepository<CComment, Long>{
    /**
     * Mothod truy vấn comment của 1 user đăng
     * 
     * @param : user id
     * 
     * @return list comment
     */
    List<CComment> findByUserId(Long userId);

    /**
     * Mothod truy vấn comment của 1 sản phẩm
     * 
     * @param : product id
     * 
     * @return list sản phẩm
     */
    List<CComment> findByProductId(Long productId);

    /**
     * Mothod truy vấn comment của 1 sản phẩm, sắp xếp từ gần đến xa và phân trạng
     * 
     * @param : product id
     * 
     * @return list sản phẩm
     */
    @Query(value = "SELECT `comment`.* FROM `comment` WHERE `comment`.product_id = :id ORDER BY `comment`.create_date DESC", nativeQuery = true)
    List<CComment> findByProductIdDetail( @Param("id") Long id, Pageable pageable);
}
