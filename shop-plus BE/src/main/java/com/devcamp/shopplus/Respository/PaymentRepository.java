package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shopplus.Entity.CPayment;
import java.util.List;


public interface PaymentRepository extends JpaRepository<CPayment, Long> {
    
    @Query(value = "SELECT * FROM payments WHERE customer_id = :id  ", nativeQuery = true)
    List<CPayment> findByCustomer_id(@Param("id") Long customer_id);
}
