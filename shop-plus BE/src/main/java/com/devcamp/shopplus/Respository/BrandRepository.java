package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.CBrand;

public interface BrandRepository extends JpaRepository<CBrand, Integer>{
    
}
