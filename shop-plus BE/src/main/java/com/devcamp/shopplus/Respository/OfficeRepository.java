package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.COffice;
import java.util.List;


public interface OfficeRepository extends JpaRepository<COffice, Long>{
   /**
    * 
    * @param provinceId
    * @return danh sách office theo thành phố
    */
    List<COffice> findByProvinceId(int provinceId);
}
