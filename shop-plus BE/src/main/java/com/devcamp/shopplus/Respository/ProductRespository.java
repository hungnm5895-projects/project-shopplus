package com.devcamp.shopplus.Respository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shopplus.Entity.CCustomer;
import com.devcamp.shopplus.Entity.CProduct;

/**
 * Respository tương tác với DataBase bảng Product
 * 
 * @author hungnm
 * @version 1.0.0
 */
public interface ProductRespository extends JpaRepository<CProduct, Long> {

    /**
     * Mothod truy vấn sản phẩm theo mã sản phẩm
     * 
     * @param : mã sản phẩm
     * 
     * @return Optional sản phẩm
     */
    Optional<CProduct> findByProductCode(String productCode);

    /**
     * Mothod truy vấn sản phẩm theo loại sản phẩm. chỉ hiển thị các sản phẩm còn
     * hàng và thứ tự từ gần đến xa
     * 
     * @param : loại sản phẩm (0: wireliess; 1: earphone; 2: wired )
     * @param : Số lượng bản ghi muốn hiển thị
     * 
     * @return list sản phẩm có số lượng bản ghi tương ứng
     */
    @Query(value = "SELECT * FROM products WHERE product_type = :product_type AND quatity_in_stock > 0 ORDER BY sale_date DESC LIMIT :size", nativeQuery = true)
    List<CProduct> findProductByTypeAndSizeQuery(@Param("product_type") int product_type, @Param("size") int size);

    /**
     * Mothod truy vấn sản phẩm HOT. chỉ hiển thị các sản phẩm còn hàng và thứ tự từ
     * gần đến xa
     * 
     * @param : Số lượng bản ghi muốn hiển thị
     * 
     * @return list sản phẩm có số lượng bản ghi tương ứng
     */
    @Query(value = "SELECT products.* FROM products WHERE products.is_hot = 1 AND products.quatity_in_stock > 0 ORDER BY products.sale_date DESC LIMIT :size", nativeQuery = true)
    List<CProduct> findProductHotWithSizeQuery(@Param("size") int size);

    /**
     * Mothod truy vấn sản phẩm Sale. chỉ hiển thị các sản phẩm còn hàng và thứ tự
     * từ gần đến xa
     * 
     * @param : Số lượng bản ghi muốn hiển thị
     * 
     * @return list sản phẩm có số lượng bản ghi tương ứng
     */
    @Query(value = "SELECT products.* FROM products WHERE products.quatity_in_stock > 0 ORDER BY products.discount DESC, products.sale_date DESC LIMIT 5", nativeQuery = true)
    List<CProduct> findProductSaleQuery();

    /**
     * Mothod truy vấn sản phẩm Best Seller. chỉ hiển thị các sản phẩm còn hàng và
     * thứ tự từ gần đến xa
     * 
     * @param : Số lượng bản ghi muốn hiển thị
     * 
     * @return list sản phẩm có số lượng bản ghi tương ứng
     */
    @Query(value = "SELECT products.* FROM products WHERE products.quatity_in_stock > 0 ORDER BY products.bought_sum DESC, products.sale_date DESC LIMIT 5", nativeQuery = true)
    List<CProduct> findProductBestSellerQuery();

    /**
     * Mothod truy vấn sản phẩm Top View. chỉ hiển thị các sản phẩm còn hàng và thứ
     * tự từ gần đến xa
     * 
     * @param : Số lượng bản ghi muốn hiển thị
     * 
     * @return list sản phẩm có số lượng bản ghi tương ứng
     */
    @Query(value = "SELECT products.* FROM products WHERE products.quatity_in_stock > 0 ORDER BY products.view DESC, products.sale_date DESC LIMIT 5", nativeQuery = true)
    List<CProduct> findProductTopViewQuery();

    /**
     * Mothod truy vấn sản phẩm Mới thêm. chỉ hiển thị các sản phẩm còn hàng và thứ
     * tự từ gần đến xa
     * 
     * @param : Số lượng bản ghi muốn hiển thị
     * 
     * @return list sản phẩm có số lượng bản ghi tương ứng
     */
    @Query(value = "SELECT products.* FROM products WHERE products.quatity_in_stock > 0 ORDER BY products.sale_date DESC LIMIT 3", nativeQuery = true)
    List<CProduct> findProductNewQuery();

    /**
     * Mothod truy vấn sản phẩm tại shop grid. thứ tự từ còn hàng > hết hàng, từ mới
     * đến xa nhất
     * 
     * 
     * @param : Số lượng bản ghi muốn hiển thị
     * 
     * @return list sản phẩm có số lượng bản ghi tương ứng
     */
    @Query(value = "SELECT products.* FROM products ORDER BY products.sale_date DESC", nativeQuery = true)
    List<CProduct> findAllProductQuery(Pageable pageable);

    /**
     * Mothod truy vấn sản phẩm tại shop grid. tìm các sản phẩm theo loại sản phẩm
     * 
     * 
     * @param : product_type
     * @param : size, page
     * 
     * @return list sản phẩm có số lượng bản ghi tương ứng
     */
    @Query(value = "SELECT * FROM products WHERE products.product_type = :type ORDER BY products.sale_date DESC", nativeQuery = true)
    List<CProduct> findAllProductByTypeQuery(@Param("type") int type, Pageable pageable);

    /**
     * Mothod truy vấn sản phẩm tại shop grid. tìm các sản phẩm theo loại sản phẩm
     * 
     * 
     * @param : product_type
     * @param : size, page
     * 
     * @return list sản phẩm có số lượng bản ghi tương ứng
     */
    @Query(value = "SELECT * FROM products WHERE products.brand_id = :brandId ORDER BY products.sale_date DESC", nativeQuery = true)
    List<CProduct> findAllProductByBrandQuery(@Param("brandId") int brandId, Pageable pageable);

    /**
     * Method truy truy vấn số lượng sản phẩm mới bán hôm nay
     * @param void
     * @return Danh sách sản phẩm bắt đầu bán hôm nay
     */
    @Query(value = "SELECT * FROM `products` WHERE  DATE(sale_date) = CURRENT_DATE()", nativeQuery = true)
    List<CProduct> findProductSaleTodayQuery();

    /**
     * Method truy truy vấn số lượng sản phẩm mới bán hôm nay
     * @param void
     * @return Danh sách sản phẩm bắt đầu bán hôm nay
     */
    @Query(value = "SELECT * FROM `products` WHERE `quatity_in_stock` < 50", nativeQuery = true)
    List<CProduct> findProductOutStockQuery();

     /**
     * Method truy truy vấn số lượng lượt xem sản phẩm
     * @param void
     * @return Số lượt xem sản phẩm
     */
    @Query(value = "SELECT sum(view) FROM `products` WHERE 1", nativeQuery = true)
    BigInteger findTotalViewQuery();

}
