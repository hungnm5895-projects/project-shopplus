package com.devcamp.shopplus.Respository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.CCountry;

/**
 * Respository tương tác với DataBase bảng Countries
 * 
 * @author hungnm
 * @version 1.0.0
 */
public interface CountryRepository extends JpaRepository<CCountry, Integer>{
    
    /**
     * 
     * @param countryCode
     * @return Optional <CCountry>
     */
    Optional<CCountry> findByCountryCode(String countryCode);
}
