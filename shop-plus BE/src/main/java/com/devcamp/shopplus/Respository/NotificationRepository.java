package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shopplus.Entity.CNotication;
import java.util.List;


public interface NotificationRepository extends JpaRepository<CNotication, Long>{
    
    @Query(value = "SELECT notifications.* FROM notifications WHERE notifications.customer_id = :id  and noti_seen = 0 ORDER BY notifications.create_date DESC LIMIT 5", nativeQuery = true)
    List<CNotication> findByCustomerIdQuery(@Param("id") Long customerId);
}
