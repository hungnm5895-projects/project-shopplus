package com.devcamp.shopplus.Respository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.CVoucher;

/**
 * Respository tương tác với DataBase bảng Vouchers
 * 
 * @author hungnm
 * @version 1.0.0
 */
public interface VoucherRespository extends JpaRepository<CVoucher, Long> {

    /**
     * Truy vấn voucher từ mã voucher.
     * 
     * @param : mã voucher
     * 
     * @return : voucher tương ứng với mã voucher
     */
    Optional<CVoucher> findByVoucherCode(String voucherCode);

}
