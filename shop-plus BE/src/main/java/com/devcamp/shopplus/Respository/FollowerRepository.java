package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.CFollower;

/**
 * Respository tương tác với DataBase bảng Followes
 * 
 * @author hungnm
 * @version 1.0.0
 */
public interface FollowerRepository extends JpaRepository<CFollower, Long>{
    
}
