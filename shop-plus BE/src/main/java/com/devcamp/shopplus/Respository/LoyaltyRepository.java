package com.devcamp.shopplus.Respository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.CLoyalty;


/**
 * Respository tương tác với DataBase bảng Loyalty
 * 
 * @author hungnm
 * @version 1.0.0
 */
public interface LoyaltyRepository extends JpaRepository<CLoyalty, Long>{
    Optional<CLoyalty> findByCustomerId(Long customerId);
}
