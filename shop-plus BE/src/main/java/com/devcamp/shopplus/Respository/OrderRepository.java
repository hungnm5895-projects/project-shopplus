package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shopplus.Entity.COrder;
import com.devcamp.shopplus.Model.OrderReponse;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<COrder, Long> {

    List<COrder> findByCustomerId(Long customerId);

    /**
     * Method truy truy vấn số đơn hàng được đặt hôm nay
     * 
     * @param void
     * @return Danh sách đơn hàng được đặt hôm nay
     */
    @Query(value = "SELECT * FROM `orders` WHERE  DATE(order_date) = CURRENT_DATE()", nativeQuery = true)
    List<COrder> findOrderTodayQuery();

    /**
     * Method truy truy vấn số đơn hàng được đặt hôm nay
     * 
     * @param void
     * @return Danh sách đơn hàng được đặt hôm nay
     */
    @Query(value = "SELECT * FROM `orders` WHERE status != 5;", nativeQuery = true)
    List<COrder> findOrderNonCompleteTodayQuery();

    /**
     * Method truy truy vấn số đơn hàng được đặt trong tuần này
     * 
     * @param : từ ngày
     * @param : đến ngày
     * @return List đơn hàng
     */
    @Query(value = "SELECT DATE(order_date) as ngay, count(*) as dem FROM orders WHERE order_date BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d') - INTERVAL DAYOFWEEK(NOW())-1 DAY AND NOW() GROUP By DATE(order_date);", nativeQuery = true)
    List<OrderReponse> findOrderWithinDayQuery();

    /**
     * Method truy truy vấn số đơn hàng được đặt trong tháng này
     * 
     * @param : từ ngày
     * @param : đến ngày
     * @return List đơn hàng
     */
    @Query(value = "SELECT DATE(order_date)  as ngay, count(*) as dem FROM orders WHERE order_date BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND NOW() GROUP By DATE(order_date);", nativeQuery = true)
    List<OrderReponse> findOrderWithinMonthQuery();
}
