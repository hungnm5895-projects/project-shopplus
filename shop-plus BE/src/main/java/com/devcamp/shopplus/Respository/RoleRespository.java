package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.Role;


public interface RoleRespository extends JpaRepository<Role, Long>{
    Role findByRoleKey(String roleKey);

}
