package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.*;

public interface TokenRepository extends JpaRepository<Token, Long> {

    Token findByToken(String token);
}
