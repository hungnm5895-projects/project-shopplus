package com.devcamp.shopplus.Respository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.shopplus.Entity.CCampaign;

/**
 * Respository tương tác với DataBase bảng Campaign
 * 
 * @author hungnm
 * @version 1.0.0
 */
public interface CampaignRespository extends JpaRepository<CCampaign, Integer> {

    /**
     * Truy vấn campaign từ mã campaign.
     * 
     * @param : mã campaign
     * 
     * @return : campaign tương ứng với mã campaign
     */
    Optional<CCampaign> findByCampaignCode(String campaignCode);

    /**
     * Truy vấn campaign sắp hết hạn, expired < 7
     * 
     * @param :
     * 
     * @return : campaign sắp hết hạn
     */
    @Query(value = "SELECT * FROM `campaign` WHERE end_date > date_sub(curdate(), interval 7 day)", nativeQuery = true)
    List<CCampaign> findCampaignExpireCode();
}
