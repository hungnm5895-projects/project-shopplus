package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.COrderDetail;
import java.util.List;


public interface OrderDetailRepository extends JpaRepository<COrderDetail, Long>{
    List<COrderDetail> findByOrderId(Long orderId);
}
