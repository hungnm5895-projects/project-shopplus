package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.CProvince;

/**
 * Respository tương tác với DataBase bảng Province
 * 
 * @author hungnm
 * @version 1.0.0
 */
public interface ProvinceRepository extends JpaRepository<CProvince, Integer> {

}
