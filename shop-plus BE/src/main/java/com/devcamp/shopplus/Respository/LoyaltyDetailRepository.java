package com.devcamp.shopplus.Respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.CLoyaltyTransaction;

public interface LoyaltyDetailRepository extends JpaRepository<CLoyaltyTransaction, Long>{
    List<CLoyaltyTransaction> findByLoyaltyId(long id);
}
