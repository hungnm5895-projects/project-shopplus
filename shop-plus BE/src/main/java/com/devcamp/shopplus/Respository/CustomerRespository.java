package com.devcamp.shopplus.Respository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.devcamp.shopplus.Entity.CCustomer;


/**
 * Respository tương tác với DataBase bảng Customer
 * 
 * @author hungnm
 * @version 1.0.0
 */
public interface CustomerRespository extends JpaRepository<CCustomer, Long>{
    Optional<CCustomer> findByUserId(long userId);

    /**
     * Method truy vấn số lượng khách hàng đăng ký hôm nay
     * @param void
     * @return Danh sách khách hàng đăng ký hôm nay
     */
    @Query(value = "SELECT * FROM `customers` WHERE DATE(create_date) = CURRENT_DATE()", nativeQuery = true)
    List<CCustomer> findCustomerCreateTodayQuery();
}
