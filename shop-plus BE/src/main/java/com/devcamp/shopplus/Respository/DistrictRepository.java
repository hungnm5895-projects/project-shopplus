package com.devcamp.shopplus.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entity.CDistrict;
/**
 * Respository tương tác với DataBase bảng District
 * 
 * @author hungnm
 * @version 1.0.0
 */
public interface DistrictRepository extends JpaRepository<CDistrict, Integer> {

}
