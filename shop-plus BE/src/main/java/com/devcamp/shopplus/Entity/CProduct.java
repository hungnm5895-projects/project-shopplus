package com.devcamp.shopplus.Entity;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import org.hibernate.validator.constraints.Range;

/**
 * Entity tương ứng bảng Province quản lý thông tin về Tỉnh/Thành phố
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "products")
public class CProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_code")
    @NotNull(message = "Mã sản phẩm không được để trống")
    private String productCode;

    @Column(name = "product_name")
    @NotNull(message = "Tên sản phẩm không được để trống")
    private String productName;

    @Column(name = "product_type")
    @NotNull(message = "Loại sản phẩm không được để trống")
    @Range(min = 0, max = 2, message = "Loại Sản phẩm không hợp lệ")
    private int productType;

    @Column(name = "product_description")
    private String productDescription;

    @Column(name = "product_line")
    @Range(min = 0, max = 2, message = "Dòng Sản phẩm không hợp lệ")
    private int productLine;

    @Column(name = "product_scale")
    private double productScale;

    @NotNull(message = "Mã Thương hiệu không được để trống")
    @PositiveOrZero(message = "Mã Thương hiệu không hợp lệ")
    private int brandId;


    @NotNull(message = "Số lượng trong kho không được để trống")
    @PositiveOrZero(message = "Số lượng trong kho không hợp lệ")
    private int quatityInStock;

    @NotNull(message = "Giá triền không được để trống")
    @PositiveOrZero(message = "Giá triền không hợp lệ")
    private double byPrice;
    private double discount;
    private int isHot;
    private int isTrend;
    private int view;
    private int rateSum;
    private int rateCount;
    private String photo;
    private int boughtSum;
    private double pinTime;
    private double boxTime;
    private String gateCharge;
    private String audioTechnology;
    private String compatible;
    private int connectSameTime;
    private String connectTechnology;
    private int madeIn;
    private double longX;
    private double widthY;
    private double highZ;
    private double weightW;

    @Temporal(TemporalType.TIMESTAMP)
    private Date saleDate;

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public double getByPrice() {
        return byPrice;
    }

    public void setByPrice(double byPrice) {
        this.byPrice = byPrice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getIsHot() {
        return isHot;
    }

    public void setIsHot(int isHot) {
        this.isHot = isHot;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getRateSum() {
        return rateSum;
    }

    public void setRateSum(int rateSum) {
        this.rateSum = rateSum;
    }

    public int getRateCount() {
        return rateCount;
    }

    public void setRateCount(int rateCount) {
        this.rateCount = rateCount;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getBoughtSum() {
        return boughtSum;
    }

    public void setBoughtSum(int boughtSum) {
        this.boughtSum = boughtSum;
    }

    public CProduct() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getProductLine() {
        return productLine;
    }

    public void setProductLine(int productLine) {
        this.productLine = productLine;
    }

    public double getProductScale() {
        return productScale;
    }

    public void setProductScale(double productScale) {
        this.productScale = productScale;
    }

   

    public int getQuatityInStock() {
        return quatityInStock;
    }

    public void setQuatityInStock(int quatityInStock) {
        this.quatityInStock = quatityInStock;
    }

    public double getPinTime() {
        return pinTime;
    }

    public void setPinTime(double pinTime) {
        this.pinTime = pinTime;
    }

    public double getBoxTime() {
        return boxTime;
    }

    public void setBoxTime(double boxTime) {
        this.boxTime = boxTime;
    }

    public String getGateCharge() {
        return gateCharge;
    }

    public void setGateCharge(String gateCharge) {
        this.gateCharge = gateCharge;
    }

    public String getAudioTechnology() {
        return audioTechnology;
    }

    public void setAudioTechnology(String audioTechnology) {
        this.audioTechnology = audioTechnology;
    }

    public String getCompatible() {
        return compatible;
    }

    public void setCompatible(String compatible) {
        this.compatible = compatible;
    }

    public int getConnectSameTime() {
        return connectSameTime;
    }

    public void setConnectSameTime(int connectSameTime) {
        this.connectSameTime = connectSameTime;
    }

    public String getConnectTechnology() {
        return connectTechnology;
    }

    public void setConnectTechnology(String connectTechnology) {
        this.connectTechnology = connectTechnology;
    }

    public int getMadeIn() {
        return madeIn;
    }

    public void setMadeIn(int madeIn) {
        this.madeIn = madeIn;
    }

    public double getLongX() {
        return longX;
    }

    public void setLongX(double longX) {
        this.longX = longX;
    }

    public double getWidthY() {
        return widthY;
    }

    public void setWidthY(double widthY) {
        this.widthY = widthY;
    }

    public double getHighZ() {
        return highZ;
    }

    public void setHighZ(double highZ) {
        this.highZ = highZ;
    }

    public double getWeightW() {
        return weightW;
    }

    public void setWeightW(double weightW) {
        this.weightW = weightW;
    }

    public int getIsTrend() {
        return isTrend;
    }

    public void setIsTrend(int isTrend) {
        this.isTrend = isTrend;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

     /**
     * Mothod xem thông tin sản phẩm
     * 
     * @param : void
     * 
     * @return số view tăng lên 1
     */
    public void viewProduct() {
        this.view++;
    }

     /**
     * Mothod xem số sao của sản phẩm
     * 
     * @param : void
     * 
     * @return số sao của sản phẩm theo số nguyên
     */
    public int getStar() {
        int star = 0;
        if (this.rateCount != 0) {
            star = rateSum/rateCount;
        }

        return star;
    }

     /**
     * Mothod vote sản phẩm, thực hiện khi gọi lên vote sản phẩm
     * 
     * @param : Số sao
     * 
     * @return Tổng số sao (rateSum) của sản phẩm tăng lên theo số sao vote, số lượng vote cộng 1
     */
    public void voteStar(int starVote){
        this.rateCount++;
        this.rateSum += starVote;
    }

     /**
     * Mothod khi order sản phẩm, thực hiện giảm số lượng trong kho và cộng vào bought sum
     * 
     * @param : Số sao
     * 
     * @return Tổng số sao (rateSum) của sản phẩm tăng lên theo số sao vote, số lượng vote cộng 1
     */
    public void orderProduct(int qty){
        this.quatityInStock = this.quatityInStock - qty;
        boughtSum += qty;
    }
}
