package com.devcamp.shopplus.Entity;

import java.util.Date;

import javax.persistence.*;

/**
 * Entity tương ứng bảng Province quản lý thông tin về người theo dõi
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "followers")
public class CFollower {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String email;

    private Date createDate;


    public CFollower() {
    }


    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }


}
