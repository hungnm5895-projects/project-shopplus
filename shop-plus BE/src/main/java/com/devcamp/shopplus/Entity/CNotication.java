package com.devcamp.shopplus.Entity;

import java.util.Date;

import javax.persistence.*;

/**
 * Entity tương ứng bảng notifications quản lý thông tin noti push cho khách
 * hàng
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "notifications")
public class CNotication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long customerId;

    private String notiContent;
    private Date createDate;

    private String notiLink;
    private int notiSeen;

    public CNotication() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNotiContent() {
        return notiContent;
    }

    public void setNotiContent(String notiContent) {
        this.notiContent = notiContent;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getNotiLink() {
        return notiLink;
    }

    public void setNotiLink(String notiLink) {
        this.notiLink = notiLink;
    }

    public int getNotiSeen() {
        return notiSeen;
    }

    public void setNotiSeen(int notiSeen) {
        this.notiSeen = notiSeen;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

}
