package com.devcamp.shopplus.Entity;

import javax.persistence.*;

/**
 * Entity tương ứng bảng loyalty quản lý thông tin về khách hàng loyalty
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "loyalty")
public class CLoyalty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int loyaltyPoint;
    private int consumptionPoint;
    private double refundRate;
    private double ammountBuy;
    private Long customerId;

    public CLoyalty() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getLoyaltyPoint() {
        return loyaltyPoint;
    }

    public void setLoyaltyPoint(int loyaltyPoint) {
        this.loyaltyPoint = loyaltyPoint;
    }

    public double getRefundRate() {
        return refundRate;
    }

    public void setRefundRate(double refundRate) {
        this.refundRate = refundRate;
    }

    public double getAmmountBuy() {
        return ammountBuy;
    }

    public void setAmmountBuy(double ammountBuy) {
        this.ammountBuy = ammountBuy;
    }

    public int getConsumptionPoint() {
        return consumptionPoint;
    }

    public void setConsumptionPoint(int consumptionPoint) {
        this.consumptionPoint = consumptionPoint;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public int getRank() {
        if (ammountBuy <= 5000000) {
            return 0;
        } else if (ammountBuy <= 10000000 ) {
            return 1;
        } else if (ammountBuy <= 20000000 ) {
            return 2;
        } else if (ammountBuy <= 50000000 ) {
            return 3;
        } return 4;
    }

    /**
     * Method khi mua hàng, số tiền mua tăng lên ammount
     * @param ammount
     * 
     */
    public void buyOrder(double ammount) {
        this.ammountBuy += ammount;
    }

   

}
