package com.devcamp.shopplus.Entity;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Entity tương ứng bảng brands quản lý thông tin về Chiến dịch khuyến mại
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "campaign")
public class CCampaign {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "campaign_code")
    @NotNull(message = "Mã chiến dịch khuyến mại không được để trống")
    private String campaignCode;

    @Column(name = "campaign_name")
    @NotNull(message = "Tên chiến dịch khuyến mại không được để trống")
    private String campaignName;

    @Column(name = "campaign_description")
    @NotNull(message = "Mô tả chiến dịch khuyến mại không được để trống")
    private String campaignDescription;

    private String note;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message = "Ngày bắt đầu không được để trống")
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message = "Ngày kết thúc không được để trống")
    @Column(name = "end_date")
    private Date endDate;

    private String photo;

    public CCampaign() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getCampaignDescription() {
        return campaignDescription;
    }

    public void setCampaignDescription(String campaignDescription) {
        this.campaignDescription = campaignDescription;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    

}
