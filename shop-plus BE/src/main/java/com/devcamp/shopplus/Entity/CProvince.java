package com.devcamp.shopplus.Entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * Entity tương ứng bảng Province quản lý thông tin về Tỉnh/Thành phố
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "province")
public class CProvince {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "_code")
    private String code;

    @Column(name = "_name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "province")
    @JsonIgnore
    private Set<CDistrict> districts;

    public CProvince() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CDistrict> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<CDistrict> districts) {
        this.districts = districts;
    }

}
