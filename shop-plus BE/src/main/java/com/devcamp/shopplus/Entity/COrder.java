package com.devcamp.shopplus.Entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;

/**
 * Entity tương ứng bảng loyalty quản lý thông tin về khách hàng loyalty
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "orders")
public class COrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @PositiveOrZero(message = "Trạng thái đơn hàng không hợp lệ")
    private int status;

    private String comments;

    private Long customerId;

    private Date orderDate;
    private Date requiredDate;
    private Date shippedDate;
    private double price;


    public COrder() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(Date requiredDate) {
        this.requiredDate = requiredDate;
    }

    public Date getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(Date shippedDate) {
        this.shippedDate = shippedDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    

}
