package com.devcamp.shopplus.Entity;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;

/**
 * Entity tương ứng bảng loyalty_transaction quản lý thông tin về giao dịch
 * loyalty
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "loyalty_transaction")
public class CLoyaltyTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @PositiveOrZero(message = "Id khách hàng thân thiết không hợp lệ")
    private Long loyaltyId;

    @PositiveOrZero(message = "Số điểm tiêu dùng phải >= 0")
    private int consumptionPoint;

    @PositiveOrZero(message = "Số điểm xếp hạng phải >= 0")
    private int loyaltyPoint;

    private String transactionName;

    private Date transactionDate;

    public CLoyaltyTransaction() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getConsumptionPoint() {
        return consumptionPoint;
    }

    public void setConsumptionPoint(int consumptionPoint) {
        this.consumptionPoint = consumptionPoint;
    }

    public int getLoyaltyPoint() {
        return loyaltyPoint;
    }

    public void setLoyaltyPoint(int loyaltyPoint) {
        this.loyaltyPoint = loyaltyPoint;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Long getLoyaltyId() {
        return loyaltyId;
    }

    public void setLoyaltyId(Long loyaltyId) {
        this.loyaltyId = loyaltyId;
    }

    
}
