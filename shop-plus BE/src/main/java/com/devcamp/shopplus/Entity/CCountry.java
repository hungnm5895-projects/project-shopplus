package com.devcamp.shopplus.Entity;

import javax.persistence.*;

/**
 * Entity tương ứng bảng Province quản lý thông tin về Quốc Gia
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "countries")
public class CCountry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "country_name")
    private String countryName;

    @Column(name = "country_code")
    private String countryCode;

    public CCountry() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

}
