package com.devcamp.shopplus.Entity;

import javax.persistence.*;

/**
 * Entity tương ứng bảng orders_detail quản lý thông tin về chi tiết đơn hàng
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "order_details")
public class COrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int quantityOrder;
    private double priceEach;

    private Long productId;
    private Long orderId;

    public COrderDetail() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantityOrder() {
        return quantityOrder;
    }

    public void setQuantityOrder(int quantityOrder) {
        this.quantityOrder = quantityOrder;
    }

    public double getPriceEach() {
        return priceEach;
    }

    public void setPriceEach(double priceEach) {
        this.priceEach = priceEach;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }


}
