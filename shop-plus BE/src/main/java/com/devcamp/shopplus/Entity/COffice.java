package com.devcamp.shopplus.Entity;

import java.math.BigDecimal;

import javax.persistence.*;

@Entity
@Table(name = "offices")
public class COffice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int provinceId;

    private String phone;

    private String address;

    private String lat;

    private String lng;

    private String name;
    public COffice() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getProvince() {
        return provinceId;
    }

    public void setProvince(int province) {
        this.provinceId = province;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

   

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

      

}
