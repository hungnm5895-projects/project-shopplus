package com.devcamp.shopplus.Entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

/**
 * Entity tương ứng bảng brands quản lý thông tin về Thương hiệu
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "brands")
public class CBrand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "brand_name")
    @NotNull(message = "Tên thương hiệu không được để trống")
    private String brandName;

    @PositiveOrZero(message = "Quốc gia không hợp lệ")
    private int brandCountryId;

    private String brandDescription;


    public CBrand() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getBrandCountryId() {
        return brandCountryId;
    }

    public void setBrandCountryId(int brandCountryId) {
        this.brandCountryId = brandCountryId;
    }

    public String getBrandDescription() {
        return brandDescription;
    }

    public void setBrandDescription(String brandDescription) {
        this.brandDescription = brandDescription;
    }

    

    
}
