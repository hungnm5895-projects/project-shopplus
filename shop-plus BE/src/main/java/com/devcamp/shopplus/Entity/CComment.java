package com.devcamp.shopplus.Entity;

import java.util.Date;

import javax.persistence.*;

/**
 * Entity tương ứng bảng comment quản lý thông tin về comment của user
 * 
 * @author hungnm
 * @version 1.0.0
 */
@Entity
@Table(name = "comment")
public class CComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date createDate;
    private String comment;
    private int emonation;

    private Long userId;
    private Long productId;

    public CComment() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getEmonation() {
        return emonation;
    }

    public void setEmonation(int emonation) {
        this.emonation = emonation;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    
}
