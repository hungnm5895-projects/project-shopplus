package com.devcamp.shopplus.Entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Table(name = "payments")
public class CPayment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@PositiveOrZero(message = "Số tiền thanh toán không hợp lệ")
	private BigDecimal ammount;

	@Column(name = "check_number")
	private String checkNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "payment_date")
	private Date paymentDate;

	private Long customer_id;

	// @Transient
	// private long userId;

	public CPayment() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getAmmount() {
		return this.ammount;
	}

	public void setAmmount(BigDecimal ammount) {
		this.ammount = ammount;
	}

	public String getCheckNumber() {
		return this.checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}

	/*
	 * public User getUser() {
	 * return user;
	 * }
	 * 
	 * public void setUser(User user) {
	 * this.user = user;
	 * }
	 * 
	 * public void setId(long id) {
	 * this.id = id;
	 * }
	 * 
	 * public long getUserId() {
	 * return user.getId();
	 * }
	 */

}
