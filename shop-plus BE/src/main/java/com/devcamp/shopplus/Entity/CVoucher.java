package com.devcamp.shopplus.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Range;

/**
 * Entity tương ứng bảng Vouchers
 * 
 * @author hungnm
 * @version 1.0.0
 */

@Entity
@Table(name = "vouchers")
public class CVoucher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Mã voucher không được để trống")
    @Column(name = "voucher_code")
    private String voucherCode;

    @NotNull(message = "Tên voucher không được để trống")
    @Column(name = "voucher_name")
    private String voucherName;

    @NotNull(message = "Loại voucher không được để trống")
    @Range(min = 0, max = 5, message = "Loại voucher không hợp lệ")
    private int type;

    private int useable;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true,  name = "create_date")
    private Date createDate;

    @Positive(message = "Giá trị voucher không hợp lệ")
    @NotNull(message = "Giá trị voucher không được để trống")
    @Column(name = "voucher_value")
    private double voucherValue;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message = "Ngày hết hạn không được để trống")
    @Column(name = "expire_date")
    private Date expireDate;

    public CVoucher() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public void setVoucherName(String voucherName) {
        this.voucherName = voucherName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUseable() {
        return useable;
    }

    public void setUseable(int useable) {
        this.useable = useable;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public double getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(double voucherValue) {
        this.voucherValue = voucherValue;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }


}
