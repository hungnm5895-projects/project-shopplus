package com.devcamp.shopplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.devcamp.shopplus.property.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class ShopPlusApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopPlusApplication.class, args);
	}

}
